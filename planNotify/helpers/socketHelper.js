﻿var eventName = {
    getUpcomingPlan: 'getUpcomingPlan',
    sendPlanNotifyToRoom: 'sendPlanNotifyToRoom'
};
exports.eventName = eventName;

function getUpcomingPlan(socket, unixTimeStamp, pastMinute, callback) {
    socket.emit(eventName.getUpcomingPlan, { timeStamp: unixTimeStamp, pastMinute: pastMinute }, (result) => {
        callback(result);
    });
}
exports.getUpcomingPlan = getUpcomingPlan;

function sendPlanNotifyToRoom(socket, plan, callback) {
    socket.emit(eventName.sendPlanNotifyToRoom, {
        plan: plan
    }, (result) => {
        callback(result);
    });
}
exports.sendPlanNotifyToRoom = sendPlanNotifyToRoom;