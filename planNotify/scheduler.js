﻿var schedule = require('node-schedule');
var socketHelper = require('./helpers/socketHelper');
var errorCodes = require('./helpers/resultHelper').errorCodes;
var config = require('./config');
var log = require('./log.js');

var moment = require('moment');
var async = require('async');


exports.init = (socket) => {
    var scheduleHandle = schedule.scheduleJob(config.cronJobSetting, function (fireDate) {
        console.log('Schedule run at ' + fireDate);
        getUpcomingPlan(socket);
    });
}

function getUpcomingPlan(socket) {
    var unixTimeStamp = moment().unix();
    var pastMinute = config.pastMinute;
    socketHelper.getUpcomingPlan(socket, unixTimeStamp, pastMinute, (result) => {
        
        if (result.errorCode == errorCodes.success) {
            console.log(result.data);
            async.eachLimit(result.data, config.asyncLimit,
                (plan, cb) => {
                    console.log(plan);
                    socketHelper.sendPlanNotifyToRoom(socket, plan, () => {
                        cb();
                    });
                },
                (err) => {
                    log.info(moment(unixTimeStamp * 1000).format('YYYY-MM-DD HH:mm:ss') + ' done at ' + moment().format('YYYY-MM-DD HH:mm:ss'));
                });
        }
        else {
            log.error(result);
        }
    });
}