﻿var io = require('socket.io-client');

var scheduler = require('./scheduler');
var config = require('./config');

module.exports = () => {
    var socket;    
    var connect = () => {
        socket = io.connect(config.socketUrl[config.env], {
            query: 'token=' + config.serviceToken
        });

        socket.on('reconnect_failed', function () { console.log('reconnect_failed'); });
        socket.on('connect_error', function (err) { console.log(err.description); });
        socket.on('connected', () => {
            console.log('connected');
        });
        socket.on('connectSuccess', () => {
            console.log('connect success');
        });

        socket.on('connectFail', () => {
            console.log('connect fail');
        });

        //chạy khởi tạo schedule 
        scheduler.init(socket);
    };

    setTimeout(() => {
        connect();

    }, 1000);
}