﻿var config = {
};
config.cronJobSetting = '0 * * * * *';
config.pastMinute = 10;
config.asyncLimit = 100;

config.env = 'local';
config.socketUrl = {
    server: 'http://chatapi.chatnhanh.com:3001',
    local: 'http://localhost:3000'
};
config.serviceToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YzI0NGMyOTFjMzU2ZjM2MmM1MTZmNmEiLCJuYW1lIjoiUGxhbk5vdGlmeUJvdCIsImVtYWlsIjoicGxhbm5vdGlmeWJvdEBteXh0ZWFtLmNvbSIsInBob25lIjoiIiwiYXZhdGFyIjoiIiwicGFzc3dvcmQiOiI2YzE3NjQiLCJjcmVhdGVEYXRlIjowLCJsYXN0VXBkYXRlRGF0ZSI6MCwibGFzdExvZ2luRGF0ZSI6MTU0NTk5MzUyNiwiaWF0IjoxNTQ1OTkzNTI2LCJleHAiOjQ2OTk1OTM1MjZ9.wvwFGBj9oTqjjXjE7n_oH0rmJflB1QSTJeiJJUxCw9M';

module.exports = config;