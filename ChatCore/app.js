﻿var express = require('express');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var moment = require('moment');

var async = require('async');

var fs = require('fs');
var https = require('httpolyglot');
var http = require('http');

var myXteamV1 = require('./services/myXteamV1');

var cryptoHelper = require('./utility/cryptoHelper');

var db = require('./models/db.js');
var dbRoom = require('./models/dbRoom.js');
var dbUser = require('./models/dbUser.js');

var defaultConfig = require('./configs');

var routes = require('./routes/index');
var users = require('./routes/users');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(require('stylus').middleware(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));



//connect to mongo
var connectDBTasks = [];
connectDBTasks.push(function (cb) {
    db.connect(function (err) {
        if (err) {
            console.log('Unable to connect to Mongo.')
            console.log(err);
            process.exit(1);
        }
        else {
            cb();
        }
    });
});
connectDBTasks.push(function (cb) {
    dbRoom.connect(function (err) {
        if (err) {
            console.log('Unable to connect to Mongo Room.')
            console.log(err);
            process.exit(1)
        }
        else {
            cb();
        }
    });
});
connectDBTasks.push(function (cb) {
    dbUser.connect(function (err) {
        if (err) {
            console.log('Unable to connect to Mongo User.')
            console.log(err);
            process.exit(1)
        }
        else {
            cb();
        }
    });
});

async.series(connectDBTasks, (err) => {
    console.log('connected db');

    //cors and preflight config
    //set lại origin
    app.use(function (req, res, next) {

        let origin = req.headers.origin;
        console.log('origin', origin);
        if (defaultConfig.allowedOrigins.includes(origin)) {
            res.header("Access-Control-Allow-Origin", origin);
        }
        res.header("Access-Control-Allow-Origin", '*');
        res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        res.header('Access-Control-Allow-Credentials', true);
        if (req.method === "OPTIONS") {
            res.sendStatus(200);
        }
        else {
            next();
        }
    });

    require('./restAPI')(app, defaultConfig.jwtSecret);

    app.use('/', routes);
    app.use('/users', users);

    console.log(moment().utc());


    // catch 404 and forward to error handler
    app.use(function (req, res, next) {
        var err = new Error('Not Found');
        err.status = 404;
        next(err);
    });

    // error handlers

    // development error handler
    // will print stacktrace
    if (app.get('env') === 'development') {
        app.use(function (err, req, res, next) {
            res.status(err.status || 500);
            res.render('error', {
                message: err.message,
                error: err
            });
        });
    }

    // production error handler
    // no stacktraces leaked to user
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: {}
        });
    });


    app.set('port', process.env.PORT || 3002);
    //var privateKey = fs.readFileSync('./muabannhanh.key', 'utf8');
    //var certificate = fs.readFileSync('./muabannhanh.crt', 'utf8');

    //var credentials = { key: privateKey, cert: certificate };

    //var server = https.createServer(credentials, app);
    var server = http.createServer(app);

    app.use(function (req, res, next) {
        console.log('https://' + req.headers.host + req.url + req.path);
        if (req.socket.encrypted) {
            // request was via https, so do no special handling
            next();
        } else {
            // request was via http, so redirect to https
            console.log('https://' + req.headers.host + req.url + req.path);
            res.redirect('https://' + req.headers.host + req.url);
        }
    });

    server.listen(app.get('port'), function () {
        console.log('Express server listening on port ' + app.get('port'))
    });

    //console.log(cryptoHelper.encrypt('lol'));

    //myXteamV1.authtoken('tester@gmail.com', '1', (result) => { console.log(result); });
    myXteamV1.getUserInfo('c7OqxQ1DdgsFWx5uQrpREXBZl1QJGnZtDzL1-ckEzoruNaBFtHXinH0UKPkq7h9GzKF1xMoUUICt4UWgRFMqcILR6x3Nq5w-n-JM1zPa1s0rXVbkEXL0onpXHDIvPXtyiT0bcKxghwZ_SK8j7goijQP4foZYbOMt1P1gzJ-u8tVPZbHw5Fr1dH6CJOPwczVL8F9d7n9ZubCOXnIOHuS5uYyUYGy8Dxg8VCkR4QZu2TtwKIm9bzTdfNg5rzwuFIdZpuQ09jP_yP9UoGr3dhOp5pIEcjP-_ERC_ua6s2JldT4TL68y5tM_LMu4a2BftZ7cusfjddvAtpbJGRBtnFy_rNHaZ4ovbDfwOwR5ADiPwirWQ1O7foND8E4VL8EbeRXAJt2RAWVJHnisM81kZpIJEbk1zqL7VWe4kwNRE77xhYxAXQUVH_4b01uehPaAZpecdbH1Oy_0QeLWJdDG_eujDNwSPM3bOq6Ek1BhtKh5ya00ufSuk35I94_eRYLRBvqYIw-Upka6_7Qbg6W_hxTtjybxNK3DPoEhEVvjmpJglt8UiakSYE-vFb9NuhLSpOdfkGzI8XihFJnt1EK2h9r-WM513MgqjqzmSgFrS6UkysZIbm-Ctic0dgbtkJpqQJGeHQYVh4OQNxJU7Tk_IvxYPhqYr8HjxYNwU8eryR_N43N2wFYn4qqhf3V1LxbOA7YLPTxDvaG2E51um8eJ2lf4N430OBNxR-jH-pGZ8pOSPhu5JX7s7eXxkXe45WvfMlMw-1uptL9wS-W13mQYZOoaFI9wfmonspiCrrzqziVAmVf9YpKleEj_NsxirZrs8MpuEmPaCSxj7nbtYArCIlDdEraQOTO88pzf9snhtbZAwVhBXneHtCcYNECQDEKX6d46gDaXmNgb2gydj_i8NqyH53G9Um8IsLH_9zi4im6LbChNw138tSqUWD3BQPy-RYiMYLMP8iWh2dxiptOGS8cNG7R4w4UkdpdBK51-FXs0k3o5RH3bjHEM1xEZlr4vqWm4kVPnB5Sgb3ZIVVxJXQW3Fv0sifQp3P_dnhRCDdeKFVb27rapogPPqppWtL-DmqumAVVMGRMM82hCTNOO3dHr7WFPtgHeNZc1TvPnYakzXWFnXvUq6-VEtnUgJZx_d-GCKNlKM62jHGyONjdYr_JgsvTHO3_GyMarETfR1bxXtJw',
        (result) => {
            console.error(result);
        });
    // socket.io events
    require('./socket.js')(server);
});



