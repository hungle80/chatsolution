﻿var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var compression = require('compression');

var fs = require('fs');
var http = require('http');
var https = require('https');

var routes = require('./routes/index');
var login = require('./routes/login');
var users = require('./routes/users');
var widget = require('./routes/widget');
var widgets = require('./routes/widgets');
var test = require('./routes/test');
var join = require('./routes/join');
var chatpane = require('./routes/chatpane');
var emoji = require('./routes/emoji');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(require('stylus').middleware(path.join(__dirname, 'public')));

app.use(compression({ shouldCompress }));
app.use(express.static(path.join(__dirname, 'public')));

function shouldCompress(req, res) {
    if (req.headers['x-no-compression']) {
        // don't compress responses with this request header
        return false
    }

    // fallback to standard filter function
    return compression.filter(req, res)
}

app.use('/', routes);
app.use('/login', login);
app.use('/users', users);
app.use('/widget', widget);
app.use('/widgets', widgets);
app.use('/test', test);
app.use('/chatpane', chatpane);
app.use('/emoji', emoji);
app.use('/join', join);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


var privateKey = fs.readFileSync('./muabannhanh.key', 'utf8');
var certificate = fs.readFileSync('./muabannhanh.crt', 'utf8');

var credentials = { key: privateKey, cert: certificate };
var options = {
    key: fs.readFileSync('server.key'),
    cert: fs.readFileSync('server.crt')
}
var httpsServer = https.createServer(credentials, app);
var httpServer = http.createServer(app);
//httpsServer.listen(8443);
//app.set('port', 443);
app.set('port', 3003);

var server = httpsServer.listen(app.get('port'), function () {
    console.log('Express server listening on port ' + server.address().port);
});

//http.createServer(function (req, res) {
  //  res.writeHead(301, { "Location": "https://" + req.headers['host'] + req.url });
  //  res.end();
//}).listen(80);
