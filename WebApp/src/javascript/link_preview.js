const REGEX_VALID_URL = new RegExp(
    "^" +
    // protocol identifier
    "(?:(?:https?|ftp)://)" +
    // user:pass authentication
    "(?:\\S+(?::\\S*)?@)?" +
    "(?:" +
    // IP address exclusion
    // private & local networks
    "(?!(?:10|127)(?:\\.\\d{1,3}){3})" +
    "(?!(?:169\\.254|192\\.168)(?:\\.\\d{1,3}){2})" +
    "(?!172\\.(?:1[6-9]|2\\d|3[0-1])(?:\\.\\d{1,3}){2})" +
    // IP address dotted notation octets
    // excludes loopback network 0.0.0.0
    // excludes reserved space >= 224.0.0.0
    // excludes network & broacast addresses
    // (first & last IP address of each class)
    "(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])" +
    "(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}" +
    "(?:\\.(?:[1-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))" +
    "|" +
    // host name
    "(?:(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+)" +
    // domain name
    "(?:\\.(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+)*" +
    // TLD identifier
    "(?:\\.(?:[a-z\\u00a1-\\uffff]{2,}))" +
    // TLD may end with dot
    "\\.?" +
    ")" +
    // port number
    "(?::\\d{2,5})?" +
    // resource path
    "(?:[/?#]\\S*)?" +
    "$", "i"
);


function resolveUrl(rootUrl, src) {
    try {
        if (src.startsWith('http')) {
            console.log('resolveUrl');
            console.log(new URL(src));

            return new URL(src).href;
        }
        else {
            console.log('resolveUrl');
            var link = new URL(rootUrl);
            return link.origin + src;
        }

    } catch (error) {
        var link = new URL(rootUrl);
        return link.origin + src;
    }
}

function getLinkPreview(text) {

    var that = this;
    return new Promise((resolve, reject) => {
        if (!text) {
            reject({ error: 'React-Native-Link-Preview did not receive either a url or text' });
        }

        let detectedUrl = null;

        text.split(' ').forEach(token => {
            if (REGEX_VALID_URL.test(token) && !detectedUrl) {
                detectedUrl = token;
            }
        });
        console.log('detectedUrl', detectedUrl);
        if (detectedUrl) {
            $.ajaxPrefilter(function (options) {
                if (options.crossDomain && jQuery.support.cors) {
                    var http = (window.location.protocol === 'http:' ? 'http:' : 'https:');
                    //options.url = http + '//cors-anywhere.herokuapp.com/' + options.url;
                    options.url = 'https://livechat.muabannhanh.com:444/' + options.url;
                }
            });

            $.get(
                text,
                function (response) {
                    console.log(response);
                    resolve(_parseResponse(response, detectedUrl));
                })
                .fail(function (jqxhr, textStatus, error) { reject({ error }); });
        } else {
            reject({ error: 'React-Native-Preview-Link did not find a link in the text' });
        }
    });
}

function _parseResponse(body, url) {
    const doc = $(body);
    var data = {
        url,
        title: this._getTitle(doc),
        description: this._getDescription(doc),
        mediaType: this._getMediaType(doc) || 'website',
        images: this._getImages(doc, url),
        videos: this._getVideos(doc)
    };
    return data;
}


function _getTitle(doc) {
    let title = doc.filter('meta[property=\'og:title\']').attr('content');

    if (!title) {
        title = doc.filter('title').text();
    }
    return title;
}

function _getDescription(doc) {
    let description = doc.filter('meta[name=description]').attr('content');

    if (description === undefined) {
        description = doc.filter('meta[name=Description]').attr('content');
    }

    if (description === undefined) {
        description = doc.filter('meta[property=\'og:description\']').attr('content');
    }

    if (description === undefined) {
        description = doc.filter('#description').html();
    }
    return description;
}

function _getMediaType(doc) {
    const node = doc.find('meta[name=medium]');

    if (node.length) {
        const content = node.attr('content');
        return content === 'image' ? 'photo' : content;
    } else {
        return doc.find('meta[property=\'og:type\']').attr('content');
    }
}

function _getImages(doc, rootUrl) {
    let images = [];
    var that = this;
    console.log('start get image');
    console.log(images);
    $.each(doc.filter('meta'), function (index, el) {

        if ($(el).attr('property') === 'og:image' || $(el).attr('property') === 'og:image:url' || $(el).attr('itemprop') === 'image') {
            var url = resolveUrl(rootUrl, $(el).attr('content'));
            console.log('image link', url);
            images.push(url);
        }
    });

    if (images.length <= 0) {
        $.each(doc.filter('link'), function (index, el) {
            if ($(el).attr('rel') === 'og:image_src') {
                var url = resolveUrl(rootUrl, $(el).attr('href'))
                console.log('image link', url);
                images.push(url);
            }
        });
    }
    console.log(doc.filter('img'));
    if (images.length <= 0) {
        $.each(doc.filter('img'), function (index, el) {
            var url = resolveUrl(rootUrl, $(el).attr('src'))
            console.log('image link', url);
            images.push(url);
        });
    }

    if (images.length <= 0) {
        $.each(doc.filter('link'), function (index, el) {
            if ($(el).attr('rel') === 'icon') {
                var url = resolveUrl(rootUrl, $(el).attr('href'))
                console.log('image link', url);
                images.push(url);
            }
        });
    }
    console.log(images);
    return images;
}


function _getVideos(doc) {
    const videos = [];
    let nodeTypes;
    let nodeSecureUrls;
    let nodeType;
    let nodeSecureUrl;
    let video;
    let videoType;
    let videoSecureUrl;
    let width;
    let height;
    let videoObj;
    let index;

    const nodes = doc.find('meta[property=\'og:video\']');
    const length = nodes.length;

    if (length) {
        nodeTypes = doc.find('meta[property=\'og:video:type\']');
        nodeSecureUrls = doc.find('meta[property=\'og:video:secure_url\']');
        width = doc.find('meta[property=\'og:video:width\']').attr('content');
        height = doc.find('meta[property=\'og:video:height\']').attr('content');

        for (index = 0; index < length; index++) {
            video = (nodes[index].attr('content'));

            nodeType = nodeTypes[index];
            videoType = nodeType ? nodeType.attr('content') : null;

            nodeSecureUrl = nodeSecureUrls[index];
            videoSecureUrl = nodeSecureUrl ? nodeSecureUrl.attr('content') : null;

            videoObj = { url: video, secureUrl: videoSecureUrl, type: videoType, width, height };
            if (videoType.indexOf('video/') === 0) {
                videos.splice(0, 0, videoObj);
            }
            else {
                videos.push(videoObj);
            }
        }
    }

    return videos;
}













