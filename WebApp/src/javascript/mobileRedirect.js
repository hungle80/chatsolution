﻿function getMobileOperatingSystem() {
    var userAgent = navigator.userAgent || navigator.vendor || window.opera;

    // Windows Phone must come first because its UA also contains "Android"
    if (/windows phone/i.test(userAgent)) {
        return "Windows Phone";
    }

    if (/android/i.test(userAgent)) {
        return "Android";
    }

    // iOS detection from: http://stackoverflow.com/a/9039885/177710
    if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
        return "iOS";
    }

    return "unknown";
}

var os = getMobileOperatingSystem();
switch (os) {
    case 'Android': {
        window.location.href = 'https://play.google.com/store/apps/details?id=chatnhanh.core';
        break;
    }
    case 'iOS': {
        window.location.href = 'https://itunes.apple.com/vn/app/chatnhanh-chat-mua-b%C3%A1n-nhanh/id1061004934?l=vi&mt=8';
        break;
    }
}