﻿var mbnUrl = '';
var maxLimit = 10; //Megabyte
var maxThumbWidth = 300;
var maxImageWidth = 1920;
var messageWrapperWidth = 220;

var $chatHistory;
var $chatTextarea;
var $chatSendButton;
var $chatUploadFileButton;
var $chatFileInput;
var $chatUnreadBagde;

//socket varible
var socket;
var currentUserId;
var currentUserInfo;
var channelAdministratorRoomId;
var currentRoomUnreadMessage = 0;
var currentRoomMembers = [];

var CLIPBOARD;
$(document).ready(function () {
    CLIPBOARD = new CLIPBOARD_CLASS("canvasImage", "pasteImageToChatBox", false);
});

$(document).ready(function () {
    globalIsFull = false;
    $chatHistory = $('.chat .chat-history');
    $chatTextarea = $('#message-to-send');
    $chatSendButton = $('#btSend');
    $chatUploadFileButton = $('#btChooseFile');
    $chatFileInput = $('#fileAttach');
    $chatUnreadBagde = $('.notify-bagde');

    $('.container').on('click', '.chat-history, .chat-message', function (e) {
        clearChatNotify(channelAdministratorRoomId);
    });

    $('.btNav').click(function () {
        console.log('click');
        var isOpen = $(this).data('open');
        toggleMenu(isOpen);
    });
    $('.pane-layer').click(function () {
        toggleMenu(true);
    });
});

function toggleMenu(isOpen) {
    if ($(document).width() <= 400) {
        var $el = $('.btNav').find('i');
        $('.btNav').data('open', !isOpen);
        if (isOpen) {
            $el.removeClass('fa-close').addClass('fa-navicon');
            $('.left-pane').animate({ left: -400 }, 200, function () {
                $('.left-pane').css('display', 'none');
            });
            $('.pane-layer').hide();
        }
        else {
            $el.addClass('fa-close').removeClass('fa-navicon');
            $('.left-pane').css('display', 'flex');
            $('.left-pane').animate({ left: 0 }, 200, function () {
                $('.pane-layer').show();
            });

        }
    }
}

function setReferrer(referrer) {
    //console.log('referrer: ' + referrer);
    var splitData = referrer.split('/');
    var host = splitData[0] + '//' + splitData[2];
    //console.log(host);
    mbnUrl = host;
}

//wrapper event
function onClickCollapseButton() {
    //parent.collapseChatBox();
    //console.log(mbnUrl);
    parent.postMessage('collapse', mbnUrl);
    $('.container').removeClass('expanded');
    $('.chat-collapse').show();
    $('.chat').hide();
}

function onClickExpandButton() {
    //parent.expandChatBox();
    parent.postMessage('expand', mbnUrl);
    $('.container').addClass('expanded');
    $('.chat-collapse').hide();
    $('.chat').show();
}

function onClickToLogin() {
    console.log(mbnUrl);
    parent.postMessage('login', mbnUrl);
}

function onClickToRegister() {
    parent.postMessage('register', mbnUrl);
}

function postUnreadMessageToParent(count) {
    var data = {
        eventName: 'unreadMessage',
        number: count
    };
    parent.postMessage(data, mbnUrl);
}

function receiveMessage(event) {
    if (event.data) {
        //console.log('receive post message');
        if (event.data.eventName == "createRoom") {
            //console.log(event);
            switch (event.data.info.type) {
                case 'private': {
                    if (event.data.info.createTextMessage) {
                        if (event.data.info.text) {
                            event.data.info.text = decodeURITryCatch(event.data.info.text);
                        }
                    }
                    break;
                }
                case 'item': {
                    event.data.info.itemName = decodeURITryCatch(event.data.info.itemName);
                    event.data.info.itemImage = decodeURITryCatch(event.data.info.itemImage);
                    event.data.info.itemLink = decodeURITryCatch(event.data.info.itemLink);
                    break;
                }
                case 'page': {
                    event.data.info.pageName = decodeURITryCatch(event.data.info.pageName);
                    event.data.info.pageImage = decodeURITryCatch(event.data.info.pageImage);
                    event.data.info.pageLink = decodeURITryCatch(event.data.info.pageLink);
                    if (event.data.info.createItemMessage) {
                        if (event.data.info.itemName) {
                            event.data.info.itemName = decodeURITryCatch(event.data.info.itemName);
                        }
                        if (event.data.info.itemImage) {
                            event.data.info.itemImage = decodeURITryCatch(event.data.info.itemImage);
                        }
                        if (event.data.info.itemLink) {
                            event.data.info.itemLink = decodeURITryCatch(event.data.info.itemLink);
                        }
                    }
                    break;
                }
            }
            //console.log(event.data.info);
            createRoom(event.data.info);
        }
        else if (event.data.eventName == "createMessage") {
            switch (event.data.info.type) {
                case 'item': {
                    event.data.info.itemName = decodeURITryCatch(event.data.info.itemName);
                    event.data.info.itemImage = decodeURITryCatch(event.data.info.itemImage);
                    event.data.info.itemLink = decodeURITryCatch(event.data.info.itemLink);
                    break;
                }
            }

        }
    }
}
window.addEventListener("message", receiveMessage, false);

function appendRecentChatRoomWhenCreateRoom(roomInfo) {
    var item = {
        _id: roomInfo._id,
        type: roomInfo.type,
        roomName: decodeURITryCatch(roomInfo.roomName),
        roomAvatar: decodeURITryCatch(roomInfo.roomAvatar),
        members: roomInfo.members
    };
    generateRecentChatRoomItem($('#divRecentChat'), item, true);
}
function showRoomAfterCreate(result) {
    //console.log('data after create room');
    //console.log(result);
    if (result.errorCode == 0) {
        var item = result.data;
        var $recentItem = $('#divRecentChat li[data-id=' + item._id + ']');
        if ($recentItem.length == 0) {
            appendRecentChatRoomWhenCreateRoom(item);
            $recentItem = $('#divRecentChat li[data-id=' + item._id + ']');

            //$recentItem.click();
        }
        else {
            $recentItem.prependTo($('#divRecentChat'));
        }

        setChatRoomSelected($recentItem, item, true);
    }
}
function createRoom(info) {
    if (socket) {
        switch (info.type) {
            case 'private':
                {
                    socket.emit('getPrivateRoom',
                        {
                            userId: info.userId
                        }, function (result) {
                            //console.log(result);
                            showRoomAfterCreate(result);
                            if (result.errorCode == 0) {
                                var roomId = result.data._id;
                                if (info.createTextMessage) {
                                    (function (_roomId) {
                                        var checkRoomCreatedHandle = setInterval(function () {
                                            var $chatHistory = getChatHistoryWrapper(_roomId);
                                            if ($chatHistory.length > 0) {
                                                clearInterval(checkRoomCreatedHandle);
                                                parseRoomLogBeforeSend($chatHistory, currentUserId, _roomId, info.text, false);
                                            }
                                        }, 500);
                                    })(roomId);
                                }
                            }
                        });
                    break;
                }
            case 'item':
                {
                    socket.emit('getItemRoom',
                        {
                            userIdOwner: info.ownerId,
                            userIdGuest: currentUserId,
                            itemId: info.itemId,
                            itemName: info.itemName,
                            itemImage: info.itemImage,
                            itemLink: info.itemLink,
                            itemPrice: info.itemPrice
                        }, function (result) {
                            //console.log(result);
                            showRoomAfterCreate(result);
                        });
                    break;
                }
            case 'page':
                {
                    socket.emit('getPageRoom',
                        {
                            userIdGuest: currentUserId,
                            pageId: info.pageId,
                            pageName: info.pageName,
                            pageLink: info.pageLink,
                            pageImage: info.pageImage
                        }, function (result) {
                            //console.log(result);
                            showRoomAfterCreate(result);
                            if (result.errorCode == 0) {
                                var roomId = result.data._id;
                                if (info.createItemMessage) {
                                    (function (_roomId) {
                                        var checkRoomCreatedHandle = setInterval(function () {
                                            var $chatHistory = getChatHistoryWrapper(_roomId);
                                            if ($chatHistory.length > 0) {
                                                clearInterval(checkRoomCreatedHandle);
                                                parseItemLogBeforeSend($chatHistory, currentUserId, _roomId, info.itemId, info.itemName, info.itemImage, info.itemLink, info.itemPrice, false);
                                            }
                                        }, 500);
                                    })(roomId);
                                }
                            }
                        });
                    break;
                }
        }
    }
}

//input event
$(document).ready(function () {

    $chatTextarea.bind('keyup', function (event) {
        var key = event.key ? event.key : event.keyIdentifier;
        //console.log(event);
        if (key == 'Enter' && event.shiftKey == false) {
            //console.log('enter');
            getTextToSend($chatTextarea, $chatHistory, channelAdministratorRoomId, event.timeStamp);
        }
    });

    $chatSendButton.bind('click', function (event) {
        getTextToSend($chatTextarea, $chatHistory, channelAdministratorRoomId, event.timeStamp);
    });

    $chatUploadFileButton.bind('click', function () {
        $chatFileInput.click();
    });
});

function fileAttachUpload(el) {
    var files = el.files;
    resetToNull(el);
    handleFileUpload(files, $(el).attr('data-id'), $chatHistory);
}

function calculateUnreadMessageCountAndPostToParent() {
    var unreadCount = $('#divRecentChat > li[data-view=false]').length;
    console.log('unread',unreadCount);
    postUnreadMessageToParent(unreadCount);
}
function getUnreadRoomCount(callback) {
    socket.emit('getUnreadRoomCount', {}, function (result) {
        //console.log(result);
        if (result.errorCode == 0) {
            unreadChatRoom = result.data;
        }
        callback(result);
    });
}

//socket event
function connectSocket(info) {
    $(document).ready(function () {
        var deviceId = Cookies.get('OneID');
        socket = io.connect(socketUrl,
            {
                query: 'userId=' + info.userId + '&phone=' + info.mobile + '&os=web&deviceId=' + deviceId
            });
        currentUserId = info.userId;

        socket.on('connectSuccess', function (result) {
            //console.log('connected');
            currentUserInfo = result.data;
            //console.log(currentUserInfo);
            loadRecentPinChatRoom($('ul#divRecentChat'));
            loadRecentChatRoom($('ul#divRecentChat'), null, '', true);
            getUnreadRoomCount(function (res) {
                if (res.data && res.data.count) {
                    var unreadCount = res.data.count;
                    postUnreadMessageToParent(unreadCount);
                }
            });

        });

        socket.on('connectFail', function () {
            //console.log('connected fail');
        });

        socket.on('newMessage', function (result) {
            if (result.errorCode == 0) {
                var log = result.data;

                updateRecentChatRoom(log);

                //if (log.roomId == currentRoomId) {
                //if (log.userIdAuthor != currentUserId) {
                //    currentRoomUnreadMessage += 1;
                //    updateUnreadMessage();
                //}
                var $chatHistory = getChatHistoryWrapper(log.roomId);
                generateLog($chatHistory, log);
                scrollToBottom($chatHistory);
                //}
                calculateUnreadMessageCountAndPostToParent();
            }
        });

        socket.on('removeMessage', function (result) {
            var log = result.data;
            updateRecentChatRoom(log, true);
            //console.log('removeMessage');
            var $chatHistory = getChatHistoryWrapper(log.roomId);
            updateLog($chatHistory, result);

        });

        socket.on('updateMessage', function (result) {
            var log = result.data;
            var $chatHistory = getChatHistoryWrapper(log.roomId);
            //console.log('updateMessage');
            updateLog($chatHistory, result);
        });

        socket.on('logIsViewed', function (result) {
            if (result.errorCode == 0) {
                var log = result.data;
                var roomId = log.roomId;
                var $chatHistory = getChatHistoryWrapper(roomId);
                updateLog($chatHistory, result);
                var currentIsViewed = log.views.find(function (x) { return x.userId == currentUserId && x.isViewed == true; });
                //console.log(currentIsViewed);
                if (currentIsViewed) {
                    var $recentItem = $('#divRecentChat li[data-id=' + roomId + ']');
                    $recentItem.attr('data-view', true);
                }
            }
        });
    });
}

function getRoomUnreadCount(logs) {
    var done = false;
    var count = 0;
    for (var i = 0; i < logs.length && !done; i++) {
        var log = logs[i];
        //console.log(log);
        for (var j = 0; j < log.views.length && !done; j++) {
            //console.log(log.views[j]);
            if (log.views[j].userId == currentUserId && log.views[j].isViewed == true) {
                done = true;
            }
        }
        if (!done) {
            count++;
        }
    }
    currentRoomUnreadMessage = count;
    updateUnreadMessage();
}

function blinkBagde($chatUnreadBagde) {
    $chatUnreadBagde.fadeTo(100, 0.1).fadeTo(200, 1.0);
}

function updateUnreadMessage() {
    if (currentRoomUnreadMessage == 0) {
        $chatUnreadBagde.html('');
        $chatUnreadBagde.hide();
    }
    else {
        $chatUnreadBagde.html(currentRoomUnreadMessage + (currentRoomUnreadMessage >= 50 ? "+" : ""));
        $chatUnreadBagde.show();
        blinkBagde($chatUnreadBagde);
    }
}


function clearChatNotify(roomId) {
    //xóa notify trên đầu room
    currentRoomUnreadMessage = 0;
    updateUnreadMessage();

    //gọi set đã xem về server
    var $lastItem = $chatHistory.find('ul li:last-child');
    if ($lastItem.length > 0) {
        var isViewed = $lastItem.data('view');
        if (isViewed == false) {
            var $message = $lastItem.find('.message');
            if ($message.length > 0 && $message.hasClass('other-message')) {
                var logId = $lastItem.attr('data-id');
                //console.log(logId);
                socket.emit('setLogIsView', { roomId: roomId, chatLogId: logId }, function (result) {
                    //console.log(result);
                    updateLog($chatHistory, result);
                    $lastItem.data('view', true);
                });
            }
        }
    }
}

function bindDataToElement(roomId) {
    $('#fileAttach').attr('data-id', roomId);
    var url = '/?/' + roomId;
    $('#btGoToFullChat').attr('href', url);
    $('#dragHandler').attr('data-id', roomId);
}



//newly add 19.12.2017
//recent chat room
function getRecentChatRoom(lastLogDate, keyword, callback) {
    socket.emit('getRecentChatRoom', {
        lastLogDate: lastLogDate,
        keyword: keyword
    }, function (result) {
        callback(result);
    });
}

function getPinRecentChatRoom(callback) {
    socket.emit('getRecentPinChatRoom', {
    }, function (result) {
        callback(result);
    });
}

function generateRecentChatRoomItem($recentWrapper, item, isPrepend, isFav) {
    if (isFav == undefined || isFav == null) {
        isFav = false;
    }
    var html = '';
    var currentView = { isViewed: false };

    if (item.lastLog) {
        currentView = item.lastLog.views.find(function (view) {
            return view.userId == currentUserId;
        });
    }

    html += '<li class="' + (isFav ? "pin-room" : "") + '" title="' + escapeQuote(item.roomName) + '" data-id="' + item._id + '" data-view="' + (currentView ? currentView.isViewed : false) + '" data-lastlogid="' + (item.lastLog ? item.lastLog.chatLogId : null) + '">';
    html += '<img class="room-avatar circle" src= "' + item.roomAvatar + '" />';
    html += '<div class="chat-item">'; //begin chat-item
    html += '<div class="chat-info">'; //begin chat-info
    html += '<div class="chat-room-name">' + item.roomName + '</div>';
    html += '</div>';//end chat-item
    html += '<div class="pinWrap"><span id="btPinRoom' + item._id + '" class="fa ' + (isFav ? 'fa-star' : 'fa-star-o') + ' btFavorite" data-favorite="' + isFav + '" data-id="' + item._id + '"></span></div>';
    html += '</li>';
    var $item = $(html);
    $item.click(function () {
        //console.log(window.location);
        setChatRoomSelected($item, item);
        toggleMenu(true);
    });
    var $lastFav;
    if (isPrepend) {
        if (isFav) {
            $recentWrapper.prepend($item);
        }
        else {
            $lastFav = $('#divRecentChat .pin-room:last');
            if ($lastFav.length > 0) {
                $item.insertAfter($lastFav);
            }
            else {
                $recentWrapper.prepend($item);
            }
        }
    }
    else {
        if (isFav) {
            $lastFav = $('#divRecentChat .pin-room:last');
            if ($lastFav.length > 0) {
                $item.insertAfter($lastFav);
            }
            else {
                $recentWrapper.prepend($item);
            }
        }
        else {
            $recentWrapper.append($item);
        }
    }

    $('#btPinRoom' + item._id).click(function () {
        var isFavorite = $(this).data('favorite');
        var roomId = $(this).data('id');
        if (isFavorite) {
            socket.emit('unpinRoom', { roomId: roomId }, function (result) {

            });
        }
        else {
            socket.emit('pinRoom', { roomId: roomId }, function (result) {
            });
        }
    });
}

function setChatRoomSelected($recentItem, item, needGet) {
    //var url = window.location.protocol + '//' + window.location.host + window.location.pathname + "?/" + item._id;
    //var title = item.roomName + " - ChatNhanh";
    //history.pushState('', title, url);

    showChatRoom(item, function () {
        setRecentChatRoomSelected($recentItem);
        setChatIsViewed(item._id);
    }, needGet);
}
function loadRecentPinChatRoom($recentWrapper) {
    getPinRecentChatRoom(function (result) {
        if (result.errorCode == 0) {
            var lst = result.data;
            for (var i = 0; i < lst.length; i++) {
                generateRecentChatRoomItem($recentWrapper, lst[i], false, true);
            }
        }
    });
}
function loadRecentChatRoom($recentWrapper, lastLogDate, keyword, isFirst) {
    getRecentChatRoom(lastLogDate, keyword, function (result) {
        if (result.errorCode == 0) {

            var lst = result.data;
            for (var i = 0; i < lst.length; i++) {
                generateRecentChatRoomItem($recentWrapper, lst[i]);
            }

            if (lst.length > 0) {
                if (isFirst) {
                    //loadRoomFromUrl();
                    var $item = $('#divRecentChat li:first-child');
                    setChatRoomSelected($item, lst[0]);
                }

                if ($('.chat-pane-list').hasScrollBar()) {
                    //có thì scroll load tiếp
                    $('.chat-pane-list').bind('scroll', function () {
                        //console.log('hello');
                        if ($(this).innerHeight() < this.scrollHeight) {
                            if ($(this).scrollTop() <= (this.scrollHeight - 15)) {
                                $(this).unbind('scroll');
                                loadRecentChatRoom($recentWrapper, lst[lst.length - 1].lastLogDate, keyword);
                            }
                        }
                    });
                }
                else {
                    loadRecentChatRoom($recentWrapper, lst[lst.length - 1].lastLogDate, keyword);
                }
            }
            removeDuplicateRecentRoom();
        }
    });
}
function updateRecentChatRoom(log, isDelete) {
    var $recentItem = $('#divRecentChat').find('li[data-id=' + log.roomId + ']');
    if ($recentItem.length > 0) {
        if (isDelete == true) {
            if ($recentItem.attr('data-lastlogid') == log._id) {
                //var html = generateLastLogFromType(log);
                //$recentItem.find('.last-message span').html(html);
            }
        }
        else {
            //$recentItem.find('.last-message').html(generateLastMessage(log.authorInfo.name, log));
            //$recentItem.find('.last-message-time').html(formatDateTimeShort(log.createDate));
            var $lastFav = $('#divRecentChat .pin-room:last');
            var recentIsFav = $recentItem.hasClass('pin-room');
            $recentItem.attr('data-lastlogid', log._id);
            if (recentIsFav) {
                $recentItem.prependTo($('#divRecentChat'));
            }
            else {
                if ($lastFav.length > 0) {
                    $recentItem.insertAfter($lastFav);
                }
                else {
                    $recentItem.prependTo($('#divRecentChat'));
                }
            }
            if (log.userIdAuthor != currentUserId) {
                $recentItem.attr('data-view', false);
            }
        }
    }
    else {
        if (!isDelete) {
            var item = {
                _id: log.roomId,
                type: log.roomInfo.type,
                roomName: log.roomInfo.roomName,
                roomAvatar: log.roomInfo.roomAvatar,
                members: log.roomInfo.members,
                lastLog: log,
                lastLogDate: log.createDate,
                lastLogAuthor: log.authorInfo,
            };
            generateRecentChatRoomItem($('#divRecentChat'), item, true);
        }
    }

    setTimeout(function () {
        removeDuplicateRecentRoom();
    }, 500);
}

$(document).ready(function () {
    bindRecentRoomAction();
});

function removeDuplicateRecentRoom() {
    $('#divRecentChat li[data-id]').each(function (e) {
        var roomId = $(this).data('id');
        var $lis = $('#divRecentChat li[data-id=' + roomId + ']');
        if ($lis.length > 1) {
            $('#divRecentChat li[data-id=' + roomId + ']:gt(0)').remove();
        }
    });
}

function showSearchRecent(isShow) {
    if (isShow) {
        $('#divRecentSearch').removeClass('hide');
        $('#divRecentChat').addClass('hide');
    }
    else {
        $('#divRecentSearch').addClass('hide');
        $('#divRecentChat').removeClass('hide');
    }
}

//chat room
function activeChatRoom($chatBox) {
    $('.chat-pane').addClass('hide');
    $chatBox.removeClass('hide');
}

function showChatRoom(info, callback, needGet) {
    channelAdministratorRoomId = info._id;
    var $chatBox = $('#divChatBoxList').find('.chat-pane[data-id=' + info._id + ']');
    if ($chatBox.length > 0) {
        activeChatRoom($chatBox);
        callback();
    }
    else {

        if (info.roomName && !needGet) { //nếu có roomName 
            generateChatRoom(info); //tạo thông tin trước luôn, ko cần chờ socket đẩy về, cần thì update lại sao                    
        }
        socket.emit('getChatRoom', { roomId: info._id }, function (result) {

            if (result.errorCode == 0) {
                var roomInfo = result.data;
                //console.log(roomInfo);
                var roomId = result.data._id;
                if (!info.roomName || needGet) {
                    generateChatRoom(roomInfo);
                }
                currentRoomMembers[roomId] = result.data.members;
                var lastLogId = null;
                var $chatHistory = getChatHistoryWrapper(roomId);

                getRoomLog($chatHistory, roomId, lastLogId, false);
            }
            else {
                showNoPermissionBox();
            }
            callback();
        });
    }
}
function getChatBox(roomId) {
    return $('#divChatBoxList .chat-pane[data-id=' + roomId + ']');
}

function generateChatInput(roomId, isShow) {
    var html = '';
    html += '<div class="text-input ' + (isShow ? "" : "hide") + '">';  //begin text-input
    html += '<textarea id="tbChatInput' + roomId + '" class="" placeholder= "Nhập để chat ..." />';
    html += '</div>';//end text-input
    html += '<div class="other-input ' + (isShow ? "" : "hide") + '">'; //begin other-input
    html += '<a class="btGoToFull" href="' + chatUrl + '/?/' + roomId + '" target="_blank" title="Chuyển đến ChatNhanh"><img src="' + chatUrl + '/images/icon.png' + '"/></a>';
    html += '<input type="file" data-id="' + roomId + '" multiple id="fileAttach' + roomId + '" name="file" style="display:none" />';
    html += '<a class="big-button mr5 btEmoji" id="btEmoji' + roomId + '"><i class="fa fa-smile-o"></i></a>';
    html += '<a class="big-button mr5" id="btUploadFile' + roomId + '"><i class="fa fa-paperclip"></i></a>';
    //html += '<a class="big-button" id="btPickLocation' + roomId + '"><i class="fa fa-map-marker"></i></a>';
    html += '<a class="big-button mr5 pull-right" id="btSendLike' + roomId + '"><img class="emojione" alt="👍" title=":thumbsup:" src="/emojione/32/1f44d.png"></a>';
    //html += '<a class="big-button" id="btSendLike' + roomId + '"><i class="fa fa-thumbs-o-up"></i></a>';
    html += '<a class="big-button mr5 pull-right hide large-font" id="btSendText' + roomId + '">Send</a>';
    html += '</div>';//end other-input
    return html;
}

function generateChatRoom(info) {
    var $chatBox = $('<div class="chat-pane"></div>');
    $chatBox.attr('data-id', info._id);
    $chatBox.attr('data-type', info.type);
    var roomId = info._id;
    var currentMember = info.members.find(function (member) { return member.userId == currentUserId; });
    var isGroupChannel = info.type == 'custom' || info.type == 'channel';
    var html = '';
    html += '<div class="chat-top-bar">';   //begin chat-top-bar
    html += '<img class="room-avatar circle" src="' + decodeURITryCatch(info.roomAvatar) + '" />';
    html += '<div class="box-info">'; //begin box-info        
    switch (info.type) {
        case 'private': {
            var member = info.members.find(function (x) {
                return x.userId != currentUserId;
            });
            //console.log('member');
            //console.log(member);
            html += '<div class="box-name"><a href="' + decodeURITryCatch(member.userInfo.url) + '" target="_blank" title="' + decodeURITryCatch(escapeQuote(info.roomName)) + '">' + decodeURITryCatch(info.roomName) + '</a></div>';
            //html += '<div class="boxMoreInfo">'; //begin boxMoreInfo
            //html += 'Mobile: ' + member.userInfo.phone;
            //html += '</div>'; //end boxMoreInfo
            break;
        }
        case 'item': {
            html += '<div class="box-name"><a href="' + decodeURITryCatch(info.item.itemLink) + '" target="_blank" title="' + decodeURITryCatch(escapeQuote(info.roomName)) + '">' + decodeURITryCatch(info.roomName) + '</a></div>';
            //html += '<div class="boxMoreInfo">'; //begin boxMoreInfo
            //html += 'Giá bán: ' + formatCurrencyVND(info.item.itemPrice);
            //html += '</div>'; //end boxMoreInfo
            break;
        }
        case 'page': {
            html += '<div class="box-name"><a href="' + decodeURITryCatch(info.page.pageLink) + '" target="_blank" title="' + decodeURITryCatch(escapeQuote(info.roomName)) + '">' + decodeURITryCatch(info.roomName) + '</a></div>';
            //html += '<div class="boxMoreInfo">'; //begin boxMoreInfo
            //html += info.members.length + ' thành viên';
            //html += '</div>'; //end boxMoreInfo
            break;
        }
        default: {
            html += '<div class="box-name"  title="' + decodeURITryCatch(escapeQuote(info.roomName)) + '">' + decodeURITryCatch(info.roomName) + '</div>';
            //html += '<div class="boxMoreInfo">'; //begin boxMoreInfo
            //html += info.members.length + ' thành viên';
            //html += '</div>'; //end boxMoreInfo
            break;
        }

    }
    html += '</div>';//end box-info    
    html += '</div>';//end chat-top-bar

    html += '<div class="chat-window chat">'; //begin chat-window chat
    html += '<div data-id="' + roomId + '" class="dragandrophandler" ondragenter="onDragenter(event, this)" ondragover="onDragover(event)" ondragleave="onDragleave(event, this)" ondrop="onDrop(event, this)">Kéo & thả tập tin vào đây để upload</div>';
    html += '<div class="chat-history chat-logs" data-id="' + roomId + '">';
    html += '<ul></ul>';
    html += '<div class="scroll-down-wrapper" style="display:none"><a id="btScrollToBottom' + roomId + '" class="btn-scroll-down btn btn-circle btn-default"><i class="fa fa-chevron-down"></i></a></div>';
    html += '</div > ';
    html += '<div class="chat-input">'; //begin chat-input
    if (isGroupChannel) {
        if (currentMember) {
            if (info.type == 'channel') {
                if (currentMember.isOwner || (currentMember.isAdmin && currentMember.permissions.postMessage)) {
                    html += generateChatInput(roomId, true);
                }
                else {
                    //mute
                    //join
                    //html += '<div class="chat-input-disabled">';
                    //html += '<a id="btMuteNotify' + roomId + '" class="big-button">Mute</a>';
                    //html += '</div>';
                    html += generateChatInput(roomId, false);
                }
            }
            else if (info.type == 'custom') {
                html += generateChatInput(roomId, true);
            }
        }
        else {
            //join
            //html += '<div class="chat-input-disabled">';
            //html += '<a class="big-button">Join</a>';
            //html += '</div>';
            html += generateChatInput(roomId, false);
        }
    }
    else {
        html += generateChatInput(roomId, true);
    }
    html += '</div>';//end chat-input
    html += '<div class="emoji-wrapper" id="divEmojiWrapper' + roomId + '"></div>';
    html += '</div>';//end chat-window chat        

    $chatBox.html(html);

    $('#divChatBoxList').append($chatBox);
    activeChatRoom($chatBox);

    var $chatHistory = getChatHistoryWrapper(roomId);
    bindChatRoomAction(roomId, $chatHistory);
}



function bindChatRoomAction(roomId, $chatHistory) {
    var $chatTextarea = $('#tbChatInput' + roomId);
    $chatTextarea.bind('keydown', function (event) {

        var key = event.key ? event.key : event.keyIdentifier;
        //console.log(event);
        if (key == 'Enter' && event.shiftKey == false) {
            //console.log('enter');
            event.preventDefault();
            getTextToSend($chatTextarea, $chatHistory, roomId, event.timeStamp, false);
        }
    });

    $chatTextarea.bind('keyup', function (event) {
        var value = $.trim($chatTextarea.val());
        //console.log(value);
        toggleSendButton(roomId, value);
    });

    $('#btSendLike' + roomId).click(function (event) {
        if (!$(this).prop('disabled')) {
            var rawMessage = '👍';
            parseRoomLogBeforeSend($chatHistory, currentUserId, roomId, rawMessage, false);
        }
    });

    $('#btSendText' + roomId).click(function (event) {
        if (!$(this).prop('disabled')) {
            getTextToSend($chatTextarea, $chatHistory, roomId, event.timeStamp, false);
        }
    });
    $chatTextarea.bind('click', function () {
        if (!$(this).prop('disabled')) {
            //console.log('click');
            setChatIsViewed(roomId);
        }
    });

    $('#fileAttach' + roomId).bind('change', function () {
        if (!$(this).prop('disabled')) {
            var files = this.files;
            handleFileUpload(files, roomId, $chatHistory, false);
        }
    });

    $('#btUploadFile' + roomId).click(function (data) {
        if (!$(this).prop('disabled')) {
            $('#fileAttach' + roomId).click();
        }
    });
    $('#divEmojiWrapper' + roomId).load('/emoji/', function () {
        //$('#divEmojiWrapper' + roomId).html(replaceEmoji($('#divEmojiWrapper' + roomId).html()));
        //$('#divEmojiWrapper' + roomId).find('.emoji-list').each(function (e) {
        //    //$(this).html(replaceEmoji($(this).html()));
        //});
        $('#divEmojiWrapper' + roomId).find('.emoji-tabs > li').click(function () {
            var tabName = $(this).data('href');
            $('#divEmojiWrapper' + roomId).find('.emoji-tabs > li').removeClass('selected');
            $(this).addClass('selected');
            $('#divEmojiWrapper' + roomId).find('.emoji-tab-content > li').hide();
            $('#divEmojiWrapper' + roomId).find('.emoji-tab-content > li[data-target=' + tabName + ']').show();
        });
        $('#divEmojiWrapper' + roomId).find('ul.emoji-list li > a').click(function (e) {
            e.preventDefault();
            var emoji = $(this).find('.emojis__list__native').html();
            //console.log('before: ' + $chatTextarea.prop('selectionStart'));
            insertAtCursor($chatTextarea[0], emoji);
            //console.log($chatTextarea.prop('selectionStart'));
            toggleSendButton(roomId, $chatTextarea.val());
        });
    });

    $('#btEmoji' + roomId).click(function (data) {
        if (!$(this).prop('disabled')) {
            $('#divEmojiWrapper' + roomId).toggle();
        }
    });

    //scroll down button
    $('#btScrollToBottom' + roomId).click(function () {
        var $chatHistory = $('.chat-history.chat-logs[data-id=' + roomId + ']');
        $chatHistory.scrollTop($chatHistory.prop('scrollHeight') - $chatHistory.prop('clientHeight'));
    });

    generateMagnificPopup($chatHistory);
}