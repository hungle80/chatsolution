﻿var planLogs = [];
function generatePlan(log) {
    var index = _.findIndex(planLogs, (item) => {
        return item._id == log._id;
    });
    if (index >= 0) {
        planLogs[index].content = log.content;
    }
    else {
        planLogs.push(log);
    }
    var html = '<div onclick="openPlanDetail(\'' + log._id + '\')">';
    html += '<div class="plan-message clearfix">';
    html += '<div class="message-header-wrap">';
    html += '<div class="message-header">';
    html += 'Đặt lịch';
    html += '</div>';
    html += '<div style="position: relative; display: flex; flex-direction: column; flex: 1 1 0%; overflow: hidden; align-items: stretch;"></div>';
    html += '<div class="day-to-come-wrap">';
    html += generatePlanDayToCome(log);
    html += '</div>';
    html += '</div>';
    html += '<div class="plan-body">';
    html += '<div class="plan-content">';
    html += '<div class="plan-title">';
    html += log.content.title;
    html += '</div>';
    html += '<div class="plan-timestamp">';
    html += moment(log.content.timeStamp * 1000).format('ddd DD MMMM HH:mm');
    html += '</div>';
    html += '<div class="plan-divide-wrap"><div class="plan-divide-top"></div><div class="plan-divide-line"></div><div class="plan-divide-bottom"></div></div>';
    html += '</div>';
    html += '<div class="plan-icon-wrap"><div></div></div>';
    html += '</div>';
    html += '<div class="plan-progress">';
    html += '<div class="plan-progress-sum">';
    html += generatePlanProgressSummary(log.content.result);
    html += '</div>';
    html += '<div class="progress">';
    html += generatePlanProgress(log.content.result, log.roomId);
    html += '</div>';
    html += '<div class="plan-progress-legends">';
    html += '<div class="plan-progress-legend"><div class="m-badge bg-success"></div> <div>Yes</div></div>';
    html += '<div class="plan-progress-legend"><div class="m-badge bg-danger"></div> <div>No</div></div>';
    html += '<div class="plan-progress-legend"><div class="m-badge bg-warning"></div> <div>Maybe</div></div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += generatePlanCommentCount(log);
    return html;
}

function generatePlanCommentCount(log) {
    var html = '';
    console.log(log.content.comments);
    if (log.content.comments && log.content.comments.length > 0) {
        html += '<div class="clearfix plan-comment">';
        html += '<div class="pull-right">';
        html += '<a onclick="showCommentPopup(\'' + log._id + '\')" id="btCommentPopup' + log._id + '" class="btn btn-xs btn-round"><span class="fa fa-comment"></span> ' + log.content.comments.length + '</a>';
        html += '</div>';
        html += '</div>';
    }
    return html;
}

function loadPlanDetail(log) {
    if (isSignedInGoogleForCalendar) {
        $('#divNotSigninGCal').hide();
    }
    else {
        $('#divNotSigninGCal').show();
    }
    $('#imgPDAvatar').prop('src', log.authorInfo.avatar);
    $('#divPDUserName').html(log.authorInfo.name);
    $('#divPDTitle').html(log.content.title);
    var time = moment(log.content.timeStamp * 1000).format('dddd, DD MMM, hh:mm A');
    currentLog = log;
    $('#divPDStatus').removeClass();
    var res = getPlanStatusLabel(log);
    $('#divPDStatus').addClass(res.bgDay);
    if (log.content.status == 'active') {
        $('#divPDStatus').html(getTimeToCome(log));
    }
    else {
        $('#divPDStatus').html(res.label);
    }
    if (log.content.note) {
        $('#divPDNote').html(log.content.note);
        $('#divPDNote').show();
    }
    else {
        $('#divPDNote').hide();
    }

    var currentMemberConfirm = _.find(log.content.result, function (item) {
        return item.userId == currentUserId;
    });

    if (log.content.status != 'active') {
        $('.btPDSelect').prop('disabled', true);
    }
    else {
        $('.btPDSelect').prop('disabled', false);
    }
    $('.btPDSelect').removeClass('active');

    if (currentMemberConfirm) {
        $('.btPDSelect[data-status=' + currentMemberConfirm.status + ']').addClass('active');
    }

    if (log.content.duration && log.content.duration > 0) {
        time += ' (' + moment.duration(log.content.duration, "minutes").humanize() + ')';
    }
    $('#divPDTimeStamp').html(time);
    if (log.content.place.address || log.content.place.lat || log.content.place.lng) {
        $('#divPDLocationWrap').show();
        var address = (log.content.place.address ? log.content.place.address : log.content.place.lat + ',' + log.content.place.lng);
        $('#divPDAddress').html(address);

        var mapUrl = 'https://maps.google.com/maps?';
        if (log.content.place.address) {
            mapUrl += 'q=' + log.content.place.address;
            if (log.content.place.lat && log.content.place.lng) {
                mapUrl += '&ll=' + log.content.place.lat + ',' + log.content.place.lng;
            }
        }
        else {
            if (log.content.place.lat && log.content.place.lng) {
                mapUrl += 'll=' + log.content.place.lat + ',' + log.content.place.lng;
            }
        }
        $('#divPDAddress').prop('href', mapUrl);
    }
    else {
        $('#divPDLocationWrap').hide();
    }
    if (log.content.comments.length > 0) {
        $('#divPDCommentCount').html(log.content.comments.length);
    }
    else {
        $('#divPDCommentCount').html('Bình luận');
    }

    loadPlanResponse(log);

    $('#divCommentPopup').click(function () {
        showCommentPopup(log._id);
    });

    $('#divInsightWrap').click(function () {
        showResponseResultPopup(log._id);
    });
}

function showResponseResultPopup(chatLogId) {
    var log = _.find(planLogs, (item) => {
        return item._id == chatLogId;
    });

    if (log) {
        currentLog = log;
        loadResponseResultList(log.content.result, log.roomId);
        $('#responseResultModal').showModal();
    }
}
function generateResponseItem(item, roomId) {
    var currentRoomMember = currentRoomMembers[roomId];

    var user = _.find(currentRoomMember, function (member) {
        return member.userId == item.userId;
    });

    var html = '<div class="comment-item clearfix" style="border-bottom: 1px solid #DDD;padding-bottom: 5px;margin-bottom: 5px;">';
    html += '<div class="pull-left">';
    html += '<img src="' + user.userInfo.avatar + '" class="avatar-chat circle" style="width:40px;height:40px">';
    html += '</div>';
    html += '<div class="pull-left" style="margin-left:8px;width:385px">';
    html += '<div class="bold">';
    html += user.userInfo.name;
    html += '</div>';
    html += '<div>';
    html += item.status;
    html += '</div>';
    html += '</div>';
    html += '</div>';
    return html;
}
function loadResponseResultList(result, roomId) {
    var html = '';
    _.each(result, function (item) {
        html += generateResponseItem(item, roomId);
    });
    $('#divResponseList').html(html);
}

function loadPlanResponse(log) {

    var currentRoomMember = currentRoomMembers[log.roomId];
    var totalMember = currentRoomMember.length;
    var responseMember = log.content.result.length;

    if (responseMember && responseMember > 0) {
        $('#divPDResponseCount').html(responseMember + ' / ' + totalMember);
        var yesR = _.filter(log.content.result, (item) => {
            return item.status == 'yes';
        });

        var noR = _.filter(log.content.result, (item) => {
            return item.status == 'no';
        });

        var maybeR = _.filter(log.content.result, (item) => {
            return item.status == 'maybe';
        });

        console.log(yesR, noR, maybeR);

        if (yesR) {
            $('#divYesWrap .response-result').html(yesR.length);
            var yesP = yesR.length / responseMember * 100;
            $('#divYesWrap .progress-bar').css('width', yesP + '%');
        }
        if (noR) {
            $('#divNoWrap .response-result').html(noR.length);
            var noP = noR.length / responseMember * 100;
            $('#divNoWrap .progress-bar').css('width', noP + '%');
        }
        if (maybeR) {
            $('#divMaybeWrap .response-result').html(maybeR.length);
            var maybeP = maybeR.length / responseMember * 100;
            $('#divMaybeWrap .progress-bar').css('width', maybeP + '%');
        }
    }
    else {
        $('#divPDResponseCount').html('0 / ' + totalMember);

        $('#divYesWrap .response-result').html('0');
        $('#divYesWrap .progress-bar').css('width', '0%');

        $('#divNoWrap .response-result').html('0');
        $('#divNoWrap .progress-bar').css('width', '0%');

        $('#divMaybeWrap .response-result').html('0');
        $('#divMaybeWrap .progress-bar').css('width', '0%');
    }

}

function showCommentPopup(chatLogId) {
    var log = _.find(planLogs, (item) => {
        return item._id == chatLogId;
    });

    if (log) {
        currentLog = log;
        loadCommentList(log.content.comments, log.roomId);
        $('#planCommentModel').showModal();
    }
}

function generateCommentItem(comment, roomId) {
    var html = '<div class="comment-item clearfix" style="border-bottom: 1px solid #DDD;padding-bottom: 5px;margin-bottom: 5px;">';
    html += '<div class="pull-left">';
    var currentRoomMember = currentRoomMembers[roomId];
    console.log(currentRoomMember);
    var user = _.find(currentRoomMember, function (member) {
        return member.userId == comment.userId;
    });
    console.log(user);
    html += '<img src="' + user.userInfo.avatar + '" class="avatar-chat circle" style="width:40px;height:40px">';
    html += '</div>';
    html += '<div class="pull-left" style="margin-left:8px;width:385px">';
    html += '<div class="bold">';
    html += user.userInfo.name;
    html += '</div>';
    html += '<div>';
    html += comment.message;
    html += '</div>';
    html += '<small class="text-muted" style="margin-top:2px">';
    html += moment(comment.createDate).format('DD MMM, YYYY, HH:mm');
    html += '</small>';
    html += '</div>';
    html += '</div>';
    return html;
}
var currentLog;
function loadCommentList(comments, roomId) {
    var html = '';
    _.each(comments, function (comment) {
        html += generateCommentItem(comment, roomId);
    });
    $('#divCommentList').html(html);
}
$(document).ready(function () {
    function sendComment() {
        var message = $('#btCommentText').val();
        if (message) {
            socket.emit('commentToPlan',
                {
                    roomId: currentLog.roomId,
                    chatLogId: currentLog._id,
                    message: message
                },
                function (result) {
                    $('#btCommentText').val('');
                    if (result.errorCode == 0) {
                        loadCommentList(result.data.content.comments, result.data.roomId);
                    }
                }
            );
        }
    }

    $('#btSendComment').click(function () {
        sendComment();
    });

    $('#btCommentText').bind('keydown', function (event) {

        var key = event.key ? event.key : event.keyIdentifier;
        //console.log(event);
        if (key == 'Enter' && event.shiftKey == false) {
            //console.log('enter');
            event.preventDefault();
            sendComment();
        }
    });

    $('.btPDSelect').click(function () {
        if (!$('.btPDSelect').prop('disabled')) {
            if (!$(this).hasClass('active')) {
                var status = $(this).data('status');
                $('.btPDSelect').removeClass('active');
                $(this).addClass('active');
                socket.emit('confirmToPlan',
                    {
                        roomId: currentLog.roomId,
                        chatLogId: currentLog._id,
                        status: status
                    },
                    function (result) {
                        console.log(result);
                        if (result.errorCode == 0) {
                            if (status == 'no') {
                                removeCalendarEvent(currentLog);
                            }
                            else {
                                addCalendarEvent(currentLog, status);
                            }
                            loadPlanResponse(result.data);
                        }
                    }
                );
            }
        }
    });
});

function openPlanDetail(chatLogId) {
    var log = _.find(planLogs, (item) => {
        return item._id == chatLogId;
    });
    console.log(log);
    if (log) {
        if (log.content.status != 'delete') {
            loadPlanDetail(log);
            $('#planDetailModal').showModal();
        }
    }
}

function getPlanStatusLabel(log) {
    var bgDay = '';
    var label = '';
    switch (log.content.status) {
        case 'active': {
            bgDay = 'bg-success';
            break;
        }
        case 'close': {
            bgDay = 'bg-default';
            label = 'Hoàn tất';
            break;
        }
        case 'delete': {
            bgDay = 'bg-danger';
            label = 'Đã huỷ';
            break;
        }
    }

    var res = { bgDay: bgDay, label: label };
    console.log(res);
    return res;
}

function getTimeToCome(log) {
    var html = 'Còn ';
    var minuteToGo = moment(log.content.timeStamp * 1000).diff(moment().utc(), 'minutes');
    if (minuteToGo >= 60) {
        var hourToGo = moment(log.content.timeStamp * 1000).diff(moment().utc(), 'hours');
        if (hourToGo >= 24) {
            var dayToGo = moment(log.content.timeStamp * 1000).diff(moment().utc(), 'days');
            if (dayToGo >= 30) {
                var monthToGo = moment(log.content.timeStamp * 1000).diff(moment().utc(), 'months');
                html += moment(log.content.timeStamp * 1000).diff(moment().utc(), 'months') + ' tháng';
            }
            else {
                html += moment(log.content.timeStamp * 1000).diff(moment().utc(), 'days') + ' ngày';
            }
        }
        else {
            html += moment(log.content.timeStamp * 1000).diff(moment().utc(), 'hours') + ' giờ';
        }
    }
    else {
        html += moment(log.content.timeStamp * 1000).diff(moment().utc(), 'minutes') + ' phút';
    }
    return html;
}

function generatePlanDayToCome(log) {
    var html = '';

    var res = getPlanStatusLabel(log);

    html += '<div class="day-to-come ' + res.bgDay + '">';
    if (res.label != '') {
        html += res.label;
    }
    else {
        html += getTimeToCome(log);
    }
    html += '</div>';
    return html;
}
function generatePlanProgress(result, roomId) {
    var html = '';
    var yes = 0, no = 0, maybe = 0;
    var total = currentRoomMembers[roomId].length;
    for (var i = 0; i < result.length; i++) {
        switch (result[i].status) {
            case 'yes': {
                yes++;
                break;
            }
            case 'no': {
                no++;
                break;
            }
            case 'maybe': {
                maybe++;
                break;
            }
        }
    }
    if (yes > 0) {
        var percent = yes / total * 100;
        html += '<div class="progress-bar bg-success" style="width:' + percent + '%"></div>';
    }
    if (no > 0) {
        var percent = no / total * 100;
        html += '<div class="progress-bar bg-danger" style="width:' + percent + '%"></div>';
    }
    if (maybe > 0) {
        var percent = maybe / total * 100;
        html += '<div class="progress-bar bg-warning" style="width:' + percent + '%"></div>';
    }
    return html;
}
function generatePlanProgressSummary(result) {
    var html = '';
    if (result.length == 0) {
        html += 'Đang chờ phản hồi';
    }
    else {
        var yes = 0, no = 0, maybe = 0;
        for (var i = 0; i < result.length; i++) {
            switch (result[i].status) {
                case 'yes': {
                    yes++;
                    break;
                }
                case 'no': {
                    no++;
                    break;
                }
                case 'maybe': {
                    maybe++;
                    break;
                }
            }

        }

        if (yes > 0) {
            html += 'Đồng ý: ' + yes;
        }
        if (no > 0) {
            if (html != '') {
                html += ', ';
            }
            html += 'Từ chối: ' + no;
        }
        if (maybe > 0) {
            if (html != '') {
                html += ', ';
            }
            html += 'Dự kiến: ' + maybe;
        }
    }
    return html;
}

function sendPlan(el) {
    //var addToGCalendar = $('#ckAddToGCalendar').prop('checked');
    //if (!isSignedInGoogleForCalendar && addToGCalendar) {
    //    handleAuthClick();
    //}
    //else {
    var roomId = $(el).data('room-id');
    var title = $('#tbAPTitle').val();
    var datetime = $('#tbAPDatetime').val();
    var note = $('#tbNote').val();

    if (!title || !datetime) {
        var msg = '';
        if (!title) {
            msg += 'Cần nhập tiêu đề.<br />';
        }
        if (!datetime) {
            msg += 'Cần nhập thời gian.';
        }
        console.log(msg);
        showNotifyError(msg);
    }
    else {

        var dDay = $('#tbAPDDay').val();
        var dHour = $('#tbAPDHour').val();
        var dMinute = $('#tbAPDMinute').val();

        var timeStamp = moment(datetime).unix();

        var duration = (parseInt(dDay) * 1440) + (parseInt(dHour) * 60) + (parseInt(dMinute));

        console.log(roomId, title, timeStamp, duration);

        closeSearchResult(roomId);
        var userId = currentUserId;
        var log = {
            type: messageType.plan,
            roomId: roomId,
            userIdAuthor: userId,
            itemGUID: generateGUID(),
            createDate: moment().utc().format(),
            authorInfo: currentUserInfo
        };

        var place = {
            lat: $('#tbAPLat').val(),
            lng: $('#tbAPLng').val(),
            address: $('#tbAPPlace').val()
        };

        var content = {
            title: title,
            timeStamp: timeStamp,
            duration: duration,
            place: place,
            note: note,
            result: [],
            comments: [],
            status: 'active',
            likes: []
        };

        log.content = content;
        var $chatHistory = getChatHistoryWrapper(roomId);
        sendRoomLog($chatHistory, log, true, function (result) {
            console.log(result);
            if (result.errorCode == 0) {
                addCalendarEvent(result.data, 'yes');
            }
        });
        clearAddPlanModal();
        $('#addPlanModal').closeModal();
        //}
    }
}
function clearAddPlanModal() {
    $('#tbAPTitle').val('');
    $('#tbAPDatetime').val('');
    $('#tbAPDDay').val(0);
    $('#tbAPDHour').val(1);
    $('#tbAPDMinute').val(0);
    $('#tbAPPlace').val('');
    $('#tbAPLat').val('');
    $('#tbAPLng').val('');
    $('#tbNote').val('');
    if (isSignedInGoogleForCalendar) {
        $('#notSigninGCal').hide();
    }
    else {
        $('#notSigninGCal').show();
    }
}
//$(document).ready(function () {
//    $('#tbAPPlace').click(function () {
//        console.log('place');
//        $('#placeChooserModal').showModal();
//    });
//});