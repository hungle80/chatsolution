﻿function checkIsImageUrl(url) {
    return (url.match(/\.(jpeg|jpg|gif|png|heic)(\?([A-Za-z_0-9&\-\+=]+)?)?$/i) != null);//kèm luôn parameter trong link image
}
function checkIsVideoUrl(url) {
    return (url.match(/\.(mp4|avi|ogv|webm)(\?([A-Za-z_0-9&\-\+=]+)?)?$/i) != null);//kèm luôn parameter trong link image
}
function checkIsAudioUrl(url) {
    return (url.match(/\.(mp3|ogg)(\?([A-Za-z_0-9&\-\+=]+)?)?$/i) != null);//kèm luôn parameter trong link image
}
function checkIsYoutubeUrl(url) {
    var p = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
    return (url.match(p)) ? RegExp.$1 : false;
}
function htmlEscape(str) {
    return str.replace(/</g, '&lt;').replace(/>/g, '&gt;');
}
function detectLinkIsMedia(str) {
    var mediaType = '';
    if (checkIsImageUrl(str)) {
        mediaType = 'image';
    }
    else if (checkIsVideoUrl(str)) {
        mediaType = 'video';
    }
    else if (checkIsAudioUrl(str)) {
        mediaType = 'audio';
    }
    else if (checkIsYoutubeUrl(str)) {
        mediaType = 'youtube';
    }
    return mediaType;
}
function replaceTextWithYouTube(e) {
    var regex = /(?:http|https|)(?::\/\/|)(?:www.|)(?:youtu\.be\/|youtube\.com(?:(?!\/playlist))(?:\/embed\/|\/v\/|\/watch\?v=|\/ytscreeningroom\?v=|\/feeds\/api\/videos\/|\/user\S*[^\w\-\s]|\S*[^\w\-\s]))([\w\-]{11})[a-z0-9;:@#?&%=+\/\$_.-]*/mig;
    var regexPL = /^(?:https?:\/\/)?(?:www\.)?youtu\.?be(?:\.com)?(\/playlist).*?(?:list)=PL(.*?)(?:&|$)/ig;
    var regexPL2 = /^(?:https?:\/\/)?(?:www\.)?youtu\.?be(?:\.com)?(\/playlist).*?(?:list)=(.*?)(?:&|$)/ig;
    var html = e.replace(regex, '<iframe class="youtube-player" type="text/html" style="max-width:100%;width:100%;min-height:300px" src="https://www.youtube.com/embed/$1" frameborder="0"></iframe>');
    html = html.replace(regexPL, '<iframe class="youtube-player" type="text/html" style="max-width:100%;width:100%;min-width:500px;min-height:300px" src="https://www.youtube.com/embed/videoseries?list=PL$2" frameborder="0"></iframe>');
    html = html.replace(regexPL2, '<iframe class="youtube-player" type="text/html" style="max-width:100%;width:100%;min-width:500px;min-height:300px" src="https://www.youtube.com/embed/videoseries?list=PL$2" frameborder="0"></iframe>');
    return html;
}
function replaceWordToEmoji(str) {
    if (str) {
        var smile = /(\s:\)\s|^:\)\s|\s:\)$|^:\)$)/g;
        str = str.replace(smile, ' 😃 ');

        var smiling = /(\s:\)\)\s|^:\)\)\s|\s:\)\)$|^:\)\)$)/g;
        str = str.replace(smiling, ' 😄 ');

        var rolling = /(\s=\)\)\s|^=\)\)\s|\s=\)\)$|^=\)\)$)/g;
        str = str.replace(rolling, ' 🤣 ');

        var grin = /(\s:D\s|^:D\s|\s:D$|^:D$)/g;
        str = str.replace(grin, ' 😁 ');

        var sad = /(\s:\(\s|^:\(\s|\s:\($|^:\($)/g;
        str = str.replace(sad, ' 😔 ');

        var cry = /(\s:\(\(\s|^:\(\(\s|\s:\(\($|^:\(\($)/g;
        str = str.replace(cry, ' 😭 ');

        var wink = /(\s;\)\s|^;\)\s|\s;\)$|^;\)$)/g;
            str = str.replace(wink, ' 😉 ');
    }
    return str;
}
function replaceAtMentionsWithLinks(e) {
    var t = e.replace(/@\[([\s\S\d _][^\]]+)\]\(userid:([\d]+)\)/ig, '<a class="mention-user" href="javascript:;" onclick="createPrivateChatRoom($2)" data-userid="$2">@$1</a>');
    return t;
} 
function parseMessageLink(e, _32only) {
    var t = e;
    t = htmlEscape(e);
    t = replaceWordToEmoji(t);
    t = replaceTextWithEmailAddress(t);
    t = replaceTextWithUrl(t);
    t = replaceEmoji(t, _32only);
    t = replaceAtMentionsWithLinks(t);
    return t;
}

function parseMessage(e, _32only) {
    var t = e;
    t = htmlEscape(e);
    t = replaceWordToEmoji(t);
    t = replaceEmoji(t, _32only);
    t = replaceAtMentionsWithLinks(t);
    return t;
}
function replaceTextWithUrl(e) {
    var t = /((href|src)=["']|)(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
    return e.replace(t, function () {
        return arguments[1] ? arguments[0] : "<a target='_blank' href=\"" + arguments[3] + '">' + decodeURI(arguments[3]) + "</a>";
    });
}

function extractFirstLinkFromText(e) {
    var t = /((href|src)=["']|)(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
    var matches = e.match(t);
    if (matches) {
        return matches[0];
    }
    return '';
}

function replaceTextWithEmailAddress(e) {
    if (e) {
        var t = /([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})/;
        var n = e.replace(t, '<a href="mailto:$1@$2.$3">$1@$2.$3</a>');
        return n;
    }
    return '';
}


function escapeQuote(e) {
    if (e) {
        return e.replace(/"/g, '&quot;').replace(/'/g, '&#39;');
    }
    return '';
}

var lut = [];
for (var i = 0; i < 256; i++) {
    lut[i] = (i < 16 ? '0' : '') + (i).toString(16);
}
function generateGUID() {
    var d0 = Math.random() * 0xffffffff | 0;
    var d1 = Math.random() * 0xffffffff | 0;
    var d2 = Math.random() * 0xffffffff | 0;
    var d3 = Math.random() * 0xffffffff | 0;
    return lut[d0 & 0xff] + lut[d0 >> 8 & 0xff] + lut[d0 >> 16 & 0xff] + lut[d0 >> 24 & 0xff] + '-' +
        lut[d1 & 0xff] + lut[d1 >> 8 & 0xff] + '-' + lut[d1 >> 16 & 0x0f | 0x40] + lut[d1 >> 24 & 0xff] + '-' +
        lut[d2 & 0x3f | 0x80] + lut[d2 >> 8 & 0xff] + '-' + lut[d2 >> 16 & 0xff] + lut[d2 >> 24 & 0xff] +
        lut[d3 & 0xff] + lut[d3 >> 8 & 0xff] + lut[d3 >> 16 & 0xff] + lut[d3 >> 24 & 0xff];
}

function compound2Unicode(str) {
    return str.normalize('NFKC');
}

function replaceSpecialCharacter(str) {
    str = str.replace(/\+/g, ' ');
    str = str.replace(/&/g, ' ');
    return str;
}
function checkEmojiOnly(str) {
    const ranges = [
        '\ud83c[\udf00-\udfff]', // U+1F300 to U+1F3FF
        '\ud83d[\udc00-\ude4f]', // U+1F400 to U+1F64F
        '\ud83d[\ude80-\udeff]', // U+1F680 to U+1F6FF
        '\ud83e[\udd00-\uddff]',
        ' ', // Also allow spaces
    ].join('|');

    const removeEmoji = str => str.replace(new RegExp(ranges, 'g'), '');
    const isOnlyEmojis = str => !removeEmoji(str).length;
    return isOnlyEmojis(str);
}
function replaceEmoji(str, _32only) {
    if (str) {
        
        str = emojione.shortnameToUnicode(str);
        var isOnlyEmojis = checkEmojiOnly(str);
        str = emojione.unicodeToImage(str);
        if (isOnlyEmojis && !_32only) {
            str = str.replace(/\/32\//g, '/64/');
        }
    }
    //console.log(str);
    return str;
}
function pad(number, length) {
    var str = '' + number;
    while (str.length < length) {
        str = '0' + str;
    }
    return str;
}

function formatBytes(bytes, decimals) {
    if (bytes == 0) return '0 Byte';
    var k = 1024;
    var dm = decimals + 1 || 3;
    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    var i = Math.floor(Math.log(bytes) / Math.log(k));
    return (bytes / Math.pow(k, i)).toPrecision(dm) + ' ' + sizes[i];
}

function getFileExtension(fileName) {
    var index = fileName.lastIndexOf('.') + 1;
    if (index == 0) {
        return '';
    }
    return fileName.substring(index).toLowerCase();
}


function insertAtCursor(myField, myValue) {
    //IE support
    if (document.selection) {
        myField.focus();
        sel = document.selection.createRange();
        sel.text = myValue;
    }
    //MOZILLA and others
    else if (myField.selectionStart || myField.selectionStart == '0') {

        var startPos = myField.selectionStart;
        var endPos = myField.selectionEnd;
        myField.value = myField.value.substring(0, startPos) +
            myValue +
            myField.value.substring(endPos, myField.value.length);

        myField.selectionStart = (startPos + myValue.length - 1) > myField.value.length ? myField.value.length : (startPos + myValue.length);
        myField.selectionEnd = (endPos + myValue.length - 1) > myField.value.length ? myField.value.length : (endPos + myValue.length);


    }
    else {
        myField.value += myValue;
    }

    $(myField).trigger('change');
    $(myField).trigger('input');
    $(myField).focus();
}
function removeAccent(str) {
    return str.normalize('NFD').replace(/[\u0300-\u036f]/g, "").replace(/Đ/g, "D").replace(/đ/g, "d");
}
function searchInString(str, key) {
    str = str.toLowerCase();

    key = key.toLowerCase();
    key = removeAccent(key);
    console.log(str);
    console.log(key);
    if (str.indexOf(key) != -1)
        return true;

    str = removeAccent(str);

    if (str.indexOf(key) != -1)
        return true;

    return false;
}

function formatCurrencyVND(amount) {
    var formatter = new Intl.NumberFormat('de-DE', {
        style: 'currency',
        currency: 'VND',
        minimumFractionDigits: 0,
    });
    amount = parseInt(amount);
    return formatter.format(amount);
}

(function ($) {
    $.fn.hasScrollBar = function () {
        return this.get(0).scrollHeight > this.height();
    };
})(jQuery);

var intervalPageTitleHandle;
function blinkPageTitle(title, time) {
    clearInterval(intervalPageTitleHandle);
    var titleEmpty = '🔔 ChatNhanh';

    function blink() {
        if (document.title == titleEmpty) {
            document.title = title;
        }
        else {
            document.title = titleEmpty;
        }
    }
    intervalPageTitleHandle = setInterval(blink, time);
}

function unBlinkPageTitle(title) {
    clearInterval(intervalPageTitleHandle);
    document.title = title;
}

function decodeURITryCatch(str) {
    var decode;
    try {
        decode = decodeURIComponent(str);
    }
    catch (ex) {
        decode = str;
    }
    return decode;
}