﻿var updatingChannelId;
function revokeUpdateChannelLink() {
    socket.emit('generatePrivateJoinLink', {}, function (result) {
        //console.log(result);
        if (result.errorCode == 0) {
            $('#updateChannelPrivateJoinLink').html(result.data);
            $('#updateChannelPrivateJoinLink').prop('href', result.data);
        }
    });
}
function loadingChannelInfoForUpdate(roomId, callback) {
    updatingChannelId = roomId;
    socket.emit('getChatRoom', { roomId: roomId }, function (result) {
        console.log(result);
        if (result.errorCode == 0) {
            var info = result.data;
            console.log(info);
            $('#tbUpdateChannelName').parent().find('label').addClass('active');     
            $('#tbUpdateChannelName').val(info.roomName);
            if (info.description) {
                $('#tbUpdateChannelDescription').parent().find('label').addClass('active');
            }
            $('#tbUpdateChannelDescription').val(info.description);
            if (info.isPrivate) {
                $('#ckUpdateChannelPublicChannel').prop('checked', false);
                $('#ckUpdateChannelPrivateChannel').prop('checked', true);
                $('#updateChannelPublicLinkWrapper').addClass('hide');
                $('#updateChannelPrivateLinkWrapper').addClass('hide');//ẩn luôn link khi set private

                $('#updateChannelPrivateJoinLink').html(info.joinLink);
                $('#updateChannelPrivateJoinLink').prop('href', info.joinLink);

                $('#tbUpdateChannelPublicLink').val('');                
            }
            else {
                $('#ckUpdateChannelPublicChannel').prop('checked', true);
                $('#ckUpdateChannelPrivateChannel').prop('checked', false);
                $('#updateChannelPublicLinkWrapper').removeClass('hide');
                $('#updateChannelPrivateLinkWrapper').addClass('hide');
                if (info.joinLink) {
                    $('#tbUpdateChannelPublicLink').parent().find('label').addClass('active');     
                }
                $('#tbUpdateChannelPublicLink').val(info.joinLink);
                revokeUpdateChannelLink();
            }
            $('#ckUpdateChannelSignMessage').prop('checked', info.signMessage);
        }
    });
}

function showEditChannelModal(roomId) {
    $('#updateChannelModal').showModal();
    loadingChannelInfoForUpdate(roomId, function () {
    });
}
$(document).ready(function () {
    $('#ckUpdateChannelPublicChannel').click(function () {
        $('#updateChannelPublicLinkWrapper').removeClass('hide');
        $('#updateChannelPrivateLinkWrapper').addClass('hide');
    });
    
    $('#ckUpdateChannelPrivateChannel').click(function () {
        $('#updateChannelPublicLinkWrapper').addClass('hide');
        $('#updateChannelPrivateLinkWrapper').addClass('hide');//ẩn luôn link khi set private
    });

    $('#btRevokeUpdateChannelLink').click(function () {
        revokeUpdateChannelLink();
    });

    $('#tbUpdateChannelName').keyup(function (e) {
        if ($('#tbUpdateChannelName').val() != '') {
            $('#tbUpdateChannelName').parent().find('label').removeClass('error');
            $('#tbUpdateChannelName').removeClass('error');
        }
    });

    function checkUpdateChannelInput() {
        var ok = true;
        if ($('#tbUpdateChannelName').val() == '') {
            ok = false;
            $('#tbUpdateChannelName').parent().find('label').addClass('error');
            $('#tbUpdateChannelName').addClass('error');
        }
        return ok;
    }
    $('#btUpdateChannel').click(function () {
        if (checkUpdateChannelInput()) {
            //
            var isPrivate = $('#ckUpdateChannelPrivateChannel').prop('checked');
            console.log(isPrivate);
            var joinLink = isPrivate ? $('#updateChannelPrivateJoinLink').html() : $('#tbUpdateChannelPublicLink').val();
            var data = {
                roomId: updatingChannelId,
                roomName: $('#tbUpdateChannelName').val().trim(),                
                description: $('#tbUpdateChannelDescription').val().trim(),                
                isPrivate: isPrivate,
                joinLink: joinLink,
                signMessage: $('#ckUpdateChannelSignMessage').prop('checked')
            };
            socket.emit('updateChannel', data, function (result) {
                console.log(result);
                if (result.errorCode == 0) {
                    if (roomInfos[data.roomId]) {
                        roomInfos[data.roomId].roomName = data.roomName;
                        roomInfos[data.roomId].description = data.description;
                        roomInfos[data.roomId].isPrivate = data.isPrivate;
                        roomInfos[data.roomId].joinLink = data.joinLink;
                        roomInfos[data.roomId].signMessage = data.signMessage;
                    }
                    $('#updateChannelModal').closeModal();                    
                }
                else {
                    showNotifyError('Lỗi xảy ra khi update kênh chat');
                    console.log(result);
                }
            });
        }
    });

    $('#btCancelUpdateChannel').click(function () {
        $('#updateChannelModal').closeModal();
    });
});