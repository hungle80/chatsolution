﻿var channelAdministratorRoomId;
var channelPermissionMemberId;
var isChannelPermission = true;
function showChannelAdministratorModal(roomId, isChannel) {
    channelAdministratorRoomId = roomId;
    isChannelPermission = isChannel;
    $('#channelAdministratorModal').showModal();
    loadingChannelAdministratorList(roomId, function () {
    });
}
function generateAdminMember(member, roomId) {
    var html = '<li class="member padding5 member-admin" data-id="' + member.userId + '" data-room-id="' + roomId + '">';
    html += '<div class="avatar-wrapper">';
    html += '<img width="50" height="50" class="circle avatar-chat" src="' + member.userInfo.avatar + '">';
    html += '</div >';
    html += '<div>';
    html += '<div class="item-name" data-email="' + member.userInfo.email + '" data-mobile="' + member.userInfo.phone + '">' + member.userInfo.name + '</div>';
    html += '<div class="author-info">' + (member.isOwner ? 'Chủ nhóm' : 'Admin') + '</div>';
    html += '<div class="member-button-wrapper">';
    if (!member.isOwner) {
        html += '<a class="button-link btRemove" onclick="removeChannelAdmin(' + member.userId + ', \'' + roomId + '\');">Xoá</a>';
    }
    html += '</div >';
    html += '</div >';
    html += '</li>';
    return html;
}
function removeChannelAdmin(userId, roomId) {
    var data = {
        userId: userId,
        roomId: roomId
    };
    socket.emit('removeChannelAdmin', data, function (result) {
        if (result.errorCode == 0) {
            $('#divChannelAdminList li[data-id=' + data.userId + ']').remove();
            $('#channelAdminPermissionModal').closeModal();
        }
        else {
            showNotifyError(result.error);
        }
    });
}
function generateAddAdminMember(member, roomId) {
    var html = '<li class="member padding5 member-admin" data-id="' + member.userId + '" data-room-id="' + roomId + '">';
    html += '<div class="avatar-wrapper">';
    html += '<img width="50" height="50" class="circle avatar-chat" src="' + member.userInfo.avatar + '">';
    html += '</div >';
    html += '<div>';
    html += '<div class="item-name" data-email="' + member.userInfo.email + '" data-mobile="' + member.userInfo.phone + '">' + member.userInfo.name + '</div>';
    html += '</div >';
    html += '</li>';
    return html;
}
function loadingChannelAdministratorList(roomId, callback) {
    channelAdministratorRoomId = roomId;
    //divChannelAdminList
    socket.emit('getChatRoom', {
        roomId: roomId
    }, function (result) {
        if (result.errorCode == 0) {
            var info = result.data;
            roomInfos[roomId] = info;
            var adminMembers = info.members.filter(function (x) { return x.isOwner || x.isAdmin; });
            console.log(adminMembers);
            var html = '';
            adminMembers.forEach(function (member) {
                html += generateAdminMember(member, roomId);
            });
            $('#divChannelAdminList ul').html(html);
        }
    });
}
function loadChannelMemberPermissions(userId, roomId) {
    channelAdministratorRoomId = roomId;
    channelPermissionMemberId = userId;
    socket.emit('getChatRoom', {
        roomId: roomId
    }, function (result) {
        if (result.errorCode == 0) {
            var info = result.data;
            roomInfos[roomId] = info;
            var member = info.members.find(function (x) { return x.userId == userId; });
            var permissions = member.permissions;
            if (!permissions) {
                permissions = {
                    editInfo: false,
                    postMessage: false,
                    editMessageOfOthers: false,
                    deleteMessageOfOthers: false,
                    addUsers: false,
                    addNewAdmins: false,
                    pinMessage: false
                };
            }
            $('#channelPermissionMemberAvatar').prop('src', member.userInfo.avatar);
            $('#channelPermissionMemberName').html(member.userInfo.name);
            $('#channelPermissionMemberRole').html(member.isOwner ? 'Chủ nhóm' : 'Admin');
            
            $('#cpChangeInfo').prop('disabled', member.isOwner);
            $('#cpPostMessage').prop('disabled', member.isOwner);
            $('#cpEditMessageOfOthers').prop('disabled', member.isOwner);
            $('#cpDeleteMessageOfOthers').prop('disabled', member.isOwner);
            $('#cpAddUsers').prop('disabled', member.isOwner);
            $('#cpAddNewAdmins').prop('disabled', member.isOwner);

            $('.per').addClass('hide');
            $('#cpChangeInfo').prop('checked', permissions.editInfo);
            if (isChannelPermission) {
                $('.per.channelPer').removeClass('hide');
                $('#cpPostMessage').prop('checked', permissions.postMessage);
                $('#cpEditMessageOfOthers').prop('checked', permissions.editMessageOfOthers);
                $('#cpDeleteMessageOfOthers').prop('checked', permissions.deleteMessageOfOthers);
            }
            else {
                $('.per.groupPer').removeClass('hide');
                //$('#cpDeleteMessage').prop('checked', permissions.deleteMessage);
            }
            $('#cpAddUsers').prop('checked', permissions.addUsers);
            $('#cpAddNewAdmins').prop('checked', permissions.addNewAdmins);
        }
    });
}
$(document).on('click', '.member-admin', function (e) {
    console.log('clickkkk');
    var userId = $(this).data('id');
    var roomId = $(this).data('room-id');
    console.log($(e.target));
    if (!$(e.target).hasClass('btRemove')) {
        console.log('load');
        $('#channelAdminPermissionModal').showModal();
        loadChannelMemberPermissions(userId, roomId);
    }
});
function loadChannelAddAdminList() {
    var roomId = channelAdministratorRoomId;
    socket.emit('getChatRoom', { roomId: roomId }, function (result) {
        if (result.errorCode == 0) {
            var roomInfo = result.data;
            roomInfos[roomId] = info;
            var roomMembers = roomInfo.members.filter(function (x) {
                return !x.isOwner;
            });
            var html = '';
            roomMembers.forEach(function (member) {
                html += generateAddAdminMember(member, roomId);
            });
            $('#divChannelAddAdminList ul').html(html);
        }
    });
}
$(document).ready(function () {
    $('#btCloseChannelAdminList').click(function () {
        $('#channelAdministratorModal').closeModal();
    });

    //add admin modal
    $('#btAddChannelAdminModal').click(function () {
        loadChannelAddAdminList();
        $('#channelAddAdminModal').showModal();
    });

    $('#btCloseChannelAddAdmin').click(function () {
        $('#channelAddAdminModal').closeModal();
    });

    //permissions
    $('#btCloseChannelAdminPermission').click(function () {
        $('#channelAdminPermissionModal').closeModal();
    });

    $('#btSaveChannelAdminPermission').click(function () {
        var permissions;
        if (isChannelPermission) {
            permissions = {
                editInfo: $('#cpChangeInfo').prop('checked'),
                postMessage: $('#cpPostMessage').prop('checked'),
                editMessageOfOthers: $('#cpEditMessageOfOthers').prop('checked'),
                deleteMessageOfOthers: $('#cpDeleteMessageOfOthers').prop('checked'),
                addUsers: $('#cpAddUsers').prop('checked'),
                addNewAdmins: $('#cpAddNewAdmins').prop('checked')
            };
        }
        else {
            permissions = {
                editInfo: $('#cpChangeInfo').prop('checked'),
                deleteMessageOfOthers: $('#cpDeleteMessageOfOthers').prop('checked'),                
                addUsers: $('#cpAddUsers').prop('checked'),
                addNewAdmins: $('#cpAddNewAdmins').prop('checked')
            };
        }
        var data = {
            roomId: channelAdministratorRoomId,
            userId: channelPermissionMemberId
        };
        var hasAnyPermission = permissions.editInfo || permissions.postMessage || permissions.editMessageOfOthers || permissions.deleteMessageOfOthers || permissions.addUsers || permissions.addNewAdmins;
        if (!hasAnyPermission) { //ko có quyền gì hết thì sẽ set tự lại ko là admin nữa
            removeChannelAdmin(data.userId, data.roomId);
        }
        else {
            data.permissions = permissions;
            socket.emit('setChannelAdmin', data, function (result) {
                if (result.errorCode == 0) {
                    loadingChannelAdministratorList(data.roomId);
                    $('#channelAdminPermissionModal').closeModal();
                }
                else {
                    showNotifyError(result.error);
                }
            });
        }
    });
});