﻿$(document).ready(function () {
    $('.btCreateChannel').click(function () {
        resetCreateChannelInput();
        socket.emit('generatePrivateJoinLink', {}, function (result) {
            console.log(result);
            if (result.errorCode == 0) {
                $('#createChannelPrivateJoinLink').html(result.data);
                $('#createChannelPrivateJoinLink').prop('href', result.data);
            }
        });
        $('#createChannelModal').showModal();
    });

    $('#ckCreateChannelPublicChannel').click(function () {
        $('#createChannelPublicLinkWrapper').removeClass('hide');
        $('#createChannelPrivateLinkWrapper').addClass('hide');
    });

    $('#ckCreateChannelPrivateChannel').click(function () {
        $('#createChannelPublicLinkWrapper').addClass('hide');
        $('#createChannelPrivateLinkWrapper').addClass('hide'); //ẩn luôn khi chọn private
    });

   
    function checkCreateChannelInput() {
        var ok = true;
        if ($('#tbCreateChannelName').val() == '') {
            ok = false;
            $('#tbCreateChannelName').parent().find('label').addClass('error');
            $('#tbCreateChannelName').addClass('error');
        }
        return ok;
    }

    function resetCreateChannelInput() {
        $('#tbCreateChannelName').val('');
        $('#tbCreateChannelDescription').val('');
        $('#ckCreateChannelPublicChannel').click();
        $('#ckCreateChannelSignMessage').click();
    }

    $('#tbCreateChannelName').keyup(function (e) {
        if ($('#tbCreateChannelName').val() != '') {
            $('#tbCreateChannelName').parent().find('label').removeClass('error');
            $('#tbCreateChannelName').removeClass('error');
        }
    });

    $('#btCreateChannel').click(function () {
        if (checkCreateChannelInput()) {
            //
            var isPrivate = $('#ckCreateChannelPrivateChannel').prop('checked');
            console.log(isPrivate);
            var joinLink = isPrivate ? $('#createChannelPrivateJoinLink').html() : $('#tbCreateChannelPublicLink').val();
            var data = {
                roomName: $('#tbCreateChannelName').val().trim(),
                roomAvatar: '',
                description: $('#tbCreateChannelDescription').val().trim(),
                members: [],
                isPrivate: isPrivate,
                joinLink: joinLink,
                signMessage: $('#ckCreateChannelSignMessage').prop('checked')
            }; 
            socket.emit('createChannel', data, function (result) {
                console.log(result);
                if (result.errorCode == 0) {
                    resetCreateChannelInput();
                    $('#createChannelModal').closeModal();
                    creatingChatRoomId = result.data._id;
                    var item = result.data;

                    var $item = $('ul#divRecentChat > li[data-id=' + item._id + ']');
                    //showChatRoom(item, function () {
                        if ($item.length > 0) {
                            setRecentChatRoomSelected($item);
                        }
                    //    setChatIsViewed(item._id);
                    //});
                    //creatingChatRoomId = '';
                }
                else {
                    showNotifyError('Lỗi xảy ra khi tạo kênh chat');
                    console.log(result);
                }
            });
        }
    });

    $('#btCancelCreateChannel').click(function () {
        $('#createChannelModal').closeModal();
    });
});