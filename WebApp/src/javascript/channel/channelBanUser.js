﻿var channelAddBanUserRoomId;
function showChannelBanUserModal(roomId) {
    channelAddBanUserRoomId = roomId;
    $('#channelBanUserModal').showModal();
    loadChannelBanUsers(roomId, function () {
    });
}
function loadChannelBanUsers(roomId) {
    socket.emit('getRoomBannedUser', { roomId: roomId }, function (result) {
        console.log(result);
        var html = '';
        if (result.errorCode == 0) {
            var members = result.data;
            if (members) {
                members.forEach(function (member) {
                    html += generateBannedMember(member, roomId);
                });
            }
        }
        $('#divBanChannelUserList ul').html(html);
    });
}

function generateBannedMember(member, roomId) {
    var html = '<li class="member padding5" data-id="' + member.userId + '" data-room-id="' + roomId + '">';
    html += '<div class="avatar-wrapper">';
    html += '<img width="50" height="50" class="circle avatar-chat" src="' + member.userInfo.avatar + '">';
    html += '</div >';
    html += '<div>';
    html += '<div class="item-name" data-email="' + member.userInfo.email + '" data-mobile="' + member.userInfo.phone + '">' + member.userInfo.name + '</div>';
    html += '<div class="member-button-wrapper">';
    html += '<a class="button-link btUnBan" onclick="removeBan(' + member.userId + ', \'' + roomId + '\');">Unblock</a>';
    html += '</div >';
    html += '</div >';
    html += '</li>';
    return html;
}
function generateAddBanMember(member, roomId) {
    var html = '<li class="member padding5 member-ban" data-id="' + member.userId + '" data-room-id="' + roomId + '">';
    html += '<div class="avatar-wrapper">';
    html += '<img width="50" height="50" class="circle avatar-chat" src="' + member.userInfo.avatar + '">';
    html += '</div >';
    html += '<div>';
    html += '<div class="item-name" data-email="' + member.userInfo.email + '" data-mobile="' + member.userInfo.phone + '">' + member.userInfo.name + '</div>';
    html += '</div >';
    html += '</li>';
    return html;
}
function loadChannelAddBanList() {
    var roomId = channelAddBanUserRoomId;
    socket.emit('getChatRoom', { roomId: roomId }, function (result) {
        if (result.errorCode == 0) {
            var roomInfo = result.data;
            roomInfos[roomId] = info;
            var roomMembers = roomInfo.members.filter(function (x) {
                return !x.isOwner;
            });
            var html = '';
            roomMembers.forEach(function (member) {
                html += generateAddBanMember(member, roomId);
            });
            $('#divChannelAddBanList ul').html(html);
        }
    });
}
$(document).on('click', '.member-ban', function (e) {
    console.log('clickkkk');
    var $this = $(this);
    confirmNotify('Có muốn Ban và mời user này ra khỏi channel ko?', function (ok) {
        console.log(ok);
        if (ok) {            
            var userId = $this.data('id');
            var roomId = $this.data('room-id');
            socket.emit('setBanUser', { roomId: roomId, userId: userId, isBanned: true }, function (res) {
                if (res.errorCode == 0) {
                    loadChannelBanUsers(roomId);
                    $('#channelAddBanModal').closeModal();
                }
            });
        }
    });
});

function removeBan(userId, roomId) {
    confirmNotify('Có muốn gỡ Ban user này ko?', function (ok) {
        if (ok) {
            var data = {
                userId: userId,
                roomId: roomId,
                isBanned: false
            };
            socket.emit('setBanUser', data, function (res) {
                if (res.errorCode == 0) {
                    $('#divBanChannelUserList .member[data-id=' + userId + ']').remove();
                }
            });
        }
    });
}
$(document).ready(function () {

    $('#btCloseBanChannelUser').click(function () {
        $('#channelBanUserModal').closeModal();
    });

    $('#btBanChannelUserModal').click(function () {
        loadChannelAddBanList();
        $('#channelAddBanModal').showModal();
    });

    $('#btCloseChannelAddBan').click(function () {
        $('#channelAddBanModal').closeModal();
    });
});