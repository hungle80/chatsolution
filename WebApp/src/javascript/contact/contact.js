﻿var $select2AddContactSearch;

function generateContactItem(contact) {
    var html = '';
    html += '<li class="padding5 member pointer" data-id="' + contact._id + '">';
    html += '<div class="avatar-wrapper">';
    html += '<img width="50" height="50" class="circle avatar-chat" src= "' + contact.avatar + '" />';    
    html += '</div>';
    html += '<div>';
    html += '<div class="item-name" data-email="' + contact.email + '" data-mobile="' + contact.phone + '">' + contact.name + '</div>';
    html += '<div class="author-info">' + contact.phone + '</div>';
    html += '</div>';
    html += '<div class="member-button-wrapper">';
    html += '<a class="button-link btRemove" onclick="removeContact(' + contact._id + ');">Xoá</a>';
    html += '</div>';
    html += '</li>';
    return html;
}
function removeContact(contactId) {
    socket.emit('removeContact', { contacts: [contactId] }, function (result) {
        if (result.errorCode == 0) {
            $('#divContactList li[data-id=' + contactId + ']').remove();
        }
        else {
            showNotifyError('Lỗi xảy ra khi xoá contact');
        }
    });
}
function loadContact() {
    socket.emit('getContactList', { pageIndex: 0, itemPerPage: 20 },
        function (result) {
            console.log(result);
            if (result.errorCode == 0) {
                var html = '';
                var contacts = result.data;
                if (contacts) {
                    contacts.forEach(function (contact) {
                         html += generateContactItem(contact);                        
                    });                    
                }
                $('#divContactList ul').html(html);
            }
        });
}

$(document).ready(function () {
    $select2AddContactSearch = $('#tbAddContactInput').select2({
        ajax: {
            url: 'https://api.muabannhanh.com/v2/user/search',
            dataType: 'json',
            data: function (params) {
                var query = {
                    q: params.term,
                    type: 'public',
                    limit: 20,
                    page: 1,
                    session_token: 'cb2663ce82a9f4ba448ba435091e27bb'
                };
                console.log(query);
                // Query parameters will be ?search=[term]&type=public
                return query;
            },
            processResults: function (data) {
                console.log(data);
                var items = [];
                for (var i = 0; i < data.result.length; i++) {
                    data.result[i].text = data.result[i].name + ' (' + data.result[i].phone + ')';
                    items.push(data.result[i]);
                }
                // Tranforms the top-level key of the response object from 'items' to 'results'                
                return {
                    results: items
                };
            },
            delay: 250
        },
        templateResult: generateSelect2Result,
        templateSelection: generateSelect2ResultSelection
    });
    function generateSelect2Result(user) {
        console.log(user);
        if (!user.id) {
            return user.text;
        }
        var $user = $('<img class="avatar-chat-search" src="' + user.avatar_url + '">' +
            '<b>' + user.name + '</b>' + '<span class="search-member-info"><i class="fa fa-phone mr5"></i>' + user.phone + (user.email ? '<i class="fa fa-envelope mr5" style="margin-left:20px;"></i>' + user.email : '') + '</span>');
        return $user;
    }
    function generateSelect2ResultSelection(user) {
        console.log(user);
        if (!user.id) {
            return user.text;
        }
        var $user = $('<img class="avatar-chat-search" style="margin-left:5px" src="' + user.avatar_url + '">' +
             '<b>' + user.name + '</b>');
        return $user;
    }
    function addContacts(contacts) {
        //console.log('pre addcontact');
        socket.emit('addContact', { contacts: contacts }, function (result) {
            //console.log('addcontact');
            //console.log(result);
            if (result.errorCode == 0) {
                $('#addConctactModal').closeModal();
                $('#tbAddContactInput').val(null).trigger('change');
                loadContact();
            }
            else {
                showNotifyError('Lỗi xảy ra khi thêm contact');
            }
        });
    }
    
    $('#btSaveAddContact').click(function () {
        var memberRaws = $('#tbAddContactInput').select2('data');
        var contacts = [];
        for (var i = 0; i < memberRaws.length; i++) {
            var member = memberRaws[i];
            contacts.push(parseInt(member.id));
        }
        if (contacts.length > 0) {
            addContacts(contacts);
        }
        else {
            showNotifyError("Cần chọn người để mời");
        }
    });

    $('#btAddContact').click(function () {
        $('#addConctactModal').showModal();
    });
    $('#btCloseAddContact').click(function () {
        $('#addConctactModal').closeModal();
    });
    $('#btCloseContactList').click(function () {
        $('#conctactListModal').closeModal();
    });
});