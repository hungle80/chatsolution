﻿var actionType = {
    savedMessageRoom: 'savedMessageRoom',
    createRoom: 'createRoom',
    renameRoom: 'renameRoom',
    changeAvatarRoom: 'changeAvatarRoom',
    addMember: 'addMember',
    removeMember: 'removeMember',
    leaveRoom: 'leaveRoom',
    deleteMessage: 'deleteMessage',
    createChannel: 'createChannel',
    renameChannel: 'renameChannel',
    joinChannel: 'joinChannel',
    leaveChannel: 'leaveChannel',
    changeChannelAvatar: 'changeChannelAvatar',
    changeChannelDescription: 'changeChannelDescription',
    changeJoinLink: 'changeJoinLink',
    changeAdminPermission: 'changeAdminPermission',
    getBanned: 'getBanned',
    getRestricted: 'getRestricted',
    updatePlan: 'updatePlan',
    deletePlan: 'deletePlan',
    planReminder: 'planReminder'
};
var messageType = {
    action: 'action',
    text: 'text',
    image: 'image',
    video: 'video',
    file: 'file',
    link: 'link',
    location: 'location',
    item: 'item',
    album: 'album',
    voice: 'voice',
    candidate: 'candidate',
    recruitment: 'recruitment',
    plan: 'plan',
    contact: 'contact'
};
var imgDataUrl;
var arrLoader = [];
var globalIsFull = true;

//hàm chung
function getChatHistoryWrapper(roomId) {
    return $('#divChatBoxList').find('.chat-pane[data-id=' + roomId + '] .chat-history');
}

//recent chat room
function setRecentChatRoomSelected($item) {
    $('#divRecentChat li').removeClass('selected');
    $item.addClass('selected');
}

function setChatIsViewed(roomId) {
    var $recentItem = $('#divRecentChat li[data-id=' + roomId + ']');

    if ($recentItem.attr('data-view') == 'false') {
        $recentItem.attr('data-view', true);
        var chatLogId = $recentItem.attr('data-lastlogid');
        var roomType = $recentItem.data('type');

        var data = {
            roomId: roomId, chatLogId: chatLogId
        };

        if (chatLogId) {

            socket.emit('setLogIsView', data, function (result) {
                //cập nhật lại đã xem trong chat room
            });
        }

    }
}

function bindRecentRoomAction() {
    var timeoutSearchRecent;
    $('#tbSearchRecent').bind('keyup', function (event) {
        var key = event.key ? event.key : event.keyIdentifier;
        //console.log(event);
        //if (key == 'Enter' && event.shiftKey == false) {
        //    //console.log('enter');            
        //    searchRecentChat();
        //}
        clearTimeout(timeoutSearchRecent);
        timeoutSearchRecent = setTimeout(function () { searchRecentChat(); }, 500);

        if ($('#tbSearchRecent').val() == '') {
            $('#btClearSearchRecent').hide();
        }
        else {
            $('#btClearSearchRecent').show();
        }
    });

    $('#btClearSearchRecent').click(function (e) {
        $('#btClearSearchRecent').hide();
        $('#tbSearchRecent').val('');
        searchRecentChat();
    });
}

function searchRecentChat() {
    if ($('#tbSearchRecent').val() == '') {
        showSearchRecent(false);
    }
    else {
        var keyword = $('#tbSearchRecent').val();
        showSearchRecent(true);
        $('#divRecentSearch').html('');
        loadRecentChatRoom($('#divRecentSearch'), null, keyword);
    }
}

function parseActionMessage(content, logId) {
    var html = '<span class="action-message">';
    switch (content.actionType) {
        case actionType.deleteMessage: {
            html += 'Tin nhắn đã bị xóa.';
            break;
        }
        case actionType.savedMessageRoom: {
            html += 'Đây là không gian riêng của Bạn, Bạn có thể tự nhắn cho mình hoặc lưu trữ tin từ nơi khác !';
            break;
        }
        case actionType.createRoom: {
            html += content.data.userName + ' đã tạo group.';
            break;
        }
        case actionType.renameRoom: {
            html += content.data.userName + ' đã đổi tên group là ' + content.data.roomName + '.';
            break;
        }
        case actionType.changeAvatarRoom: {
            html += content.data.userName + ' đã đổi thay đổi photo của group.';
            break;
        }
        case actionType.addMember: {
            html += content.data.userName + ' đã thêm ' + content.data.memberInfo.name + ' vào group.';
            break;
        }
        case actionType.removeMember: {
            html += content.data.userName + ' đã xoá ' + content.data.memberInfo.name + ' khỏi group.';
            break;
        }
        case actionType.createChannel: {
            console.log('creaetChannel');
            html += content.data.userName + ' đã tạo channel.';
            break;
        }
        case actionType.renameChannel: {
            html += content.data.userName + ' đã đổi tên channel là ' + content.data.roomName + '.';
            break;
        }
        case actionType.joinChannel: {
            html += content.data.userName + ' đã tham gia channel.';
            break;
        }
        case actionType.leaveChannel: {
            html += content.data.userName + ' đã rời khỏi channel';
            break;
        }
        case actionType.leaveRoom: {
            html += content.data.userName + ' đã rời khỏi nhóm';
            break;
        }
        case actionType.updatePlan: {
            //console.log(content);
            html += content.data.userName + ' đã thay đổi đặt lịch ' + content.data.title;
            break;
        }
        case actionType.deletePlan: {
            //console.log(content);
            html += content.data.userName + ' đã xoá đặt lịch ' + content.data.title;
            break;
        }
        case actionType.planReminder: {
            //console.log(content);
            html += '<span id="btGoFromPlanReminder' + logId + '">Đặt lịch ' + content.data.title + ' đã tới hạn (' + moment(content.data.timeStamp * 1000).format('DD MMMM, HH:mm') + ')</span>';
            if (logId) {
                $(document).on('click', '#btGoFromPlanReminder' + logId, function () {
                    scrollToLogFromPlanReminder(content.data);
                });
            }
            break;
        }
    }

    html += '</span>';
    return html;
}

function scrollToLogFromPlanReminder(replyLog) {
    replyLog._id = replyLog.chatLogId;
    var $chatHistory = getChatHistoryWrapper(replyLog.roomId);
    var $searchResult = getChatSearchResultWrapper(replyLog.roomId);
    var $chatItem = $chatHistory.find('li[data-id=' + replyLog._id + ']');
    //console.log($chatItem.html());
    if ($chatItem.length > 0) {
        closeSearchResult(replyLog.roomId, false);
        scrollToAndHighlight($chatHistory, $chatItem);
    }
    else {
        closeSearchResult(replyLog.roomId, true);

        if ($searchResult.find('ul li').length == 0) {
            loadRoomNearbyLogs($searchResult, replyLog);
        }
        else {
            //tìm xem có log trong đó ko
            $chatItem = $searchResult.find('li[data-id=' + replyLog._id + ']');

            if ($chatItem.length > 0) {
                //nếu có thì scroll đến
                scrollToAndHighlight($searchResult, $chatItem);
            }
            else {
                //nếu ko thì xoá hết li rồi gọi socket lấy, dùng chung hàm trên
                $searchResult.find('ul').html('');
                loadRoomNearbyLogs($searchResult, replyLog);
            }
        }
    }
}


function generateLinkPreview(log) {
    var html = '';
    var mediaType = detectLinkIsMedia(log.content.link);
    var $chatBox = getChatBox(log.roomId);
    var roomType = '';
    if ($chatBox.length > 0) {
        roomType = $chatBox.attr('data-type');
    }
    html += "<div class='message-link-wrapper " + ((roomType != 'channel' && log.userIdAuthor == currentUserId) ? "float-right" : "") + "'>"; //mở ko cần đóng, cuối hàm sẽ đóng    
    switch (mediaType) {
        case 'image': {
            html += "<a href='" + log.content.link + "' target='_blank'>";
            html += "<img src='" + log.content.link + "'>";
            html += "</a>";
            break;
        }
        case 'video': {
            html += "<video style='max-width:100%' controls><source src='" + log.content.link + "' /></video>";
            break;
        }
        case 'audio': {
            html += "<audio controls><source src='" + log.content.link + "'/></audio>";
            break;
        }
        case 'youtube': {
            html += replaceTextWithYouTube(log.content.link);
            break;
        }
        default: {
            html += "<a href='" + log.content.link + "' target='_blank'>";
            if (log.content.imageLink) {
                html += "<div class='message-link-img' style='background-image:url(\"" + log.content.imageLink + "\")'></div>";
            }
            html += "<div class='message-link-info'>";
            html += "<div class='message-link-title'>";
            html += log.content.title;
            html += "</div>";
            html += "<div class='message-link-des'>";
            html += log.content.description ? log.content.description : '';
            html += "</div>";
            html += "</div>";
            html += "</a>";
            break;
        }
    }
    return html;
}
function generateLastLogFromType(log) {
    var html = '';

    switch (log.type) {
        case messageType.text: {
            html += parseMessage(log.content.content);
            break;
        }
        case messageType.action: {
            html += parseActionMessage(log.content);
            break;
        }
        case messageType.image: {
            html += 'đã gửi hình';
            break;
        }
        case messageType.album: {
            html += 'đã gửi album hình';
            break;
        }
        case messageType.file: {
            html += 'đã gửi file';
            break;
        }
        case messageType.video: {
            html += 'đã gửi video';
            break;
        }
        case messageType.voice: {
            html += 'đã gửi audio';
            break;
        }
        case messageType.location: {
            html += 'đã gửi vị trí';
            break;
        }
        case messageType.link: {
            html += log.content.text;
            break;
        }
        case messageType.item: {
            html += 'đã gửi thông tin sản phẩm';
            break;
        }
        case messageType.candidate: {
            html += 'đã gửi hồ sơ ứng viên';
            break;
        }
        case messageType.recruitment: {
            html += 'đã gửi thông tin tuyển dụng';
            break;
        }
        case messageType.plan: {
            html += 'đã gửi đặt lịch';
            break;
        }
        case messageType.contact: {
            html += 'đã gửi liên hệ';
            break;
        }
    }

    return html;
}

function generateMessageFromType(log) {
    var html = '';

    switch (log.type) {
        case messageType.text: {
            if (log.replyOrForward == 'REPLY' && log.userIdAuthor == currentUserId) {
                html += '<div class="float-right">';
            }
            html += parseMessageLink(log.content.content);
            if (log.replyOrForward == 'REPLY' && log.userIdAuthor == currentUserId) {
                html += '</div>';
            }
            break;
        }
        case messageType.action: {
            html += parseActionMessage(log.content, log._id);
            break;
        }
        case messageType.image:
        case messageType.video: {
            var width = 'auto', height = 'auto';
            if (log.content.thumbWidth && log.content.thumbHeight) {
                width = log.content.thumbWidth;
                height = log.content.thumbHeight;
                if (log.content.thumbWidth > messageWrapperWidth) {
                    width = messageWrapperWidth;
                    height = (messageWrapperWidth / log.content.thumbWidth) * log.content.thumbHeight;
                }
                width = width + 'px';
                height = height + 'px';
            }
            html += "<a class='gallery-item " + (log.type == messageType.video ? "video-link" : "") + "' data-gallery='multiimages' data-title='" + log.content.name + "' href='" + log.content.link + "'  target='_blank'>";
            html += "<img data-src='" + log.content.link + "' src='" + log.content.thumbLink + "' style='width:" + width + ";height:" + height + "'/>";
            if (log.type == messageType.video) {
                html += '<div class="overlay">';
                html += '<div class="circle center-play">';
                html += '<i class="fa fa-play" style="line-height: 17px; margin-left: 4px;"></i>';
                html += '</div > ';
                html += '</div > ';
            }
            html += "</a>";
            //html += "<div>" + log.content.name + "</div>";
            break;
        }
        case messageType.file: {
            html += "<i class='fa-download fa download'></i> <a class='' href='" + log.content.link + "' target='_blank'>" + log.content.name + "</a>";
            break;
        }
        //case messageType.video: {
        //    html += "<video controls><source src='" + log.content.link + "' ></video>";
        //    break;
        //}
        case messageType.location: {
            html += '<iframe width="100% " height="200" frameborder="0" style="border: 0" ';
            //if (log.content.address) {
            //    html += ' src = "https://www.google.com/maps/embed/v1/place?center=' + log.content.lat + ',' + log.content.lng + '&amp;q=' + (log.content.address)  + '&amp;key=' + googleMapAPIKey + '" ></iframe >';
            //}
            //else {
            html += ' src = "https://www.google.com/maps/embed/v1/place?q=' + log.content.lat + ',' + log.content.lng + '&amp;key=' + googleMapAPIKey + '" ></iframe >';
            //}
            break;
        }
        case messageType.link: {
            html += parseMessageLink(log.content.text);
            if (log.content.isParse == true || detectLinkIsMedia(log.content.link) != '') {
                html += "</div>"; //đóng tag trước vì có màu nền                
                html += generateLinkPreview(log);
            }
            break;
        }
        case messageType.item: {
            html += generateItemMessage(log);
            break;
        }
        case messageType.album: {
            html += generateAlbum(log);
            break;
        }
        case messageType.voice: {
            html += generateVoiceMessage(log);
            break;
        }
        case messageType.candidate: {
            html += generateCandidateMessage(log);
            break;
        }
        case messageType.recruitment: {
            html += generateRecruitmentMessage(log);
            break;
        }
        case messageType.plan: {
            html += generatePlan(log);
            break;
        }
        case messageType.contact: {
            html += generateContactMessage(log);
            break;
        }
    }
    return html;
}
function generateContactMessage(log) {
    var html = '';
    var link = 'https://muabannhanh.com/' + log.content.mobile;
    html += '<div class="item-message clearfix">';
    html += '<a class="item-image" style="background-image:url(' + log.content.avatar + ')" target="_blank" href="' + link + '"></a>';
    html += '<div class="item-info">';
    html += '<div class="item-name"><a target="_blank" href="' + link + '">' + log.content.name + '</a></div>';
    html += '<div class="item-price">' + log.content.mobile + '</div>';
    html += '<div class="item-button">';
    html += '<a class="btn btn-xs btn-radius btn-outline-success" onclick="createPrivateChatRoom(' + log.content.contactId + ')"><span class="fa fa-comment"></span></a>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    return html;
}
function generateRecruitmentMessage(log) {
    var html = '';
    html += '<div class="item-message clearfix">';
    //html += '<a class="item-image" style="background-image:url(' + log.content.avatar + ')" target="_blank" href="' + log.content.link + '"></a>';
    html += '<div class="item-info">';
    html += '<div class="item-name"><a target="_blank" href="' + log.content.link + '">' + log.content.jobName + '</a></div>';
    html += '<div class="">' + log.content.companyName + '</div>';
    html += '<div class="">' + log.content.address + '</div>';
    html += '<div class="item-price">' + log.content.salary + '</div>';
    html += '</div>';
    html += '</div>';
    return html;
}
function generateCandidateMessage(log) {
    var html = '';
    html += '<div class="item-message clearfix">';
    html += '<a class="item-image" style="background-image:url(' + log.content.avatar + ')" target="_blank" href="' + log.content.link + '"></a>';
    html += '<div class="item-info">';
    html += '<div class="item-name"><a target="_blank" href="' + log.content.link + '">' + log.content.name + '</a></div>';
    html += '<div class="item-price">' + log.content.phone + '</div>';
    html += '</div>';
    html += '</div>';
    return html;
}
function generateVoiceMessage(log) {
    var item = log.content;
    var html = '<audio controls><source src="' + item.link + '" type="audio/mpeg" /></audio>';
    return html;
}
function generateAlbum(log) {
    var items = log.content.items;
    var html = '<div class="album-message">';
    html += '<div class="album-row">';
    for (var i = 0; i < items.length; i++) {
        var item = items[i];

        if (i != 0 && i % 3 == 0) {
            html += '</div>';
            html += '<div class="album-row">';
        }

        var width = 'auto', height = 'auto';
        if (item.thumbWidth && item.thumbHeight) {
            width = item.thumbWidth;
            height = item.thumbHeight;
            if (item.thumbWidth > messageWrapperWidth) {
                width = messageWrapperWidth;
                height = (messageWrapperWidth / item.thumbWidth) * item.thumbHeight;
            }
            width = width + 'px';
            height = height + 'px';
        }

        html += '<div class="album-item">';
        html += "<a class='gallery-item " + (item.isVideo ? "video-link" : "") + "' data-gallery='multiimages' data-title='" + item.name + "'  href='" + item.link + "'  target='_blank'>";
        html += "<img data-src='" + item.link + "' src='" + item.thumbLink + "' />";
        if (item.isVideo) {
            html += '<div class="overlay">';
            html += '<div class="circle center-play">';
            html += '<i class="fa fa-play" style="line-height: 17px; margin-left: 4px;"></i>';
            html += '</div > ';
            html += '</div > ';
        }
        html += "</a>";
        html += '</div>';
    }
    html += '</div>';
    html += '</div>';
    return html;
}

function findMemberView(currentRoomMember, viewUserId) {
    return currentRoomMember.find(function (x) {
        return x.userId === viewUserId;
    });
}
function generateIsView($chatHistory, log) {
    var html = '';
    var items = '';
    var roomType = $chatHistory.closest('.chat-pane').data('type');
    //console.log(roomType);
    if (roomType != 'channel') {
        if (log.views) {
            for (var i = 0; i < log.views.length; i++) {
                if (log.views[i].userId != currentUserId && log.views[i].showAvatar === true) {
                    var $img = $chatHistory.find('img.seenAvatar[data-id=' + log.views[i].userId + ']');
                    if ($img.length > 0) {
                        var $wrapper = $img.closest('p');
                        $img.remove();
                        if ($wrapper.length > 0) {
                            if ($wrapper.find('img').length == 0) {
                                $wrapper.remove();
                            }
                        }
                    }

                    //console.log(currentRoomMembers[log.roomId]);
                    if (currentRoomMembers[log.roomId]) {
                        var item = findMemberView(currentRoomMembers[log.roomId], log.views[i].userId);
                        if (item) {
                            items += '<img data-id="' + item.userId + '" src="' + item.userInfo.avatar + '" title="' + item.userInfo.name + '" class="seenAvatar"/>';
                        }
                    }
                }
            }

            if (items != '') {
                html += '<p class="seen-wrapper">';
                html += items;
                html += '</p>';
            }
        }
    }
    return html;
}
function generateDateTimeLabel($chatHistory, log, prepend) {
    var html = '';
    var $nearbyItem;
    if (prepend) {
        $nearbyItem = $chatHistory.find('ul li.chat-item:first-child');
    }
    else {
        $nearbyItem = $chatHistory.find('ul li.chat-item:last-child');
    }
    if ($nearbyItem.length > 0) {
        var nearDate = $nearbyItem.data('date');
        if (!moment(log.createDate).isSame(nearDate, 'day')) {
            html += '<li>';
            html += '<div class="date-divider"><span>' + formatFullDateTime(prepend ? nearDate : log.createDate) + '</span></div>';
            html += '</li>';
        }

        if (prepend) {
            $chatHistory.find('ul').prepend(html);
        }
        else {
            $chatHistory.find('ul').append(html);
        }
    }
}
function generateActionOnlyMessage(log) {
    var html = '';
    html += '<div class="action-message center">';
    html += generateMessageFromType(log);
    html += '</div>';
    return html;
}
function generateItemMessage(log) {
    var html = '';
    html += '<div class="item-message clearfix">';
    html += '<a class="item-image" style="background-image:url(' + log.content.itemImage + ')" target="_blank" href="' + log.content.itemLink + '"></a>';
    html += '<div class="item-info">';
    html += '<div class="item-name"><a target="_blank" href="' + log.content.itemLink + '">' + log.content.itemName + '</a></div>';
    html += '<div class="item-price">' + formatCurrencyVND(log.content.itemPrice) + '</div>';
    html += '</div>';
    html += '</div>';
    return html;
}
function generateLogFull($chatHistory, log, prepend, file) {

    var roomType;
    var $chatBox = getChatBox(log.roomId);
    if ($chatBox.length > 0) {
        roomType = $chatBox.attr('data-type');
    }
    else if (log.roomInfo) {
        roomType = log.roomInfo.type;
    }
    var isChannel = roomType == 'channel';
    var channelAvatar;
    var channelName;
    if (isChannel) {
        if (log.roomInfo) {
            channelName = log.roomInfo.roomName;
            channelAvatar = log.roomInfo.roomAvatar;
        }
        else {
            channelName = $chatBox.find('.box-name').html();
            channelAvatar = $chatBox.find('img.box-avatar').attr('src');
        }
    }
    var itemGUID = log.itemGUID;
    if (!itemGUID) {
        itemGUID = 'x';
    }
    //console.log('itemGUID', itemGUID);
    //console.log(log);
    if ($chatHistory.find('li[data-itemGUID=' + itemGUID + ']').length == 0) {
        var html = "";
        generateDateTimeLabel($chatHistory, log, prepend);
        var isViewed = true;
        if (log.views) {
            for (var i = 0; i < log.views.length; i++) {
                if (log.views[i].userId == currentUserId) {
                    isViewed = log.views[i].isViewed;
                }
            }
        }

        var isActionMessage = (log.type == messageType.action && log.content.actionType != actionType.deleteMessage);
        if (isChannel) {
            html += "<li data-author='" + log.userIdAuthor + "' data-date='" + log.createDate + "' data-view='" + isViewed + "' " + (log._id ? "data-id='" + log._id + "'" : "") + (log.itemGUID ? " data-itemGUID='" + log.itemGUID + "'" : "") + " class='chat-item clearfix " + (log.isPin ? " isPinned" : " ") + (isActionMessage ? " isAction" : "") + "'>";
        }
        else {
            html += "<li data-author='" + log.userIdAuthor + "' data-date='" + log.createDate + "' data-view='" + isViewed + "' " + (log._id ? "data-id='" + log._id + "'" : "") + (log.itemGUID ? " data-itemGUID='" + log.itemGUID + "'" : "") + " class='chat-item clearfix " + (log.userIdAuthor == currentUserId ? " my-message" : "") + (log.isPin ? " isPinned" : " ") + (isActionMessage ? " isAction" : "") + "'>";
        }
        if (isActionMessage) { //loại message action
            html += generateActionOnlyMessage(log);
        }
        //else if (log.type == messageType.item) {
        //    html += generateItemMessage(log);
        //}
        else {
            html += "<div class='clearfix'>";
            html += "<div class='message-gutter'>";
            html += "<div class='message-avatar'>";
            if (isChannel) {
                html += '<img src="' + channelAvatar + '" class="circle avatar-chat pointer" title="' + escapeQuote(log.authorInfo.name) + '"/>';
            }
            else {
                html += '<img onclick="createPrivateChatRoom(' + log.userIdAuthor + ')" src="' + log.authorInfo.avatar + '" class="circle avatar-chat pointer" title="' + escapeQuote(log.authorInfo.name) + '"/>';
            }
            html += "</div>"; //end message-avatar
            html += "<div class='message-time'>" + formatTime(log.createDate) + "</div>";
            html += "<div class='message-pin-holder'>";
            if (log.isPin) {
                html += "<a id='btUnPin" + log._id + "' class='btUnPin pointer'><span class='fa fa-thumb-tack'></span></a>";
            }
            html += "</div>";
            html += "</div>"; //end message-gutter
            html += "<div class='message-content'>";
            html += "<div class='message-header'>";
            if (isChannel) {
                var signMessage = false;
                if (log.signMessage == undefined) {
                    if (roomInfos[log.roomId]) {
                        signMessage = roomInfos[log.roomId].signMessage;
                    }
                }
                else {
                    signMessage = log.signMessage;
                }

                html += "<span class='message-data-name'>" + channelName + (signMessage ? " <small class='text-mute ml10'><span class='fa fa-user'></span> " + log.authorInfo.name + "</small>" : "") + "</span>";
            }
            else {
                if (log.userIdAuthor != currentUserId) {
                    html += "<span class='message-data-name'>" + log.authorInfo.name + "</span>";
                }
            }
            html += "<span class='message-data-time' >" + formatDateTime(log.createDate) + "</span>";
            html += "</div>";    //end  message-header
            html += "<div class='message-body " + ((log.type != messageType.location && log.type != messageType.link) ? "auto-width " : " ") + "'>";

            if (log.replyOrForward && !log.isDeleted) {
                switch (log.replyOrForward) {
                    case 'FORWARD': {
                        html += '<div class="clearfix forward-header">';
                        html += '<div class="text-muted small">';
                        html += 'Tin nhắn chuyển tiếp';
                        html += '</div>';
                        html += '<div class="text-muted small">';
                        html += 'Từ <a class="text-primary" href="javascript:;" onclick="createPrivateChatRoom(' + log.originMessage.authorInfo._id + ')">' + log.originMessage.authorInfo.name + '</a>';
                        html += '</div>';
                        html += '</div>';
                        break;
                    }
                    case 'REPLY': {
                        console.log('reply');
                        var tempId = log._id ? log._id : log.itemGUID;
                        var $quote = $('<div id="btGoFromReply' + tempId + '" class="quote-in-message reply-quote"></div>');
                        $quote.append('<div class="quote-content clearfix"></div>');
                        $quote.find('.quote-content').append('<div class="quote-image"><img class="" src=""/></div>');
                        $quote.find('.quote-content').append('<div class="quote-panel"><div class="quote-user"></div><div class="quote-text"></div></div>');
                        generateReplyQuote($quote, log.originMessage);

                        $(document).on('click', '#btGoFromReply' + tempId, function () {
                            if (!$('#btGoFromReply' + log._id).prop('disabled')) {
                                var replyLog = log.originMessage;
                                var $chatHistory = getChatHistoryWrapper(replyLog.roomId);
                                var $searchResult = getChatSearchResultWrapper(replyLog.roomId);
                                var $chatItem = $chatHistory.find('li[data-id=' + replyLog._id + ']');
                                //console.log($chatItem.html());
                                if ($chatItem.length > 0) {
                                    closeSearchResult(replyLog.roomId, false);
                                    scrollToAndHighlight($chatHistory, $chatItem);
                                }
                                else {
                                    closeSearchResult(replyLog.roomId, true);

                                    if ($searchResult.find('ul li').length == 0) {
                                        loadRoomNearbyLogs($searchResult, replyLog);
                                    }
                                    else {
                                        //tìm xem có log trong đó ko
                                        $chatItem = $searchResult.find('li[data-id=' + replyLog._id + ']');

                                        if ($chatItem.length > 0) {
                                            //nếu có thì scroll đến
                                            scrollToAndHighlight($searchResult, $chatItem);
                                        }
                                        else {
                                            //nếu ko thì xoá hết li rồi gọi socket lấy, dùng chung hàm trên
                                            $searchResult.find('ul').html('');
                                            loadRoomNearbyLogs($searchResult, replyLog);
                                        }
                                    }
                                }
                            }
                        });
                        console.log($quote[0].outerHTML);
                        html += $quote[0].outerHTML;
                        break;
                    }
                }
            }
            //html += "<div class='message-body auto-width'>";
            if (file) {
                html += generateMessageUploading(log, file);
            }
            else {
                html += generateMessageFromType(log);
            }
            html += "</div>";    //end  message-body

            html += "<div class='message-buttons'>";
            html += "<div class='btn-group'>";
            if (!log._id) {
                if (log.type != messageType.item) {
                    html += "<a class='btn btn-sm btDelete' title='Xoá' id='btDelete" + log.itemGUID + "'><span class='fa fa-trash'></span></a>";
                }
            }
            else {
                if (!log.isDeleted) {
                    if (log.type != messageType.plan) {
                        html += "<a class='btn btn-sm btForward title='Chuyển tiếp' id='btForward" + log._id + "'><span class='fa fa-share'></span></a>";
                    }
                    html += "<a class='btn btn-sm btReply' title='Trả lời' id='btReply" + log._id + "'><span class='fa fa-quote-left'></span></a>";
                    if (log.userIdAuthor == currentUserId) {
                        html += "<a class='btn btn-sm btDelete' title='Xoá' id='btDelete" + log._id + "'><span class='fa fa-trash'></span></a>";
                    }
                    if (!log.isPin) {
                        html += "<a class='btn btn-sm btPin' title='Pin' id='btPin" + log._id + "'><span class='fa fa-thumb-tack'></span></a>";
                    }
                }
            }
            html += "</div>";    //end  button-group
            html += "</div>";    //end  message-buttons
            html += "</div>";    //end  message-content
            html += "</div>";    //end  clearfix
        }
        html += generateIsView($chatHistory, log);
        html += "</li>";

        var $html = $(html);
        //console.log($html);
        var $previousItem;
        var authorId;
        if (prepend) {
            if (!isActionMessage) {
                if (isChannel) {
                    $html.addClass('first');
                }
                else {
                    $previousItem = $chatHistory.find('ul > li:first');
                    if ($previousItem.length > 0 && $previousItem.hasClass('isAction')) {
                        $html.addClass('first');
                    }
                    else {
                        $previousItem = $chatHistory.find('ul > li:not(.isAction):first');
                        if ($previousItem.length > 0) {
                            authorId = $previousItem.attr('data-author');
                            if (authorId != log.userIdAuthor) {
                                $html.addClass('first');
                            }
                            else {
                                $html.addClass('first');
                                $previousItem.removeClass('first');
                            }
                        }
                        else {
                            $html.addClass('first');
                        }
                    }
                }
            }

            $html.prependTo($chatHistory.find('ul'));
        }
        else {
            if (!isActionMessage) {
                if (isChannel) {
                    $html.addClass('first');
                }
                else {
                    $previousItem = $chatHistory.find('ul > li:last');
                    if ($previousItem.length > 0 && $previousItem.hasClass('isAction')) {
                        $html.addClass('first');
                    }
                    else {
                        $previousItem = $chatHistory.find('ul > li:not(.isAction):last');
                        if ($previousItem.length > 0) {
                            authorId = $previousItem.attr('data-author');
                            if (authorId != log.userIdAuthor) {
                                $html.addClass('first');
                            }
                        }
                        else {
                            $html.addClass('first');
                        }
                    }
                }
            }
            $html.appendTo($chatHistory.find('ul'));
        }

        if (file) { //gửi file thì add loader
            var $topLoader = $("#pr" + log.itemGUID).percentageLoader({
                width: 80, height: 80, controllable: false, progress: 0, onProgressUpdate: function (val) {
                    $topLoader.setValue(Math.round(val * 100.0));
                }
            });

            $topLoader.setProgress(0 / 100);
            $topLoader.setValue(formatBytes(0, 3));

            if (arrLoader[log.itemGUID] == null) {
                arrLoader[log.itemGUID] = $topLoader;
            }
        }
        if (!log._id) { //mới chat thì add button xóa vào
            if (log.type != messageType.item) {
                var $btDelete = $('#btDelete' + log.itemGUID);
                $btDelete.click(function (e) {
                    //console.log('x');
                    confirmNotify('Xóa tin nhắn?', function (res) {
                        if (res) {
                            deleteMessage(log.itemGUID, log.roomId);
                        }
                    });
                });

                //setTimeout(function () {
                //    $btDelete.remove();
                //}, 30000);
            }
        }
        else {
            if (log.type != messageType.item) {
                var $btDelete = $('#btDelete' + log._id);
                $btDelete.click(function (e) {
                    //console.log('x');
                    confirmNotify('Xóa tin nhắn?', function (res) {
                        if (res) {
                            deleteRoomLog(log._id, log.roomId);
                        }
                    });
                });
            }
            if (log.isPin) {
                $('#btUnPin' + log._id).click(function (e) {
                    if (!$(this).prop('disabled')) {
                        unpinMessage(log._id, log.roomId);
                    }
                });
            }
            else {
                $('#btPin' + log._id).click(function (e) {
                    if (!$(this).prop('disabled')) {
                        pinMessage(log._id, log.roomId);
                    }
                });

            }
            $('#btForward' + log._id).click(function (e) {
                if (!$(this).prop('disabled')) {
                    showForwardModal(log);
                }
            });
            $('#btReply' + log._id).click(function (e) {
                if (!$(this).prop('disabled')) {
                    showReplyPopup(log);
                }
            });
        }

    }
}

function generateLog($chatHistory, log, prepend, file) {

    var $chatBox = getChatBox(log.roomId);
    if ($chatBox.length > 0) {
        roomType = $chatBox.attr('data-type');
    }
    else if (log.roomInfo) {
        roomType = log.roomInfo.type;
    }
    var isChannel = roomType == 'channel';
    var channelAvatar;
    var channelName;
    if (isChannel) {
        if (log.roomInfo) {
            channelName = log.roomInfo.roomName;
            channelAvatar = log.roomInfo.roomAvatar;
        }
        else {
            channelName = $chatBox.find('.box-name').html();
            channelAvatar = $chatBox.find('img.room-avatar').attr('src');
        }
    }
    //console.log(log);
    if ($chatHistory.find('li[data-itemGUID=' + log.itemGUID + ']').length == 0) {
        var html = "";
        generateDateTimeLabel($chatHistory, log, prepend);

        var isViewed = true;
        if (log.views) {
            for (var i = 0; i < log.views.length; i++) {
                if (log.views[i].userId == currentUserId) {
                    isViewed = log.views[i].isViewed;
                }
            }
        }
        var isActionMessage = (log.type == messageType.action && log.content.actionType != actionType.deleteMessage);

        html += "<li data-date='" + log.createDate + "' data-view='" + isViewed + "' " + (log._id ? "data-id='" + log._id + "'" : "") + (log.itemGUID ? " data-itemGUID='" + log.itemGUID + "'" : "") + " class='chat-item clearfix'>";

        if (isActionMessage) { //loại message action
            html += generateActionOnlyMessage(log);
        }
        //else if (log.type == messageType.item) {
        //    html += generateItemMessage(log);
        //}
        else {

            html += "<div class='message-data " + ((!isChannel && log.userIdAuthor == currentUserId) ? "align-right" : "") + "'>";
            if (isChannel) {
                var signMessage = false;
                if (log.signMessage == undefined) {
                    if (roomInfos[log.roomId]) {
                        signMessage = roomInfos[log.roomId].signMessage;
                    }
                }
                else {
                    signMessage = log.signMessage;
                }
                html += '<img src="' + channelAvatar + '" class="circle avatar-chat"/>';
                html += "<span class='message-data-name'>" + channelName + (signMessage ? " <small class='text-mute ml10'><span class='fa fa-user'></span> " + log.authorInfo.name + "</small>" : "") + "</span>";
                html += "&nbsp;&nbsp;";
                html += "<span class='message-data-time' >" + formatDateTime(log.createDate) + "</span>";
            }
            else if (log.userIdAuthor == currentUserId) {
                html += "<span class='message-data-time' >" + formatDateTime(log.createDate) + "</span>";
                html += "&nbsp;&nbsp;";
                html += '<img src="' + log.authorInfo.avatar + '" class="circle avatar-chat"/>';
                //html += "<span class='message-data-name'>" + log.authorInfo.name + "</span>";
                //html += "<i class='fa fa-circle me'></i>";
            }
            else {
                //html += "<i class='fa fa-circle online'></i>";
                html += '<img src="' + log.authorInfo.avatar + '" class="circle avatar-chat"/>';
                html += "<span class='message-data-name'>" + log.authorInfo.name + "</span>";
                html += "&nbsp;&nbsp;";
                html += "<span class='message-data-time' >" + formatDateTime(log.createDate) + "</span>";
            }
            html += "</div>";
            if (!log._id) {
                html += "<div class='send-status " + ((!isChannel && log.userIdAuthor == currentUserId) ? "float-right" : "float-left") + "'>Đang gửi</div>";
                if (log.type != messageType.item) {
                    html += "<div id='btDelete" + log.itemGUID + "' class='delete-button-wrapper'><i class='fa fa-trash custom-icon'></i></div>";
                }
            }

            html += generateIsView($chatHistory, log);
            html += "<div class='message " + (log.type == messageType.image ? " gallery " : "") + ((log.type != messageType.location && log.type != messageType.link) ? "auto-width " : " ") + ((!isChannel && log.userIdAuthor == currentUserId) ? ("my-message float-right") : ("other-message " + (log.type != messageType.link ? "float-left" : ""))) + "'> ";
            if (file) {
                html += generateMessageUploading(log, file);
            }
            else {
                html += generateMessageFromType(log);
            }
            html += "</div>";
        }
        html += "</li>";

        if (prepend) {
            $chatHistory.find('ul').prepend(html);
        }
        else {
            $chatHistory.find('ul').append(html);
        }

        if (file) { //gửi file thì add loader
            var $topLoader = $("#pr" + log.itemGUID).percentageLoader({
                width: 80, height: 80, controllable: false, progress: 0, onProgressUpdate: function (val) {
                    $topLoader.setValue(Math.round(val * 100.0));
                }
            });

            $topLoader.setProgress(0 / 100);
            $topLoader.setValue(formatBytes(0, 3));

            if (arrLoader[log.itemGUID] == null) {
                arrLoader[log.itemGUID] = $topLoader;
            }
        }

        if (!log._id) { //mới chat thì add button xóa vào
            if (log.type != messageType.item) {
                var $btDelete = $('#btDelete' + log.itemGUID);
                $btDelete.click(function (e) {
                    //console.log('x');
                    confirmNotify('Xóa tin nhắn?', function (res) {
                        if (res) {
                            deleteMessage(log.itemGUID, log.roomId);
                        }
                    });
                });

                setTimeout(function () {
                    $btDelete.remove();
                }, 30000);
            }
        }
    }

}

//forward message
var selectedForwardRoomId;
var currentForwardLog;
function showForwardModal(log) {
    selectedForwardRoomId = null;
    currentForwardLog = log;
    loadRecentPinChatRoom($('#divForwardList'), true);
    loadRecentChatRoom($('#divForwardList'), null, null, true, true);
    $('#forwardModal').showModal();
}
function bindForwardSearchAction() {
    var timeoutSearchForward;
    $('#tbSearchForwardList').bind('keyup', function (event) {
        var key = event.key ? event.key : event.keyIdentifier;
        clearTimeout(timeoutSearchForward);
        timeoutSearchForward = setTimeout(function () { searchForwardList(); }, 500);

        if ($('#tbSearchForwardList').val() == '') {
            $('#btClearSearchForwardList').hide();
        }
        else {
            $('#btClearSearchForwardList').show();
        }
    });

    $('#btClearSearchForwardList').click(function (e) {
        $('#btClearSearchForwardList').hide();
        $('#tbSearchForwardList').val('');
        searchForwardList();
    });
}
function searchForwardList() {
    if ($('#tbSearchForwardList').val() == '') {
        showSearchForward(false);
    }
    else {
        var keyword = $('#tbSearchForwardList').val();
        showSearchForward(true);
        $('#divForwardSearch').html('');
        loadRecentChatRoom($('#divForwardSearch'), null, keyword, null, true);
    }
}
function showSearchForward(isShow) {
    if (isShow) {
        $('#divForwardSearch').removeClass('hide');
        $('#divForwardList').addClass('hide');
    }
    else {
        $('#divForwardSearch').addClass('hide');
        $('#divForwardList').removeClass('hide');
    }
}
$(document).ready(function () {
    bindForwardSearchAction();
    $('#btForwardMessage').click(function () {
        if (selectedForwardRoomId) {
            var originMessage = {
                _id: currentForwardLog._id,
                roomId: currentForwardLog.roomId,
                type: currentForwardLog.type,
                content: currentForwardLog.content,
                authorInfo: {
                    _id: currentForwardLog.authorInfo._id,
                    name: currentForwardLog.authorInfo.name,
                    avatar: currentForwardLog.authorInfo.avatar,
                    phone: currentForwardLog.authorInfo.phone
                }
            };
            socket.emit('forwardMessage',
                {
                    roomId: selectedForwardRoomId,
                    originMessage: originMessage,
                    itemGUID: generateGUID()
                },
                function (result) {
                    console.log(result);
                    if (result.errorCode == 0) {
                        $('#forwardModal').closeModal();
                    }
                });
        }
        else {
            showNotifyError('Chưa chọn room để gửi');
        }
    });
});

//reply message
var currentReplyLogs = [];
function toggleButtonWhenReply(disable, roomId) {
    if (disable) {
        $('#btUploadFile' + roomId).attr('disabled', disable);
        $('#btAddPlan' + roomId).attr('disabled', disable);
    }
    else {
        $('#btUploadFile' + roomId).removeAttr('disabled');
        $('#btAddPlan' + roomId).removeAttr('disabled');
    }
    $('#btUploadFile' + roomId).prop('disabled', disable);
    $('#btAddPlan' + roomId).prop('disabled', disable);
}

function closeReplyPopup(roomId) {
    currentReplyLogs[roomId] = null;
    toggleButtonWhenReply(false, roomId);
    $('#btCloseQuote' + roomId).closest('.reply-quote').hide();
}

function showReplyPopup(log) {
    currentReplyLogs[log.roomId] = log;
    var $chatBox = getChatBox(log.roomId);
    var $quote = $chatBox.find('.reply-quote-main');
    toggleButtonWhenReply(true, log.roomId);
    generateReplyQuote($quote, log);
    $quote.show();
}
function generateReplyQuote($quote, log) {
    var $quoteImage = $quote.find('.quote-image');
    var $quoteUser = $quote.find('.quote-user');
    var $quoteText = $quote.find('.quote-text');
    var $quotePanel = $quote.find('.quote-panel');

    $quoteImage.show();
    $quoteUser.html(log.authorInfo.name);
    console.log(log.type);
    switch (log.type) {
        case messageType.text: {
            $quoteImage.hide();
            $quoteText.html(parseMessage(log.content.content, true));
            $quotePanel.css('width', '100%');
            break;
        }
        case messageType.link: {
            $quoteImage.hide();
            $quoteText.html(parseMessageLink(log.content.text, true));
            $quotePanel.css('width', '100%');
            break;
        }
        case messageType.file: {
            var $image = $quoteImage.find('img');
            $image.prop('src', '/images/document-file-icon.png');
            $quoteText.html('Tập tin - ' + log.content.name);
            $quotePanel.css('width', '');
            break;
        }
        case messageType.image: {
            var $image = $quoteImage.find('img');
            $image.prop('src', log.content.thumbLink);
            $quoteText.html('Hình ảnh');
            $quotePanel.css('width', '');
            break;
        }
        case messageType.album: {
            var $image = $quoteImage.find('img');
            $image.prop('src', log.content.items[0].thumbLink);
            $quoteText.html('Album');
            $quotePanel.css('width', '');
            break;
        }
        case messageType.video: {
            var $image = $quoteImage.find('img');
            $image.prop('src', log.content.thumbLink);
            $quoteText.html('Video');
            $quotePanel.css('width', '');
            break;
        }
        case messageType.voice: {
            var $image = $quoteImage.find('img');
            $image.prop('src', '/images/audio.png');
            $quoteText.html('Audio');
            $quotePanel.css('width', '');
            break;
        }
        case messageType.location: {
            var $image = $quoteImage.find('img');
            $image.prop('src', '/images/location_icon.jpg');
            var mapName = log.content.address ? log.content.address : log.content.lat + ',' + log.content.lng;
            $quoteText.html('Map - ' + mapName);
            $quotePanel.css('width', '');
            break;
        }
        case messageType.item: {
            var $image = $quoteImage.find('img');
            $image.prop('src', log.content.itemImage);
            $quoteText.html('Tin đăng - ' + log.content.itemName);
            $quotePanel.css('width', '');
            break;
        }
        case messageType.recruitment: {
            var $image = $quoteImage.find('img');
            $image.prop('src', '/images/recruitment.jpg');
            $quoteText.html('Tuyển dụng - ' + log.content.jobName);
            $quotePanel.css('width', '');
            break;
        }
        case messageType.candidate: {
            var $image = $quoteImage.find('img');
            $image.prop('src', log.content.avatar);
            $quoteText.html('Ứng viên - ' + log.content.name);
            $quotePanel.css('width', '');
            break;
        }
        case messageType.plan: {
            var $image = $quoteImage.find('img');
            $image.prop('src', '/images/planIcon.png');
            $quoteText.html('Đặt lịch - ' + log.content.title);
            $quotePanel.css('width', '');
            break;
        }
    }
}

//pin message
function pinMessage(logId, roomId) {
    confirmNotify('Pin tin nhắn này?', function (res) {
        if (res) {
            socket.emit('pinMessage', {
                roomId: roomId,
                logId: logId
            }, function (result) {
                if (result.errorCode != 0) {
                    showNotifyError('Lỗi khi pin tin nhắn!');
                }
            });
        }
    });
}

function unpinMessage(logId, roomId) {
    confirmNotify('Bỏ pin tin nhắn này?', function (result) {
        if (result) {
            socket.emit('unpinMessage', {
                roomId: roomId,
                logId: logId
            }, function (result) {
                if (result.errorCode != 0) {
                    showNotifyError('Lỗi khi unpin tin nhắn!');
                }
            });
        }
    });
}
function deleteMessage(itemGUID, roomId) {
    var $item = $('li[data-itemGUID=' + itemGUID + ']');
    var chatLogId = $item.attr('data-id');
    //console.log(chatLogId);
    deleteRoomLog(chatLogId, roomId);
}

function deleteRoomLog(logId, roomId) {
    socket.emit('deleteMessage', {
        roomId: roomId, chatLogId: logId
    }, function (result) {
        if (result.errorCode != 0) {
            showNotifyError('Lỗi khi xóa tin nhắn!');
        }
    });
}
function updateLogStatus($chatHistory, log, isSuccess) {
    var $item = $chatHistory.find('li[data-itemGUID=' + log.itemGUID + ']');
    if (isSuccess == true) {
        $item.find('.send-status').html('Đã gửi');
        $item.find('.send-status').removeClass('error');
        setTimeout(function () {
            $item.find('.send-status').remove();
        }, 2000);
    }
    else {
        $item.find('.send-status').html('Không gửi được');
        $item.find('.send-status').addClass('error');
    }
}
function updateLogIsView($chatHistory, $item, log) {
    //console.log(log.views);
    var html = generateIsView($chatHistory, log);
    $item.find('.seen-wrapper').remove();
    if ($item.find('.message').length > 0) {
        $(html).insertBefore($item.find('.message'));
    }
    else if ($item.children('.clearfix').length > 0) {
        $(html).insertAfter($item.children('.clearfix'));
    }
}
function updateLog($chatHistory, result) {
    //console.log(result);
    var log;
    if (result.errorCode == 0) {
        log = result.data;

        var $item;
        if (log.itemGUID) {
            $item = $chatHistory.find('li[data-itemGUID=' + log.itemGUID + ']');
        }
        else {
            $item = $chatHistory.find('li[data-id=' + log._id + ']');
        }

        $item.attr('data-id', log._id);
        $item.find('.message-data-time').html(formatDateTime(log.createDate));
        //console.log(log);
        if (log.type == messageType.action) {
            if (log.content.actionType == actionType.deleteMessage) {
                $item.find('.delete-button-wrapper').remove();
                $item.find('.delete-button-wrapper').remove();
            }
        }
        if (log.type == messageType.link && (log.content.isParse || detectLinkIsMedia(log.content.link) != '')) {
            $item.find('.message-link-wrapper').remove();
            if ($item.find('.message').length > 0) {
                $(generateLinkPreview(log)).insertAfter($item.find('.message'));
            }
            else {
                $(generateLinkPreview(log)).insertAfter($item.find('.message-body'));
            }
        }
        else {
            if ($item.find('.message').length > 0) {
                var message = generateMessageFromType(log);
                if (log.isDeleted) {
                    $item.find('.message-buttons .btn-group').html('');
                }
                if (!log.isDeleted && log.replyOrForward == 'REPLY') {
                    var $quote = $item.find('.quote-in-message');
                    $item.find('.message').html($quote);
                    $item.find('.message').append(message);
                }
                else if (!log.isDeleted && log.replyOrForward == 'FORWARD') {
                    var $forwardHeader = $item.find('.forward-header');
                    $item.find('.message-body').html($forwardHeader);
                    $item.find('.message-body').append(message);
                }
                else {
                    $item.find('.message').html(message);
                }
            }
            else {
                var message = generateMessageFromType(log);
                if (log.isDeleted) {
                    $item.find('.message-buttons .btn-group').html('');
                }
                if (!log.isDeleted && log.replyOrForward == 'REPLY') {
                    var $quote = $item.find('.quote-in-message');
                    $item.find('.message-body').html($quote);
                    $item.find('.message-body').append(message);
                }
                else if (!log.isDeleted && log.replyOrForward == 'FORWARD') {
                    var $forwardHeader = $item.find('.forward-header');
                    $item.find('.message-body').html($forwardHeader);
                    $item.find('.message-body').append(message);
                }
                else {
                    $item.find('.message-body').html(generateMessageFromType(log));
                }
            }
        }

        if (!log.isDeleted && $item.find('.btn-group').length > 0) {
            if ($item.find('.btn-group .btPin').length == 0) {
                var $btPin = $("<a class='btn btn-sm btPin pointer' id='btPin" + log._id + "'><span class='fa fa-thumb-tack'></span></a>");
                $btPin.appendTo($item.find('.btn-group'));
                $btPin.click(function (e) {
                    if (!$(this).prop('disabled')) {
                        pinMessage(log._id, log.roomId);
                    }
                });
            }
            if ($item.find('.btn-group .btReply').length == 0) {
                var $btReply = $("<a class='btn btn-sm btReply pointer' id='btReply" + log._id + "'><span class='fa fa-quote-left'></span></a>");
                $btReply.prependTo($item.find('.btn-group'));
                $btReply.click(function (e) {
                    if (!$(this).prop('disabled')) {
                        showReplyPopup(log);
                    }
                });
            }
            if (log.type != messageType.plan) {
                if ($item.find('.btn-group .btForward').length == 0) {
                    var $btForward = $("<a class='btn btn-sm btForward pointer' id='btForward" + log._id + "'><span class='fa fa-share'></span></a>");
                    $btForward.prependTo($item.find('.btn-group'));
                    $btForward.click(function (e) {
                        if (!$(this).prop('disabled')) {
                            showForwardModal(log);
                        }
                    });
                }
            }
        }

        updateLogIsView($chatHistory, $item, log);

        updateLogStatus($chatHistory, log, true);
    }
    else {
        log = result.data;
        if (log) {
            updateLogStatus($chatHistory, log, false);
        }
    }
}

function scrollToBottom($chatHistory) {
    //console.log($chatHistory[0]);
    $chatHistory.scrollTop($chatHistory[0].scrollHeight);
}

function generateMagnificPopup($chatBox) {
    $chatBox.magnificPopup({
        delegate: '.gallery-item',
        type: 'image',
        mainClass: 'mfp-with-zoom mfp-img-mobile',
        image: {
            verticalFit: true,
            titleSrc: function (item) {

                var caption = item.el.attr('data-title');

                //var pinItURL = "http://pinterest.com/pin/create/button/";

                //pinItURL += '?url=' + '@DAL.WebUrl';
                //pinItURL += '&media=' + item.el.attr('href');
                //pinItURL += '&description=' + caption;


                return caption;// + ' &middot; <a class="pin-it" href="' + pinItURL + '" target="_blank"><img src="http://assets.pinterest.com/images/pidgets/pin_it_button.png" /></a>';
            }
        },
        closeOnContentClick: false,
        closeBtnInside: false,
        gallery: {
            // options for gallery
            enabled: true,
            navigateByImgClick: true,
            preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
        },
        zoom: {
            enabled: true, // By default it's false, so don't forget to enable it

            duration: 300, // duration of the effect, in milliseconds
            opener: function (element) {
                return element.find('img');
            }
        },
        callbacks: {
            elementParse: function (item) {
                //console.log(item);
                if (item.el[0].className.indexOf('video-link') != -1) {
                    item.type = 'iframe';
                } else {
                    item.type = 'image';
                }
            }
        }
    });
}

//upload
function generateMessageUploading(log, file) { //người gửi mới thấy nên ko cần kiểm tra message của ai
    var html = "<div id='pr" + log.itemGUID + "' class='progress-ring float-left'></div>";
    html += "<div class='uploading-filename float-left'>" + log.content.name + "</div>";
    return html;
}

function updateUploadingProgress(itemGUID, percentComplete, sizeComplete) {
    var $topLoader = arrLoader[itemGUID];
    $topLoader.setProgress(percentComplete / 100);
    $topLoader.setValue(formatBytes(sizeComplete, 3));
}

function resetToNull(el) {
    $(el)[0].files = null;
}
function handeSingleFileUpload(file, roomId, $chatHistory, isFull) {
    var size = file.size;
    if (maxLimit != 0 && size > maxLimit * 1024 * 1024) {
        var msg = 'File upload vượt quá ' + maxLimit + 'MB';
        //console.log(msg);
        showNotifyError(msg);
    }
    else {
        var fileName = file.name.toString();
        fileName = replaceSpecialCharacter(compound2Unicode(fileName));
        var itemGUID = generateGUID();
        socket.emit('generateUploadSAS', { roomId: roomId, itemGUID: itemGUID, fileName: fileName },
            function (result) {
                if (result.errorCode == 0) {
                    var az = result.data;
                    executeFileToCloud($chatHistory, file, az, roomId, itemGUID, isFull);
                }
                else {
                    console.log(result);
                    showNotifyError('Lỗi xảy ra khi upload');
                }
            });
    }
}
function handleFileUpload(files, roomId, $chatHistory, isFull) {
    for (var i = 0; i < files.length; i++) {
        handeSingleFileUpload(files[i], roomId, $chatHistory, isFull);
    }
}

function uploadFileToCloud(file, azUrl, itemGUID, showProgress) {
    return new Promise((resolve, reject) => {

        var maxBlockSize = 2 * 1024 * 1024;
        var fileSize = file.size;

        if (fileSize < maxBlockSize) {
            maxBlockSize = fileSize;
        }

        var blockIds = [];
        var blockIdPrefix = "block-";

        var currentFilePointer = 0;
        var totalBytesRemaining = fileSize;
        var numberOfBlocks = 0;
        var bytesUploaded = 0;

        if (fileSize % maxBlockSize == 0) {
            numberOfBlocks = fileSize / maxBlockSize;
        } else {
            numberOfBlocks = parseInt(fileSize / maxBlockSize, 10) + 1;
        }

        var reader = new FileReader();

        reader.onloadend = function (evt) {
            if (evt.target.readyState == FileReader.DONE) { // DONE == 2
                var uri = azUrl + '&comp=block&blockid=' + blockIds[blockIds.length - 1];
                var requestData = new Uint8Array(evt.target.result);
                $.ajax({
                    url: uri,
                    type: "PUT",
                    data: requestData,
                    processData: false,
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader('x-ms-blob-type', 'BlockBlob');
                        //xhr.setRequestHeader('Content-Length', requestData.length);
                    },
                    success: function (data, status) {
                        bytesUploaded += requestData.length;
                        var percentComplete = ((parseFloat(bytesUploaded) / parseFloat(file.size)) * 100).toFixed(2);
                        if (showProgress) {
                            updateUploadingProgress(itemGUID, percentComplete, bytesUploaded);
                        }
                        uploadFileInBlocks();
                    },
                    error: function (xhr, desc, err) {
                        showNotifyError('Lỗi xảy ra khi upload');
                        reject({ errorCode: 101, error: err });
                    }
                });
            }
        };

        function uploadFileInBlocks() {

            if (totalBytesRemaining > 0) {
                var fileContent = file.slice(currentFilePointer, currentFilePointer + maxBlockSize);
                var blockId = blockIdPrefix + pad(blockIds.length, 6);
                //console.log("block id = " + blockId);
                blockIds.push(btoa(blockId));
                reader.readAsArrayBuffer(fileContent);
                currentFilePointer += maxBlockSize;
                totalBytesRemaining -= maxBlockSize;
                if (totalBytesRemaining < maxBlockSize) {
                    maxBlockSize = totalBytesRemaining;
                }
            } else {
                commitBlockList();
            }
        }

        uploadFileInBlocks();

        function commitBlockList() {
            var uri = azUrl + '&comp=blocklist';
            //console.log(uri);
            var requestBody = '<?xml version="1.0" encoding="utf-8"?><BlockList>';
            for (var i = 0; i < blockIds.length; i++) {
                requestBody += '<Latest>' + blockIds[i] + '</Latest>';
            }
            requestBody += '</BlockList>';

            $.ajax({
                url: uri,
                type: "PUT",
                data: requestBody,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('x-ms-blob-content-type', file.type);
                    //xhr.setRequestHeader('Content-Length', requestBody.length);
                },
                success: function (data, status) {
                    resolve({ errorCode: 0 });
                },
                error: function (xhr, desc, err) {
                    showNotifyError('Lỗi xảy ra khi upload');
                    reject({ errorCode: 102, error: err });
                }
            });
        }
    });
}

function executeMainFile($chatHistory, file, log, info, itemGUID, isFull) {
    //console.log(file);
    if (info.fileType == messageType.image) {
        resizeImage(file, maxImageWidth).then(function (res0) {
            var newfile = res0.file;
            //console.log(newfile);
            log.content.width = res0.width;
            log.content.height = res0.height;
            log.content.size = newfile.size;

            uploadMainFile($chatHistory, newfile, log, info, itemGUID, isFull);
        });
    }
    else {
        log.content.size = file.size;
        uploadMainFile($chatHistory, file, log, info, itemGUID, isFull);
    }
}

function uploadMainFile($chatHistory, file, log, info, itemGUID, isFull) {

    uploadFileToCloud(file, info.sasUrl, itemGUID, true).then(function (res) {
        //console.log(res);
        if (res.errorCode == 0) {
            //gửi socket
            log.content.link = info.link;
            log.content.thumbLink = info.thumbLink;
            sendRoomLog($chatHistory, log, isFull);
        }
    });
}

function executeFileToCloud($chatHistory, file, info, roomId, itemGUID, isFull) {

    var log = {
        type: info.fileType,
        roomId: roomId,
        userIdAuthor: currentUserId,
        itemGUID: itemGUID,
        createDate: moment().utc().format(),
        authorInfo: currentUserInfo,
        content: { name: file.name }
    };
    if (isFull) {
        generateLogFull($chatHistory, log, false, file);
    }
    else {
        generateLog($chatHistory, log, false, file);
    }
    scrollToBottom($chatHistory);

    //chưa xử lý video thumbnail
    //tạo thumbnail
    if (info.fileType == messageType.image) {
        resizeImage(file, maxThumbWidth).then(function (res) {
            var thumbfile = res.file;
            log.content.thumbWidth = res.width;
            log.content.thumbHeight = res.height;
            log.content.thumbSize = thumbfile.size;
            //console.log(thumbfile);
            uploadFileToCloud(thumbfile, info.sasThumbUrl, itemGUID, false).then(function (res) {
                executeMainFile($chatHistory, file, log, info, itemGUID, isFull);
            });
        });
    }
    else if (info.fileType == messageType.video) {
        createVideoThumbnail(file, maxThumbWidth).then(function (res) {
            var thumbfile = res.file;
            log.content.thumbWidth = res.width;
            log.content.thumbHeight = res.height;
            log.content.thumbSize = thumbfile.size;
            log.content.videoLength = res.videoLength;

            uploadFileToCloud(thumbfile, info.sasThumbUrl, itemGUID, false).then(function (res) {
                executeMainFile($chatHistory, file, log, info, itemGUID, isFull);
            });
        });
    }
    else {
        executeMainFile($chatHistory, file, log, info, itemGUID, isFull);
    }
}



//file drag drop event

//document event
var dragTimer;
$(document).on('dragover', function (e) {
    var dt = e.originalEvent.dataTransfer;
    if (dt.types != null && (dt.types.indexOf ? dt.types.indexOf('Files') != -1 : dt.types.contains('application/x-moz-file'))) {
        $(".dragandrophandler").addClass('show');
        e.stopPropagation();
        e.preventDefault();
        window.clearTimeout(dragTimer);
    }
});
$(document).on('dragleave', function (e) {
    dragTimer = window.setTimeout(function () {
        $(".dragandrophandler").removeClass('show');
    }, 250);
});
$(document).on('drop', function (e) {
    e.stopPropagation();
    e.preventDefault();
    $(".dragandrophandler").removeClass('show');
});
//file event
function onDragenter(e, el) {
    e.stopPropagation();
    e.preventDefault();
    $(el).css('border', '1px solid green');
    window.clearTimeout(dragTimer);
}
function onDragleave(e, el) {
    //console.log('leave');
    e.stopPropagation();
    e.preventDefault();
    dragTimer = window.setTimeout(function () {
        $(el).removeClass('show');
    }, 250);
    $(el).css('border', '');
}
function onDragover(e) {
    //console.log('over');
    e.stopPropagation();
    e.preventDefault();
    window.clearTimeout(dragTimer);
}
function onDrop(e, el) {
    //console.log('drop');
    $(el).css('border', '');
    e.preventDefault();
    var files = e.dataTransfer.files;
    $(el).removeClass('show');
    //We need to send dropped files to Server
    var roomId = $(el).attr('data-id');
    var $chatHistoryWrapper;

    if (typeof getChatHistoryWrapper === 'function') {

        $chatHistoryWrapper = getChatHistoryWrapper(roomId);
    }
    else { //widget
        $chatHistoryWrapper = $chatHistory;
    }
    //console.log($(el));
    //console.log($(el).data('id'));
    //console.log($chatHistoryWrapper);
    handleFileUpload(files, roomId, $chatHistoryWrapper, globalIsFull);
}

//copy/paste file
function setImageDataUrl(src) {
    //console.log(src);
    imgDataUrl = src;
}
function sendPasteImage() {
    var xhr = new XMLHttpRequest();
    xhr.responseType = 'blob';
    xhr.onload = function () {
        var recoveredBlob = xhr.response;
        //console.log(recoveredBlob);
        recoveredBlob.name = 'Paste-' + moment().format('YYYYMMDDHms') + '.png';
        //console.log(channelAdministratorRoomId);
        var $chatHistoryWrapper;
        if (typeof getChatHistoryWrapper === 'function') {
            $chatHistoryWrapper = getChatHistoryWrapper(channelAdministratorRoomId);
        }
        else {
            $chatHistoryWrapper = $chatHistory;
        }
        handleFileUpload([recoveredBlob], channelAdministratorRoomId, $chatHistoryWrapper, globalIsFull);
        $('#pasteImageToChatBox').closeModal();
    };
    xhr.open('GET', imgDataUrl);
    xhr.send();
}
$(document).ready(function () {
    $('#btSendCBImage').click(function () {
        sendPasteImage();
    });
    $(document).keyup(function (event) {
        if (event.which == 13) {
            if (!$('#pasteImageToChatBox').hasClass('hide')) {
                if (typeof sendPasteImage === 'function') {
                    sendPasteImage();
                }
            }
        }
        if (event.which == 27) {
            if (typeof closeWithEsc === 'function') {
                closeWithEsc();
            }
        }
    });
});

//parse log and update
function parseRoomLogBeforeSend($chatHistory, userId, roomId, rawMessage, isFull) {
    var firstLink = extractFirstLinkFromText(rawMessage);
    var type = messageType.text;
    if (firstLink) {
        type = messageType.link;
    }

    var log = {
        type: type,
        roomId: roomId,
        userIdAuthor: userId,
        itemGUID: generateGUID(),
        createDate: moment().utc().format(),
        authorInfo: currentUserInfo
    };

    var content = {};

    switch (type) {
        case messageType.text: {
            content.content = rawMessage;
            content.contentType = 'text';
            break;
        }
        case messageType.link: {
            content.text = rawMessage;
            content.link = firstLink;
            break;
        }
    }

    log.content = content;

    sendRoomLog($chatHistory, log, isFull);
}

function updateLinkContent(log) {

    var link = log.content.link;
    getLinkPreview(link).then(function (res) {
        //console.log('kết quả crawl');
        //console.log(res);
        socket.emit('updateLink', {
            roomId: log.roomId,
            chatLogId: log._id,
            text: log.content.text,
            link: log.content.link,
            title: res.title,
            description: res.description,
            imageLink: (res.images.length > 0 ? res.images[0] : ''),
            itemGUID: log.itemGUID
        }, function (result) {
            //console.log(result);
        });
    });
}

function parseItemLogBeforeSend($chatHistory, userId, roomId, itemId, itemName, itemImage, itemLink, itemPrice, isFull) {
    var log = {
        type: messageType.item,
        roomId: roomId,
        userIdAuthor: userId,
        itemGUID: generateGUID(),
        createDate: moment().utc().format(),
        authorInfo: currentUserInfo,
        content: {
            itemId: itemId,
            itemName: itemName,
            itemImage: itemImage,
            itemLink: itemLink,
            itemPrice: itemPrice
        }
    };

    sendRoomLog($chatHistory, log, isFull);
}

//text input
var messageEnterTimeStamp;
function clearChatInput($chatTextarea) {
    $chatTextarea.mentionsInput('clear');
    $chatTextarea.val('');
}

function getTextToSend($chatTextarea, $chatHistory, roomId, timeStamp, isFull) {
    var rawMessage = $.trim($chatTextarea.val());
    //console.log(rawMessage.length);
    if (rawMessage == '' || !rawMessage) {
        clearChatInput($chatTextarea);
    }
    else {
        if (timeStamp != messageEnterTimeStamp) { //loại bỏ trường hợp sự kiện Enter phát 2 lần trên 1 số trình duyệt
            messageEnterTimeStamp = timeStamp;
            //console.log(rawMessage);
            rawMessage = $chatTextarea.mentionsInput('getValue');
            //console.log(rawMessage);
            clearChatInput($chatTextarea);
            parseRoomLogBeforeSend($chatHistory, currentUserId, roomId, rawMessage, isFull);
        }
        else {
            clearChatInput($chatTextarea);
        }
    }
}



//Get logs
function getRoomLog($chatHistory, roomId, lastLogId, isFull) {
    var $current_top_element = $chatHistory.find('ul').children().first();
    socket.emit('getRoomLogs',
        {
            roomId: roomId,
            lastLogId: lastLogId
        }, function (result) {
            //console.log(result);
            if (result.errorCode == 0) {
                var logs = result.data;
                //console.log('logs');
                //console.log(logs);
                if (logs) {

                    for (var i = 0; i < logs.length; i++) {
                        var log = logs[i];

                        if (isFull) {
                            generateLogFull($chatHistory, log, true);
                        }
                        else {
                            generateLog($chatHistory, log, true);
                        }
                    }

                    if (!lastLogId) { //lần đầu
                        scrollToBottom($chatHistory);
                        //lấy số lượng tin chưa đọc
                        //getRoomUnreadCount(logs);
                    }
                    else {
                        //scroll tới message mới load
                        var previous_height = 0;
                        $current_top_element.prevAll().each(function () {
                            previous_height += $(this).outerHeight();
                        });

                        $chatHistory.scrollTop(previous_height);
                    }

                    if (logs.length > 0) {
                        $chatHistory.bind('scroll', function () {
                            if ($(this).innerHeight() < this.scrollHeight) {
                                if ($(this).scrollTop() <= 15) {
                                    $(this).unbind('scroll');
                                    var logId = $chatHistory.find('ul li.chat-item:first-child').data('id');
                                    getRoomLog($chatHistory, roomId, logId, isFull);
                                }
                            }

                            var $scrollHandle = $chatHistory.find('.scroll-down-wrapper');
                            //console.log($scrollHandle);
                            //console.log('scrollTop', $(this).scrollTop());
                            //console.log('scrollHeight', $(this).prop('scrollHeight'));
                            //console.log('clientHeight', $(this).prop('clientHeight'));
                            if ($(this).scrollTop() + $(this).prop('clientHeight') >= $(this).prop('scrollHeight') - 30) {
                                //if (nếu ko có hiện thì mới cho hiện) {
                                //console.log('hide scroll');
                                $scrollHandle.fadeOut(500);
                                //$scrollHandle.animate({
                                //scrollTop: $(this).prop('scrollHeight') + 200
                                //},
                                //  500, 'linear');
                                //}
                            }
                            else {
                                $scrollHandle.fadeIn(500);
                                //$scrollHandle.animate({
                                //    scrollTop: $(this).prop('scrollHeight')
                                //},
                                //    500, 'linear');
                            }
                        });
                    }
                }
            }
            else {
                showNotifyError('Lỗi không lấy được log!');
            }
        });
}

function sendRoomLog($chatHistory, log, isFull, callback) {
    if (currentReplyLogs[log.roomId]) {
        log.replyOrForward = 'REPLY';
        var originLog = currentReplyLogs[log.roomId];
        log.originMessage = {
            _id: originLog._id,
            roomId: originLog.roomId,
            type: originLog.type,
            content: originLog.content,
            authorInfo: {
                _id: originLog.authorInfo._id,
                name: originLog.authorInfo.name,
                avatar: originLog.authorInfo.avatar,
                phone: originLog.authorInfo.phone
            }
        };
    }
    //console.log(log);    
    if (isFull) {
        generateLogFull($chatHistory, log);
    }
    else {
        generateLog($chatHistory, log);
    }
    scrollToBottom($chatHistory); //scroll tới cuối khi chat vào
    if (log.replyOrForward == 'REPLY') {
        socket.emit('replyMessage', {
            roomId: log.roomId,
            type: log.type,
            content: log.content,
            originMessage: log.originMessage,
            itemGUID: log.itemGUID
        }, function (result) {
            closeReplyPopup(log.roomId);
            updateLog($chatHistory, result);
            if (result.errorCode == 0 && log.type == messageType.link) {
                updateLinkContent(result.data);
            }
        });
    }
    else {
        switch (log.type) {
            case messageType.text: {
                //console.log(log.content.content);
                socket.emit('sendText', {
                    roomId: log.roomId, text: log.content.content, itemGUID: log.itemGUID
                }, function (result) {
                    updateLog($chatHistory, result);

                });

                setTimeout(function () {
                    updateLogStatus($chatHistory, log, false);
                }, 10000);
                break;
            }
            case messageType.link: {
                socket.emit('sendLink', {
                    roomId: log.roomId, text: log.content.text, link: log.content.link, itemGUID: log.itemGUID
                }, function (result) {
                    updateLog($chatHistory, result);
                    if (result.errorCode == 0) {
                        updateLinkContent(result.data);
                    }
                });

                setTimeout(function () {
                    updateLogStatus($chatHistory, log, false);
                }, 10000);
                break;
            }
            case messageType.image: {
                socket.emit('sendImage', {
                    roomId: log.roomId, fileName: log.content.name,
                    link: log.content.link, thumbLink: log.content.thumbLink,
                    height: log.content.height, width: log.content.width, size: log.content.size,
                    thumbHeight: log.content.thumbHeight, thumbWidth: log.content.thumbWidth, thumbSize: log.content.thumbSize,
                    itemGUID: log.itemGUID
                }, function (result) {
                    updateLog($chatHistory, result);
                });
                break;
            }
            case messageType.video: {
                socket.emit('sendVideo', {
                    roomId: log.roomId, fileName: log.content.name,
                    link: log.content.link, thumbLink: log.content.thumbLink,
                    height: log.content.height, width: log.content.width, size: log.content.size,
                    thumbHeight: log.content.thumbHeight, thumbWidth: log.content.thumbWidth, thumbSize: log.content.thumbSize,
                    itemGUID: log.itemGUID
                }, function (result) {
                    updateLog($chatHistory, result);
                });
                break;
            }
            case messageType.file: {
                socket.emit('sendFile', {
                    roomId: log.roomId, fileName: log.content.name,
                    link: log.content.link, thumbLink: log.content.thumbLink,
                    height: log.content.height, width: log.content.width, size: log.content.size,
                    thumbHeight: log.content.thumbHeight, thumbWidth: log.content.thumbWidth, thumbSize: log.content.thumbSize,
                    itemGUID: log.itemGUID
                }, function (result) {
                    updateLog($chatHistory, result);
                });
                break;
            }
            case messageType.item: {
                socket.emit('sendItem', {
                    roomId: log.roomId,
                    itemId: log.content.itemId,
                    itemName: log.content.itemName,
                    itemImage: log.content.itemImage,
                    itemLink: log.content.itemLink,
                    itemPrice: log.content.itemPrice,
                    itemGUID: log.itemGUID
                }, function (result) {
                    updateLog($chatHistory, result);
                });
                break;
            }
            case messageType.plan: {
                socket.emit('sendPlan', {
                    roomId: log.roomId,
                    title: log.content.title,
                    timeStamp: log.content.timeStamp,
                    duration: log.content.duration,
                    place: log.content.place,
                    note: log.content.note,
                    itemGUID: log.itemGUID
                }, function (result) {
                    updateLog($chatHistory, result);
                    if (typeof callback == 'function') {
                        callback(result);
                    }
                });
                break;
            }
        }
    }
}

function toggleSendButton(roomId, value) {
    if (value != '') {
        $('#btSendText' + roomId).removeClass('hide');
        $('#btSendLike' + roomId).addClass('hide');
    }
    else {
        $('#btSendText' + roomId).addClass('hide');
        $('#btSendLike' + roomId).removeClass('hide');
    }
}
//socket


//document click
///
$(document).bind('click', function (e) {
    //console.log(e.target);
    //console.log($(e.target).hasClass('btEmoji'));
    if ($(e.target).hasClass('emoji-wrapper') || $(e.target).closest('.emoji-wrapper').length > 0 || $(e.target).hasClass('btEmoji') || $(e.target).closest('.btEmoji').length > 0) {

    }
    else {
        $('.emoji-wrapper').hide();
    }

    if ($(e.target).hasClass('.btAccountMenu') || $(e.target).closest('.btAccountMenu').length > 0) {

    }
    else {
        $('.btAccountMenu').removeClass('open');
        $('.accountInfoDropdown').hide();
    }

    if ($(e.target).hasClass('.menuButton') || $(e.target).closest('.menuButton').length > 0) {

    }
    else {
        $('.menuMain').hide();
    }

    if ($(e.target).hasClass('.dropdown-button') || $(e.target).closest('.dropdown-button').length > 0
        || $(e.target).hasClass('.form_datetime') || $(e.target).closest('.form_datetime').length > 0) {
    }
    else {
        $('.dropdown-button').removeClass('open');
        $('.dropdown-menu').hide();
    }
});
$(document).ready(function () {
    $.fn.datetimepicker.dates[`vi`] = {
        days: ["Chủ Nhật", "Thứ Hai", "Thứ Ba", "Thứ Tư", "Thứ Năm", "Thứ Sáu", "Thứ Bảy", "Chủ Nhật"],
        daysShort: ["CN", "Thứ 2", "Thứ 3", "Thứ 4", "Thứ 5", "Thứ 6", "Thứ 7", "CN"],
        daysMin: ["CN", "T2", "T3", "T4", "T5", "T6", "T7", "CN"],
        months: ["Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6", "Tháng 7", "Tháng 8", "Tháng 9", "Tháng 10", "Tháng 11", "Tháng 12"],
        monthsShort: ["Th 1", "Th 2", "Th 3", "Th 4", "Th 5", "Th 6", "Th 7", "Th 8", "Th 9", "Th 10", "Th 11", "Th 12"],
        today: "Hôm nay"
    };

});
$(document).on('focusin', '.input-group input', function (e) {
    //console.log('focus');
    $(e.target).parent().find('label').addClass('active');
    $(e.target).parent().find('label').addClass('focus');
    $(e.target).addClass('focus');
});

$(document).on('focusout', '.input-group input', function (e) {
    //console.log('focus out');
    if (!e.target.value) {
        $(e.target).parent().find('label').removeClass('active');
    }
    $(e.target).parent().find('label').removeClass('focus');
    $(e.target).removeClass('focus');
});