﻿

/**
 * image pasting into canvas
 * 
 * @param {string} canvas_id - canvas id
 * @param {boolean} autoresize - if canvas will be resized
 */
function CLIPBOARD_CLASS(canvasId, modalId, isFull) {
    var _self = this;
    //var canvas = document.getElementById(canvas_id);
    //var ctx = document.getElementById(canvas_id).getContext("2d");
    var ctrl_pressed = false;
    var command_pressed = false;
    var paste_event_support;
    var pasteCatcher;

    //handlers
    document.addEventListener('keydown', function (e) {
        _self.on_keyboard_action(e);
    }, false); //firefox fix
    document.addEventListener('keyup', function (e) {
        _self.on_keyboardup_action(e);
    }, false); //firefox fix
    document.addEventListener('paste', function (e) {
        _self.paste_auto(e);
    }, false); //official paste handler

    //constructor - we ignore security checks here
    this.init = function () {
        pasteCatcher = document.createElement("div");
        pasteCatcher.setAttribute("id", "paste_ff");
        pasteCatcher.setAttribute("contenteditable", "");
        pasteCatcher.style.cssText = 'opacity:0;position:fixed;top:0px;left:0px;width:10px;margin-left:-10000px;';
        document.body.appendChild(pasteCatcher);

        // create an observer instance
        var observer = new MutationObserver(function (mutations) {
            mutations.forEach(function (mutation) {
                if (paste_event_support === true || ctrl_pressed == false || mutation.type != 'childList') {
                    //we already got data in paste_auto()
                    return true;
                }

                //if paste handle failed - capture pasted object manually
                if (mutation.addedNodes.length == 1) {
                    if (mutation.addedNodes[0].src != undefined) {
                        //image
                        _self.paste_createImage(mutation.addedNodes[0].src);
                    }
                    //else if (mutation.addedNodes[0].data != undefined) {
                    //    console.log(mutation.addedNodes[0].data);
                    //    console.log('el: ' + $(document.activeElement).html());
                    //    if (currentRoomId != undefined) {
                    //        var $el = $('.fullChatBox[data-room-id=' + currentRoomId + '] .boxFooter textarea');
                    //        if (!isFull) {
                    //            $el = $('.ChatBox[data-room-id=' + currentRoomId + '] .ChatFlyoutFooter textarea')
                    //        }
                    //        if ($el.is(''))
                    //        $el.html(mutation.addedNodes[0].data);
                    //    }

                    //}
                    //register cleanup after some time.
                    setTimeout(function () {
                        pasteCatcher.innerHTML = '';
                    }, 20);
                }
            });
        });
        var target = document.getElementById('paste_ff');
        var config = { attributes: true, childList: true, characterData: true };
        observer.observe(target, config);
    } ();

    //default paste action
    this.paste_auto = function (e) {
        //console.log('pasteCatcher: ' + pasteCatcher);
        paste_event_support = false;
        if (pasteCatcher != undefined) {
            pasteCatcher.innerHTML = '';
        }
        //console.log('clipboard: ' + JSON.stringify(e.clipboardData));
        if (e.clipboardData) {
            var items = e.clipboardData.items;
            //console.log('items: ' + items);
            if (items) {
                //console.log(items.length);
                paste_event_support = true;
                //access data directly
                for (var i = 0; i < items.length; i++) {
                    //console.log(items[i].type);
                    if (items[i].type.indexOf("image") !== -1) {
                        //image
                        var blob = items[i].getAsFile();
                        var URLObj = window.URL || window.webkitURL;
                        var source = URLObj.createObjectURL(blob);
                        this.paste_createImage(source);
                    }
                    else {
                        //if(document.activeElement.type == 'text' || document.activeElement.type == 'textarea')
                        //{
                        //    console.log('text: ' + items[i].getData('text'));
                        //    $(document.activeElement).html(items[i].getData('text'));
                        //}
                    }
                }
                //e.preventDefault();
            }
            else {
                //wait for DOMSubtreeModified event
                //https://bugzilla.mozilla.org/show_bug.cgi?id=891247
            }
        }
    };
    //on keyboard press
    this.on_keyboard_action = function (event) {
        k = event.keyCode;
        //ctrl
        if (k == 17 || event.metaKey || event.ctrlKey) {
            if (ctrl_pressed == false)
                ctrl_pressed = true;
        }
        //v
        if (k == 86) {

            //console.log('clipboard 0: ' + window.clipboardData);

            if (document.activeElement != undefined && (document.activeElement.type == 'text' || document.activeElement.type == 'textarea')) {
                //let user paste into some input
                return false;
            }

            if (ctrl_pressed == true && pasteCatcher != undefined) {
                pasteCatcher.focus();
            }
        }
    };
    //on kaybord release
    this.on_keyboardup_action = function (event) {
        //ctrl
        if (event.ctrlKey == false && ctrl_pressed == true) {
            ctrl_pressed = false;
        }
        //command
        else if (event.metaKey == false && command_pressed == true) {
            command_pressed = false;
            ctrl_pressed = false;
        }
    };

    //draw pasted image to canvas
    this.paste_createImage = function (source) {
        //console.log(currentRoomId);
        if (channelAdministratorRoomId) {
            var $image;
            var isTaskAtt = false;

            //chat
            $("#" + modalId).showModal();
            setImageDataUrl(source);
            if (!isFull) {
                sendPasteImage();
            }
            else {
                $image = $('#' + canvasId);
                //console.log(source);
                //var pastedImage = new Image();            

                var options = {
                    autoCropArea: 1,
                    preview: '.img-preview',
                    minContainerWidth: 670,
                    minContainerHeight: 400,
                    crop: function (e) {
                        //$dataX.val(Math.round(e.x));
                        //$dataY.val(Math.round(e.y));
                        //$dataHeight.val(Math.round(e.height));
                        //$dataWidth.val(Math.round(e.width));
                        //$dataRotate.val(e.rotate);
                        //$dataScaleX.val(e.scaleX);
                        //$dataScaleY.val(e.scaleY);
                    }
                };

                $image.cropper('destroy').attr('src', source).cropper(options);

                $('.docs-buttons').on('click', '[data-method]', function () {
                    var $this = $(this);
                    var data = $this.data();
                    var $target;
                    var result;

                    if ($this.prop('disabled') || $this.hasClass('disabled')) {
                        return;
                    }

                    if ($image.data('cropper') && data.method) {
                        data = $.extend({}, data); // Clone a new one

                        if (typeof data.target !== 'undefined') {
                            $target = $(data.target);

                            if (typeof data.option === 'undefined') {
                                try {
                                    data.option = JSON.parse($target.val());
                                } catch (e) {
                                    console.log(e.message);
                                }
                            }
                        }

                        if (data.method === 'rotate') {
                            $image.cropper('clear');
                        }

                        result = $image.cropper(data.method, data.option, data.secondOption);

                        if (data.method === 'rotate') {
                            $image.cropper('crop');
                        }

                        switch (data.method) {
                            case 'scaleX':
                            case 'scaleY':
                                $(this).data('option', -data.option);
                                break;

                            case 'getCroppedCanvas':
                                if (result) {

                                    // Bootstrap's Modal
                                    //$('#getCroppedCanvasModal').modal().find('.modal-body').html(result);
                                    //console.log(result);
                                    result.toBlob(function (blob) {
                                        $image.cropper('destroy').attr('src', URL.createObjectURL(blob)).cropper(options);
                                        if (!isTaskAtt) {
                                            setImageDataUrl(URL.createObjectURL(blob));
                                        }
                                        else {
                                            setAttImageDataUrl(URL.createObjectURL(blob));
                                        }
                                    });

                                    //if (!$download.hasClass('disabled')) {
                                    //    $download.attr('href', result.toDataURL('image/jpeg'));
                                    //}
                                }

                                break;

                            case 'destroy':
                                if (uploadedImageURL) {
                                    URL.revokeObjectURL(uploadedImageURL);
                                    uploadedImageURL = '';
                                    $image.attr('src', originalImageURL);
                                }

                                break;
                        }

                        if ($.isPlainObject(result) && $target) {
                            try {
                                $target.val(JSON.stringify(result));
                            } catch (e) {
                                console.log(e.message);
                            }
                        }

                    }
                });
            }
        }
    };
}

