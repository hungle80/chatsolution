﻿var SnippetAccount = function () {
    jQuery.validator.addMethod('mobileVN', function (phone_number, element) {
        phone_number = phone_number.replace(/\s+|-/g, '');
        return this.optional(element) || phone_number.length > 9 &&
            phone_number.match(/^((09|03|07|08|05)+([0-9]{8})\b)$/);
    }, 'Số mobile không đúng định dạng');

    jQuery.validator.addMethod('checkRetype', function (rpassword, element) {
        var newpassword = $(element).closest('form').find('[name=password]').val();
        return rpassword == newpassword;
    }, 'Mật khẩu nhập lại không trùng!');

    function showMainAccount() {
        $('#mainAccount').show();
        $('#updateAccountInfo').hide();
        $('#updateAccountPassword').hide();
    }

    function showUpdateAccount() {
        $('#mainAccount').hide();
        $('#updateAccountInfo').show();
        $('#updateAccountPassword').hide();
    }

    function showUpdatePassword() {
        $('#mainAccount').hide();
        $('#updateAccountInfo').hide();
        $('#updateAccountPassword').show();
    }

    var switchFormDisplay = function () {
        $('.btBackToMainAccount').click(function () {
            showMainAccount();
        });

        $('#btShowUpdateAccount').click(function () {
            showUpdateAccount();
        });

        $('#btShowUpdatePassword').click(function () {
            showUpdatePassword();
        });
    }

    var handleUpdateAccountInfo = function () {
        
        $('#btUpdateAccountInfo').click(function (e) {
            e.preventDefault();
            var form = $('#formUpdateAccountInfo');

            form.validate({
                rules: {
                    fullname: {
                        required: true
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    mobile: {
                        required: true,
                        mobileVN: true
                    }
                },
                messages: {
                    fullname: "Cần nhập họ tên",
                    email: {
                        required: "Cần nhập email",
                        email: "Email không đúng định dạng"
                    },
                    mobile: {
                        required: "Cần nhập mobile"
                    }
                }
            });


            if (!form.valid()) {
                return;
            }

            var email = form.find('[name=email]').val();
            var mobile = form.find('[name=mobile]').val();
            var fullname = form.find('[name=fullname').val();

            var data = {
                email: email,
                mobile: mobile,
                fullname: fullname
            };
            socket.emit('updateUserInfo', data, function (result) {
                if (result.errorCode == 0) {
                    currentUserInfo.email = email;
                    currentUserInfo.mobile = mobile;
                    currentUserInfo.name = fullname;
                    loadAccountInfo();
                    showMainAccount();
                }
                else {
                    showNotifyError(result.error);
                }
            });
        });
    }

    var handleUpdateAccountPassword = function () {
        
        $('#btUpdateAccountPassword').click(function (e) {
            e.preventDefault();
            var form = $('#formUpdateAccountPassword');

            form.validate({
                rules: {
                    opassword: {
                        required: true
                    },
                    password: {
                        required: true
                    },
                    rpassword: {
                        required: true,
                        checkRetype: true
                    }
                },
                messages: {
                    opassword: "Cần nhập mật khẩu cũ",
                    password: "Cần nhập mật khẩu mới",
                    rpassword: {
                        required: "Cần nhập lại mật khẩu mới"
                    }
                }
            });


            if (!form.valid()) {
                return;
            }

            var oldPassword = form.find('[name=opassword]').val();
            var newPassword = form.find('[name=password]').val();
            //var rNewPassword = form.find('[name=rpassword]').val();

            var data = {
                oldPassword: oldPassword,
                newPassword: newPassword
            };

            socket.emit('updateUserPassword', data, function (result) {
                if (result.errorCode == 0) {
                    showMainAccount();
                }
                else {
                    showNotifyError(result.error);
                }
            });
        });
    }

    var handleUpdateUserAvatar = function () {

    }

    var loadAccountInfo = function () {
        var form = $('#formUpdateAccountInfo');
        if (currentUserInfo) {
            form.find('[name=email]').val(currentUserInfo.email);
            form.find('[name=mobile]').val(currentUserInfo.phone);
            form.find('[name=fullname]').val(currentUserInfo.name);
        }
    }

    return {
        init: function () {
            loadAccountInfo();
            switchFormDisplay();
            handleUpdateAccountInfo();
            handleUpdateAccountPassword();
            handleUpdateUserAvatar();
        }
    };
}();

//== Class Initialization
jQuery(document).ready(function () {
    SnippetAccount.init();
});