﻿// Client ID and API key from the Developer Console
var CLIENT_ID = '826691770642-i9dbfbch5db8h6qlpq76ph8ho5hr9n10.apps.googleusercontent.com';
var API_KEY = 'AIzaSyDtOVT1uj8uBH4tZzJXYP8bo2J7FS0FV-w';

// Array of API discovery doc URLs for APIs used by the quickstart
var DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest"];

// Authorization scopes required by the API; multiple scopes can be
// included, separated by spaces.
var SCOPES = "https://www.googleapis.com/auth/calendar.events https://www.googleapis.com/auth/calendar.readonly";

function handleClientLoad() {
    gapi.load('client:auth2', initClient);
}

var authorizeButton, signoutButton;

function initClient() {
    setTimeout(function () {
        authorizeButton = document.getElementById('authorize_button');
        signoutButton = document.getElementById('signout_button');

        gapi.client.init({
            apiKey: API_KEY,
            clientId: CLIENT_ID,
            discoveryDocs: DISCOVERY_DOCS,
            scope: SCOPES
        }).then(function () {
            // Listen for sign-in state changes.
            gapi.auth2.getAuthInstance().isSignedIn.listen(updateSigninStatus);

            // Handle the initial sign-in state.
            updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
            authorizeButton.onclick = handleAuthClick;
            signoutButton.onclick = handleSignoutClick;
        }, function (error) {
            appendPre(JSON.stringify(error, null, 2));
        });
    }, 200);
}
var isSignedInGoogleForCalendar = false;
function updateSigninStatus(isSignedIn) {
    isSignedInGoogleForCalendar = isSignedIn;
    console.log('is google signin', isSignedIn);
    if (isSignedIn) {
        //authorizeButton.style.display = 'none';
        //signoutButton.style.display = 'block';
        //listUpcomingEvents();
    } else {
        //console.log(authorizeButton);
        //console.log(signoutButton);
        //authorizeButton.style.display = 'block';
        //signoutButton.style.display = 'none';
        //handleAuthClick();
    }
}

function handleAuthClick(event) {
    gapi.auth2.getAuthInstance().signIn();
}

function handleSignoutClick(event) {
    gapi.auth2.getAuthInstance().signOut();
}

function appendPre(message) {
    var pre = document.getElementById('content');
    var textContent = document.createTextNode(message + '\n');
    pre.appendChild(textContent);
}

function addCalendarEvent(log, status) {
    //gapi.client.calendar.events.get({
    //    'calendarId': 'primary',
    //    'eventId': log._id
    //})
    //    .then(function (response) {
    //        var eventX = response.result.item;
    //        if (eventX) {
    //        }
    //        else {
    var xStatus = status == 'yes' ? 'confirmed' : 'tentative';
    getCalendarEvent(log._id, function (eventX) {
        if (!eventX) {
            var content = log.content;
            var start = moment(content.timeStamp * 1000);
            var end = moment(start).add(content.duration, 'm');
            
            var event = {
                'id': log._id,
                'summary': content.title,
                'location': content.place ? content.place.address : '',
                'description': content.note,
                'start': {
                    'dateTime': start.format('YYYY-MM-DDTHH:mm:ssZ')
                },
                'end': {
                    'dateTime': end.format('YYYY-MM-DDTHH:mm:ssZ')
                },
                'recurrence': [
                ],
                'reminders': {
                    'useDefault': false,
                    'overrides': [
                        { 'method': 'email', 'minutes': 24 * 60 },
                        { 'method': 'popup', 'minutes': 10 }
                    ]
                },
                status: xStatus
            };

            gapi.client.calendar.events.insert({
                'calendarId': 'primary',
                'resource': event
            }).execute(function (event) {
                console.log('Event created: ' + event.htmlLink);
            });
        }
        else {
            eventX.status = xStatus;
            gapi.client.calendar.events.patch({
                'calendarId': 'primary',
                'eventId': eventX.id,
                'resource': eventX
            }).execute(function (ev) {
                console.log(ev);
            });
        }
    });



    //}
    //});
}

function removeCalendarEvent(log) {
    getCalendarEvent(log._id, function (eventX) {
        if (eventX) {
            gapi.client.calendar.events.delete({
                'calendarId': 'primary',
                'eventId': log._id
            }).execute(function (ev) {
                console.log(ev);
            });
        }
    });
}

function getCalendarEvent(eventId, callback) {
    gapi.client.calendar.events.get({
        'calendarId': 'primary',
        'eventId': eventId
    }).execute(function (event) {
        //console.log('get cal event');
        if (event.code == 404) {
            callback(null);
        }
        else {
            callback(event);
        }
    });

}

function listUpcomingEvents() {
    gapi.client.calendar.events.list({
        'calendarId': 'primary',
        'timeMin': (new Date()).toISOString(),
        'showDeleted': true,
        'singleEvents': true,
        'maxResults': 10,
        'orderBy': 'startTime'
    }).then(function (response) {
        var events = response.result.items;
        //appendPre('Upcoming events:');
        console.log(events);
        //if (events.length > 0) {
        //    for (i = 0; i < events.length; i++) {
        //        var event = events[i];
        //        var when = event.start.dateTime;
        //        if (!when) {
        //            when = event.start.date;
        //        }
        //        appendPre(event.summary + ' (' + when + ')')
        //    }
        //} else {
        //    appendPre('No upcoming events found.');
        //}
    });
}