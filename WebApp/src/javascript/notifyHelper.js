﻿
function showNotify(title, message, type) {
    new PNotify({
        title: title,
        text: message,
        type: type,
        history: {
            history: false
        },
    });
}

function showNotifySuccess(message) {
    showNotify('Thông báo', message, 'success', false);
}

function showNotifyError(message) {
    showNotify('Lỗi', message, 'error', false);
}

function confirmNotify(message, callback) {
    (new PNotify({
        title: 'Xác nhận',
        text: message,
        icon: 'fa fa-question-circle-o',
        hide: false,
        confirm: {
            confirm: true
        },
        //buttons: {
        //    closer: false,
        //    sticker: false
        //},
        history: {
            history: false
        },
        addclass: 'stack-modal',
        stack: {
            'dir1': 'down',
            'dir2': 'right',
            'modal': true
        }
    })).get().on('pnotify.confirm', function () {
        callback(true);
    }).on('pnotify.cancel', function () {
        callback(false);
    });
}
