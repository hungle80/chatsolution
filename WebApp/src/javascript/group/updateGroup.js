﻿var updatingGroupId;
function revokeUpdateGroupLink() {
    socket.emit('generatePrivateJoinLink', {}, function (result) {
        //console.log(result);
        if (result.errorCode == 0) {
            $('#updateGroupPrivateJoinLink').html(result.data);
            $('#updateGroupPrivateJoinLink').prop('href', result.data);
        }
    });
}
function loadingGroupInfoForUpdate(roomId, callback) {
    updatingGroupId = roomId;
    socket.emit('getChatRoom', { roomId: roomId }, function (result) {
        console.log(result);
        if (result.errorCode == 0) {
            var info = result.data;
            console.log(info);
            $('#tbUpdateGroupName').parent().find('label').addClass('active');
            $('#tbUpdateGroupName').val(info.roomName);
            if (info.description) {
                $('#tbUpdateGroupDescription').parent().find('label').addClass('active');
            }
            $('#tbUpdateGroupDescription').val(info.description);
            if (info.isPrivate) {
                $('#ckUpdateGroupPublicGroup').prop('checked', false);
                $('#ckUpdateGroupPrivateGroup').prop('checked', true);
                $('#updateGroupPublicLinkWrapper').addClass('hide');
                $('#updateGroupPrivateLinkWrapper').addClass('hide'); //ẩn luôn link khi set private

                $('#updateGroupPrivateJoinLink').html(info.joinLink);
                $('#updateGroupPrivateJoinLink').prop('href', info.joinLink);

                $('#tbUpdateGroupPublicLink').val('');
            }
            else {
                $('#ckUpdateGroupPublicGroup').prop('checked', true);
                $('#ckUpdateGroupPrivateGroup').prop('checked', false);
                $('#updateGroupPublicLinkWrapper').removeClass('hide');
                $('#updateGroupPrivateLinkWrapper').addClass('hide');
                if (info.joinLink) {
                    $('#tbUpdateGroupPublicLink').parent().find('label').addClass('active');
                }
                $('#tbUpdateGroupPublicLink').val(info.joinLink);
                revokeUpdateChannelLink();
            }

            if (info.onlyAdminAddUser) {
                $('#ckUpdateGroupOnlyAdminAddUser').prop('checked', true);
                $('#ckUpdateGroupAllUserAddUser').prop('checked', false);
            }
            else {
                $('#ckUpdateGroupOnlyAdminAddUser').prop('checked', false);
                $('#ckUpdateGroupAllUserAddUser').prop('checked', true);
            }
        }
    });
}

function showEditGroupModal(roomId) {
    $('#updateGroupModal').showModal();
    loadingGroupInfoForUpdate(roomId, function () {
    });
}
$(document).ready(function () {
    $('#ckUpdateGroupPublicGroup').click(function () {
        $('#updateGroupPublicLinkWrapper').removeClass('hide');
        $('#updateGroupPrivateLinkWrapper').addClass('hide');
    });

    $('#ckUpdateGroupPrivateGroup').click(function () {
        $('#updateGroupPublicLinkWrapper').addClass('hide');
        $('#updateGroupPrivateLinkWrapper').addClass('hide'); //ẩn luôn link khi set private
    });

    $('#btRevokeUpdateGroupLink').click(function () {
        revokeUpdateGroupLink();
    });

    $('#tbUpdateGroupName').keyup(function (e) {
        if ($('#tbUpdateGroupName').val() != '') {
            $('#tbUpdateGroupName').parent().find('label').removeClass('error');
            $('#tbUpdateGroupName').removeClass('error');
        }
    });

    function checkUpdateGroupInput() {
        var ok = true;
        if ($('#tbUpdateGroupName').val() == '') {
            ok = false;
            $('#tbUpdateGroupName').parent().find('label').addClass('error');
            $('#tbUpdateGroupName').addClass('error');
        }
        return ok;
    }
    $('#btUpdateGroup').click(function () {
        if (checkUpdateGroupInput()) {
            //
            var isPrivate = $('#ckUpdateGroupPrivateGroup').prop('checked');
            console.log(isPrivate);
            var joinLink = isPrivate ? $('#updateGroupPrivateJoinLink').html() : $('#tbUpdateGroupPublicLink').val();
            var data = {
                roomId: updatingGroupId,
                roomName: $('#tbUpdateGroupName').val().trim(),
                description: $('#tbUpdateGroupDescription').val().trim(),
                isPrivate: isPrivate,
                joinLink: joinLink,
                onlyAdminAddUser: $('#ckUpdateGroupOnlyAdminAddUser').prop('checked')
            };
            socket.emit('updateGroup', data, function (result) {
                console.log(result);
                if (result.errorCode == 0) {
                    if (roomInfos[data.roomId]) {
                        roomInfos[data.roomId].roomName = data.roomName;
                        roomInfos[data.roomId].description = data.description;
                        roomInfos[data.roomId].isPrivate = data.isPrivate;
                        roomInfos[data.roomId].joinLink = data.joinLink;
                        roomInfos[data.roomId].onlyAdminAddUser = data.onlyAdminAddUser;
                    }
                    $('#updateGroupModal').closeModal();
                }
                else {
                    showNotifyError('Lỗi xảy ra khi update kênh chat');
                    console.log(result);
                }
            });
        }
    });

    $('#btCancelUpdateGroup').click(function () {
        $('#updateGroupModal').closeModal();
    });
});