﻿function resizeImage(file, maxWidth) {
    //var img = document.createElement("img");
    return new Promise(function (resolve, reject) {
        //console.log(file);
        var img = new Image();
        img.onload = function () {
            canvas = $("#uploading_canvas").get(0);

            var MAX_WIDTH = maxWidth;
            var width = img.width;
            var height = img.height;

            if (width > MAX_WIDTH) {
                height *= MAX_WIDTH / width;
                width = MAX_WIDTH;
            }

            canvas.width = width;
            canvas.height = height;
            var ctx = canvas.getContext("2d");
            ctx.drawImage(img, 0, 0, width, height);

            var ext = getFileExtension(file.name);
            if (ext == 'jpg') {
                ext = 'jpeg';
            }
            //console.log(ext);
            var fileName = file.name;
            //var dataurl = canvas.toDataURL("image/" + ext + "");
            canvas.toBlob(function (blob) {
                //console.log(blob);
                blob.name = fileName;
                resolve({ file: blob, width: width, height: height });
            }, 'image/' + ext);
        };

        img.src = window.URL.createObjectURL(file);
    });
}

function createVideoThumbnail(file, maxThumbWidth) {
    //var img = document.createElement("img");
    return new Promise(function (resolve, reject) {

        var fileReader = new FileReader();
        fileReader.onload = function () {
            var blob = new Blob([fileReader.result], { type: file.type });
            var url = URL.createObjectURL(blob);
            var video = document.createElement('video');

            var timeupdate = function () {
                if (snapImage()) {
                    video.removeEventListener('timeupdate', timeupdate);
                    video.pause();
                }
            };

            video.addEventListener('loadeddata', function () {
                if (snapImage()) {
                    video.removeEventListener('timeupdate', timeupdate);
                }
            });

            var snapImage = function () {
                var canvas = document.createElement('canvas');
                canvas.width = maxThumbWidth;
                canvas.height = (maxThumbWidth / video.videoWidth) * video.videoHeight;
                canvas.getContext('2d').drawImage(video, 0, 0, canvas.width, canvas.height);
                var image = canvas.toDataURL();
                var success = image.length > 10;
                if (success) {
                    var fileName = file.name;
                    canvas.toBlob(function (blob) {
                        //console.log(blob);
                        blob.name = fileName.substring(0, fileName.lastIndexOf('.')) + 'jpg';
                        console.log(blob.name);
                        resolve({ file: blob, width: canvas.width, height: canvas.height, videoLength: Math.round(video.duration) });
                    }, 'image/jpeg');
                }
                return success;
            };

            video.addEventListener('timeupdate', timeupdate);
            video.preload = 'metadata';
            video.src = url;
            // Load video in Safari / IE11
            video.muted = true;
            video.playsInline = true;
            video.play();

        }

        fileReader.readAsArrayBuffer(file);
    });
}