﻿$(document).on('click', '.btCloseModal', function (e) {
    $(this).closest('.modal').addClass('hide');
    $(this).trigger('modal.closeWithoutChange');
});
function closeWithEsc() {
    var $modal = $('.modal:not(.hide):last');
    $modal.addClass('hide');
    $modal.removeClass('show');
    $modal.css('display', '');
    $modal.trigger('modal.closeWithoutChange');
    if ($('.modal.show').length == 0) {
        $('body').removeClass('modal-open');
    }
}
$.fn.closeModal = function () {    
    this.addClass('hide');
    this.removeClass('show');
    this.css('display', '');
    if ($('.modal.show').length == 0) {
        $('body').removeClass('modal-open');
    }
};
$.fn.showModal = function (callback) {
    this.removeClass('hide');
    this.addClass('show');
    this.css('display', 'block');
    this.appendTo('body');
    $('body').addClass('modal-open');
};

