﻿var socket;

var appDeepLinkJoin = 'mbnapp://chat/room?type=join&link=';

function getMobileOperatingSystem() {
    var userAgent = navigator.userAgent || navigator.vendor || window.opera;

    // Windows Phone must come first because its UA also contains "Android"
    if (/windows phone/i.test(userAgent)) {
        return "Windows Phone";
    }

    if (/android/i.test(userAgent)) {
        return "Android";
    }

    // iOS detection from: http://stackoverflow.com/a/9039885/177710
    if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
        return "iOS";
    }

    return "unknown";
}
function connectSocket(info, joinLink) {
    var OS = getMobileOperatingSystem();
    

    var roomType, roomId;
    var userId = 0, mobile;
    if (info) {
        userId = info.userId;
        mobile = info.mobile;
    }

    if (OS == 'Android' || OS == 'iOS') {
        console.log('is mobile');
        $('#iosStore').attr('content', 'app-id=994117263, app-argument:' + appDeepLinkJoin + joinLink);
        $('#btOpenApp').attr('href', appDeepLinkJoin + joinLink);                  
        $('#downloadAppWrap').removeAttr('hidden');
        $('#btJoinRoom').attr('hidden', true);
        if (OS == 'iOS') {
            $('#downloadWrap').attr('hidden', true);
        }
    }


    socket = io.connect(socketUrl, {
        query: 'userId=' + userId + '&phone=' + mobile + '&os=web'
    });

    socket.on('connectSuccess', function (res) {
        console.log(res);
        socket.emit('getRoomFromJoinLink', {
            joinLink: joinLink
        }, function (result) {
            console.log(result);
            if (result.errorCode == 0) {
                var roomInfo = result.data;
                roomId = roomInfo._id;
                roomType = roomInfo.type;
                if (roomType == 'custom') {
                    $('#btJoinRoom').html('Join group');
                }
                $('#groupAvatar').prop('src', roomInfo.roomAvatar);
                $('#groupName').html(roomInfo.roomName);
                $('#groupInfo').html(roomInfo.members.length + ' thành viên');
                $('#groupDescription').html(roomInfo.description);
                if (OS == 'Android' || OS == 'iOS') {
                    setTimeout(function () {
                        window.location.href = appDeepLinkJoin + joinLink;
                    }, 200);
                }
            }
        });
    });

    $('#btJoinRoom').click(function () {
        if (info) {
            if (roomType == 'channel') {
                socket.emit('joinChannel', { roomId: roomId }, function (res) {
                    console.log(res);
                    if (res.errorCode == 0 || res.errorCode == 101) {
                        window.location.href = '/?/' + roomId;
                    }
                    else {
                        showNotify('Cảnh báo', res.error);
                    }
                });
            }
            else if (roomType == 'custom') {
                socket.emit('joinRoom', { roomId: roomId }, function (res) {
                    if (res.errorCode == 0 || res.errorCode == 101) {
                        window.location.href = '/?/' + roomId;
                    }
                    else {
                        showNotify('Cảnh báo', res.error);
                    }
                });
            }
        }
        else {
            //show đăng nhập
            $('.loginLayer').removeClass('d-none');
        }
    });
    
}