﻿var maxLimit = 10; //Megabyte
var maxThumbWidth = 300;
var maxImageWidth = 1920;
var messageWrapperWidth = 300;


var CLIPBOARD;
$(document).ready(function () {
    CLIPBOARD = new CLIPBOARD_CLASS("canvasImage", "pasteImageToChatBox", true);
});
//socket varible
var socket;
var currentUserId;
var currentUserInfo;
var channelAdministratorRoomId;
var unreadChatRoom;
var currentRoomMembers = [];
var searchItemCount = 20;
var creatingChatRoomId = '';
var currentInviteRoomInfo;
var OneSignal = window.OneSignal || [];
var roomPermissions = [];
var roomInfos = [];
var $select2Search;
var $select2InviteSearch;

OneSignal.push(["init", {
    appId: "9b80d065-51a1-49d9-aba7-99c2cef36734",
    autoRegister: true,
    persistNotification: false,
    notificationClickHandlerMatch: 'origin',

    notifyButton: {
        enable: true,
        displayPredicate: function () {
            return OneSignal.isPushNotificationsEnabled()
                .then(function (isPushEnabled) {
                    /* The user is subscribed, so we want to return "false" to hide the notify button */
                    return !isPushEnabled;
                });
        },
        colors: { // Customize the colors of the main button and dialog popup button
            'circle.background': 'orange',
            'circle.foreground': 'white',
            'badge.background': 'orange',
            'badge.foreground': 'white',
            'badge.bordercolor': 'white',
            'pulse.color': 'white',
            'dialog.button.background.hovering': 'rgb(77, 101, 113)',
            'dialog.button.background.active': 'rgb(70, 92, 103)',
            'dialog.button.background': 'rgb(84,110,123)',
            'dialog.button.foreground': 'white'
        },
        text: {
            'tip.state.unsubscribed': 'Đăng ký nhận thông báo',
            'tip.state.subscribed': "Bạn đã đăng ký nhận thông báo",
            'tip.state.blocked': "Bạn đã chặn thông báo",
            'message.prenotify': 'Bấm để đăng ký nhận thông báo',
            'message.action.subscribed': "Cám ơn bạn đã đăng ký!",
            'message.action.resubscribed': "Bạn đã đăng ký nhận thông báo",
            'message.action.unsubscribed': "Bạn sẽ không nhận được thông báo nữa",
            'dialog.main.title': 'Đang nhận thông báo',
            'dialog.main.button.subscribe': 'Đăng ký',
            'dialog.main.button.unsubscribe': 'Không nhận thông báo',
            'dialog.blocked.title': 'Để bỏ chặn thông báo',
            'dialog.blocked.message': "Làm theo chỉ dẫn sau để đăng ký nhận thông báo:"
        }
    },
    //promptOptions: {
    //    /* These prompt options values configure both the HTTP prompt and the HTTP popup. */
    //    /* actionMessage limited to 90 characters */
    //    actionMessage: "Để nhận được thông báo từ ChatNhanh, bạn vui lòng xác nhận",
    //    /* acceptButtonText limited to 15 characters */
    //    acceptButtonText: "Xác nhận",
    //    /* cancelButtonText limited to 15 characters */
    //    cancelButtonText: " &nbsp;"
    //},
    safari_web_id: 'web.onesignal.auto.012c2ba4-f65b-47d9-b245-c87f55979016'
}]);

function getOneSignalId() {
    OneSignal.push(function () {
        OneSignal.getUserId(function (osId) {
            if (osId != null) {

                OneSignal.isPushNotificationsEnabled().then(function (isPushEnabled) {
                    if (!isPushEnabled) {
                    }

                    Cookies.set('OneID', osId, { expires: 365 });

                    socket.emit('updateOneSignalUserId', {
                        oneSignalUserId: osId
                    }, function (result) {
                        //console.log('updateOneSignalUserId', result);
                    });
                });
            }
            //console.log("OneSignal User ID:", osId);
            // (Output) OneSignal User ID: 270a35cd-4dda-4b3f-b04e-41d7463a2316
        });
    });
}

function logout() {
    var oneSignalUserId = Cookies.get('OneID');
    //console.log(oneSignalUserId);
    if (oneSignalUserId) {
        socket.emit('removeOneSignalUserId', { oneSignalUserId: oneSignalUserId }, function (res) {
            Cookies.remove('OneID');
            window.location.href = 'https://muabannhanh.com/thanh-vien/thoat?redirectUrl=' + encodeURI(window.location.protocol + '//' + window.location.host);
        });
    }
    else {
        window.location.href = 'https://muabannhanh.com/thanh-vien/thoat?redirectUrl=' + encodeURI(window.location.protocol + '//' + window.location.host);
    }
}

$(window).on("popstate", function (e) {
    //console.log('pop');
    if (currentUserId) {
        //console.log('x');
        loadRoomFromUrl();
    }
});

function detectChromeAndInsertCss() {
    var isWebkit = 'WebkitAppearance' in document.documentElement.style;
    if (isWebkit) {
        var head = document.head, link = document.createElement("link");

        link.type = "text/css";
        link.rel = "stylesheet";
        link.href = '/stylesheets/chrome.css';

        head.appendChild(link);
    }
}

$(function () {
    detectChromeAndInsertCss();
    $('#divChatBoxList').on('click', '.btExtentTab', function (e) {
        e.preventDefault();
        var $parentPane;
        if ($(this).hasClass('selected')) {
            $parentPane = $(this).closest('.chat-pane');
            $parentPane.find($(this).attr('href')).addClass('hide');
            $(this).removeClass('selected');
            $parentPane.find('.chat-added-info').addClass('hide');
        }
        else {
            $parentPane = $(this).closest('.chat-pane');
            $parentPane.find('.chat-added-info').removeClass('hide');
            $parentPane.find('.extend-pane').addClass('hide');
            $parentPane.find('.btExtentTab').removeClass('selected');
            $parentPane.find($(this).attr('href')).removeClass('hide');
            $(this).addClass('selected');
        }
    });
});

function loadAccountInfo() {
    var avatar = currentUserInfo.avatar ? currentUserInfo.avatar : '"/images/noavatar.jpg"';
    console.log(avatar);
    $('#accountAvatar').css('background-image', 'url(' + avatar + ')');
    $('.accountName > a').html(currentUserInfo.name);
    $('.accountName > a').attr('href', currentUserInfo.url);
    $('.accountOtherInfo').html(currentUserInfo.phone);

}
var isSafari = navigator.vendor == "Apple Computer, Inc.";
if (!isSafari) {
    var favicon = new Favico({
        animation: 'slide'
    });
}

//sự kiện các button trên thanh topbar leftmenu
$(document).ready(function () {
    $('#createChatModal').on('modal.closeWithoutChange', function (e) {
        creatingChatRoomId = '';
    });

    //$('.btAccountMenu').click(function () {
    //    $(this).toggleClass('open');
    //    $('.accountInfoDropdown').toggle();
    //});
    $('.menuButton').click(function () {
        $(this).toggleClass('open');
        $('.menuMain').toggle();
    });

    $('.btShowContact').click(function () {
        $('#conctactListModal').showModal();
    });

    $('.btSettings').click(function () {
        $('#settingModal').showModal();
    });

    $('.btCreateChat').click(function () {
        $('#createChatModal').showModal();
    });

    $('#btStartChat').click(function () {
        
        var memberRaws = $('#tbMemberInput').select2('data');
        var members = [];
        for (var i = 0; i < memberRaws.length; i++) {
            var member = memberRaws[i];
            members.push({
                userId: parseInt(member.id),
                name: member.name,
                phone: member.phone,
                email: member.email,
                avatar: member.avatar_url,
                url: 'https://muabannhanh.com/' + member.phone
            });
        }

        if (members.length > 0) {
            createCustomChat(members);
        }
        else {
            showNotifyError("Cần chọn người để chat");
        }
    });

    $select2Search = $('#tbMemberInput').select2({
        ajax: {
            url: 'https://api.muabannhanh.com/v2/user/search',
            dataType: 'json',
            data: function (params) {
                var query = {
                    q: params.term,
                    type: 'public',
                    limit: 20,
                    page: 1,
                    session_token: 'cb2663ce82a9f4ba448ba435091e27bb'
                };
                //console.log(query);
                // Query parameters will be ?search=[term]&type=public
                return query;
            },
            processResults: function (data) {
                //console.log(data);
                var items = [];
                for (var i = 0; i < data.result.length; i++) {
                    data.result[i].text = data.result[i].name + ' (' + data.result[i].phone + ')';
                    items.push(data.result[i]);
                }
                // Tranforms the top-level key of the response object from 'items' to 'results'                
                return {
                    results: items
                };
            },
            delay: 250
        },
        templateResult: generateSelect2Result,
        templateSelection: generateSelect2ResultSelection
    });
    function generateSelect2Result(user) {
        //console.log(user);
        if (!user.id) {
            return user.text;
        }
        var $user = $('<img class="avatar-chat-search" src="' + user.avatar_url + '">' +
            '<b>' + user.name + '</b>' + '<span class="search-member-info"><i class="fa fa-phone mr5"></i>' + user.phone + (user.email ? '<i class="fa fa-envelope mr5" style="margin-left:20px;"></i>' + user.email : '') + '</span>');
        return $user;
    }
    function generateSelect2ResultSelection(user) {
        //console.log(user);
        if (!user.id) {
            return user.text;
        }
        var $user = $('<img class="avatar-chat-search" style="margin-left:5px" src="' + user.avatar_url + '">' +
            '<b>' + user.name + '</b>');
        return $user;
    }

    $select2InviteSearch = $('#tbMemberInviteInput').select2({
        ajax: {
            url: 'https://api.muabannhanh.com/v2/user/search',
            dataType: 'json',
            data: function (params) {
                var query = {
                    q: params.term,
                    type: 'public',
                    limit: 20,
                    page: 1,
                    session_token: 'cb2663ce82a9f4ba448ba435091e27bb'
                };
                //console.log(query);
                // Query parameters will be ?search=[term]&type=public
                return query;
            },
            processResults: function (data) {
                //console.log(data);
                var items = [];
                for (var i = 0; i < data.result.length; i++) {
                    data.result[i].text = data.result[i].name + ' (' + data.result[i].phone + ')';
                    items.push(data.result[i]);
                }
                // Tranforms the top-level key of the response object from 'items' to 'results'                
                return {
                    results: items
                };
            },
            delay: 250
        },
        templateResult: generateSelect2Result,
        templateSelection: generateSelect2ResultSelection
    });

    $('#btStartInvite').click(function (e) {
        //console.log($('#tbMemberInviteInput').select2('data'));
        var memberRaws = $('#tbMemberInviteInput').select2('data');
        var members = [];
        for (var i = 0; i < memberRaws.length; i++) {
            var member = memberRaws[i];
            members.push({
                userId: parseInt(member.id),
                name: member.name,
                phone: member.phone,
                email: member.email,
                avatar: member.avatar_url,
                url: 'https://muabannhanh.com/' + member.phone
            });
        }
        if (members.length > 0) {
            inviteMembersToChat(currentInviteRoomInfo._id, members);
        }
        else {
            showNotifyError("Cần chọn người để mời");
        }
    });
});


function displayUnreadMessageCountOnTab() {
    var unreadCount = $('#divRecentChat > li[data-view=false]').length;
    //console.log(unreadCount);
    if (!isSafari) {
        favicon.badge(unreadCount > 99 ? 99 : unreadCount);
    }
    if (unreadCount == 0) {
        unBlinkPageTitle('ChatNhanh');
    }
    else {
        blinkPageTitle('(' + unreadCount + ') ChatNhanh', 1000);
    }
}
function createRoom(info) {
    switch (info.type) {
        case 'private':
            {
                socket.emit('getPrivateRoom',
                    {
                        userId: info.userId
                    }, function (result) {
                        //console.log(result);
                        showRoomAfterCreate(result);
                        if (result.errorCode == 0) {
                            var roomId = result.data._id;
                            if (info.createTextMessage) {
                                (function (_roomId) {
                                    var checkRoomCreatedHandle = setInterval(function () {
                                        var $chatHistory = getChatHistoryWrapper(_roomId);
                                        if ($chatHistory.length > 0) {
                                            clearInterval(checkRoomCreatedHandle);
                                            parseRoomLogBeforeSend($chatHistory, currentUserId, _roomId, info.text, true);
                                        }
                                    }, 500);
                                })(roomId);
                            }
                        }
                    });
                break;
            }
        case 'item':
            {
                socket.emit('getItemRoom',
                    {
                        userIdOwner: info.ownerId,
                        userIdGuest: currentUserId,
                        itemId: info.itemId,
                        itemName: info.itemName,
                        itemImage: info.itemImage,
                        itemLink: info.itemLink,
                        itemPrice: info.itemPrice
                    }, function (result) {
                        //console.log(result);
                        showRoomAfterCreate(result);
                    });
                break;
            }
        case 'page':
            {
                socket.emit('getPageRoom',
                    {
                        userIdGuest: currentUserId,
                        pageId: info.pageId,
                        pageName: info.pageName,
                        pageLink: info.pageLink,
                        pageImage: info.pageImage
                    }, function (result) {
                        //console.log(result);
                        showRoomAfterCreate(result);
                        if (result.errorCode == 0) {
                            var roomId = result.data._id;
                            if (info.createItemMessage) {
                                (function (_roomId) {
                                    var checkRoomCreatedHandle = setInterval(function () {
                                        var $chatHistory = getChatHistoryWrapper(_roomId);
                                        if ($chatHistory.length > 0) {
                                            clearInterval(checkRoomCreatedHandle);
                                            parseItemLogBeforeSend($chatHistory, currentUserId, _roomId, info.itemId, info.itemName, info.itemImage, info.itemLink, info.itemPrice, true);
                                        }
                                    }, 500);
                                })(roomId);
                            }
                        }
                    });
                break;
            }
    }
}

function showRoomAfterCreate(result) {
    //console.log('data after create room');
    //console.log(result);
    if (result.errorCode == 0) {
        var item = result.data;
        var $recentItem = $('#divRecentChat li[data-id=' + item._id + ']');
        if ($recentItem.length == 0) {
            appendRecentChatRoomWhenCreateRoom(item);
            $recentItem = $('#divRecentChat li[data-id=' + item._id + ']');

            //$recentItem.click();
        }
        else {
            $recentItem.prependTo($('#divRecentChat'));
        }
        setChatRoomSelected($recentItem, item, true);
    }
}
function appendRecentChatRoomWhenCreateRoom(roomInfo) {
    //console.log(roomInfo);
    var lastLogAuthor = null;
    if (roomInfo.lastLog) {
        var lastLogAuthorId = roomInfo.lastLog.userIdAuthor;
        lastLogAuthor = roomInfo.members.find(function (val) {
            return val.userId == lastLogAuthorId;
        }).userInfo;
    }
    var item = {
        _id: roomInfo._id,
        type: roomInfo.type,
        roomName: decodeURITryCatch(roomInfo.roomName),
        roomAvatar: decodeURITryCatch(roomInfo.roomAvatar),
        members: roomInfo.members,
        lastLog: roomInfo.lastLog,
        lastLogAuthor: lastLogAuthor
    };
    generateRecentChatRoomItem($('#divRecentChat'), item, true);
}
//socket
function connectSocket(token, _id/*, createRoomInfo*/) {
    $(document).ready(function () {
        var deviceId = Cookies.get('OneID');
        socket = io.connect(socketUrl,
            {
                //query: 'token=' + token + '&os=web&deviceId=' + deviceId,
                query: 'userId=' + '165199' + '&phone=' + '0943282606' + '&os=web&deviceId=' + '473baa11-3dda-48af-ab2d-46908964201f',
                transports: ["websocket"],
                upgrade: false
            });
        socket.on('reconnect_attempt', (e) => {
            console.warn('reconnect_attempt', e);
        });
        socket.on('reconnect_failed', () => {
            console.warn('reconnect_failed');
        });
        socket.on('reconnect_error', (e) => {
            console.warn('reconnect_error', e);
        });
        socket.on("error", function (e) {
            console.warn('error', e);
            if (e.code == 'invalid_token' || e.type == 'UnauthorizedError') {
                window.location.href = '/login';
            }
            
        });
        // currentUserId = _id;
        currentUserId = 165199;
        socket.on('connectSuccess', function (result) {
            console.log('connected');
            //console.log(result);
            currentUserInfo = result.data;

            socket.emit('searchContact', {
                pageIndex: 0,
                itemPerPage: 5,
                keyword: 'a'
            },
                (res) => {
                    //console.log(res);
                });

            //socket.emit('sendPlan',
            //    {
            //        roomId: '5ba46b9d577b4816ac07e071',
            //        title: 'test plan',
            //        timeStamp: moment().add(2, 'm').startOf('minute').unix(),
            //        duration: 1440,
            //        place: {
            //            lat: '10.7859636',
            //            lng: '106.6987031',
            //            address:''
            //        },
            //        itemGUID: generateGUID()
            //    },
            //    (res) => {
            //        console.log(res);
            //    });

            //socket.emit('checkPhoneNumber', {
            //    token: '49a84adf0470f626957ee3948a2ed854',
            //    contacts: [
            //        { phone: '0943282606', name: 'Hưng' },
            //        { phone: '0906679001', name: 'Ngọc' }
            //    ]
            //}, function (resX) {
            //    console.log('check res');
            //    console.log(resX);
            //});

            //console.log(currentUserInfo);.
            loadAccountInfo();
            getOneSignalId();
            getUnreadRoomCount(function (res) {
                if (res.data && res.data.count) {
                    var unreadCount = res.data.count;
                    if (!isSafari) {
                        favicon.badge(unreadCount > 99 ? 99 : unreadCount);
                    }
                    blinkPageTitle('(' + res.data.count + ') ChatNhanh', 1000);
                }
            });
            loadRecentPinChatRoom($('ul#divRecentChat'));
            loadRecentChatRoom($('ul#divRecentChat'), null, '', true);

            loadContact();

            //if (createRoomInfo.type != '') {
            //    createRoom(createRoomInfo);
            //}
            //socket.emit('getItemRoomNotReply', { fromDate: new Date('2018-01-01'), toDate: new Date() }, function (result) {
            //    console.log(result);
            //});
        });

        socket.on('currentOnlineUser', function (result) {
            //console.log(result);
        });

        socket.on('connectFail', function () {
            console.error('connected fail');
        });
        socket.on('userConnected', function (result) {
            if (result.errorCode == 0) {
                //console.log('userConnected: ' + result.data.userId);
                var userId = result.data.userId;
                $('#divChatBoxList .memberPane li.member[data-id=' + userId + '] .user-status').removeClass('off');
                $('#divChatBoxList .memberPane li.member[data-id=' + userId + '] .user-status').addClass('on');
            }
        });

        socket.on('userDisconnected', function (result) {
            if (result.errorCode == 0) {
                //console.log('userDisconnected: ' + result.data.userId);
                var userId = result.data.userId;
                $('#divChatBoxList .memberPane li.member[data-id=' + userId + '] .user-status').removeClass('on');
                $('#divChatBoxList .memberPane li.member[data-id=' + userId + '] .user-status').addClass('off');
            }
        });

        //socket event ON
        socket.on('newMessage', function (result) {
            //console.log('newMessage');
            if (result.errorCode == 0) {
                var log = result.data;
                //console.log(log);
                updateRecentChatRoom(log);
                //nếu là room mới tạo thì focus tới
                if (creatingChatRoomId == log.roomId) {
                    $('#divRecentChat li[data-id=' + log.roomId + ']').click();
                    creatingChatRoomId = '';
                }
                var $chatHistory = getChatHistoryWrapper(log.roomId);
                if ($chatHistory.length > 0) {
                    if (log.userIdAuthor != currentUserId) {
                        //currentRoomUnreadMessage += 1;
                        //updateUnreadMessage();
                    }
                    generateLogFull($chatHistory, log);
                    scrollToBottom($chatHistory);
                    generateExtentItem(log.roomId, log, log.type, true);
                }
                if (log.type == messageType.action) {
                    generateAdditionAction(log);
                }
                displayUnreadMessageCountOnTab();
            }
        });

        socket.on('removeMessage', function (result) {
            var log = result.data;
            var $chatHistory = getChatHistoryWrapper(log.roomId);
            //console.log('removeMessage');
            updateRecentChatRoom(log, true);
            updateLog($chatHistory, result);
            //console.log(log);
            removeExtentItem(log.roomId, log);
            displayUnreadMessageCountOnTab();
        });

        socket.on('updateMessage', function (result) {
            var log = result.data;
            var $chatHistory = getChatHistoryWrapper(log.roomId);
            //console.log('updateMessage');
            updateLog($chatHistory, result);
            displayUnreadMessageCountOnTab();
        });

        socket.on('logIsViewed', function (result) {
            if (result.errorCode == 0) {
                var log = result.data;
                var roomId = log.roomId;
                var $chatHistory = getChatHistoryWrapper(roomId);
                updateLog($chatHistory, result);
                var currentIsViewed = log.views.find(function (x) { return x.userId == currentUserId && x.isViewed == true; });
                //console.log(currentIsViewed);
                if (currentIsViewed) {
                    var $recentItem = getRecentChatItem(roomId);
                    $recentItem.attr('data-view', true);
                }
                displayUnreadMessageCountOnTab();
            }
        });

        socket.on('addPinItem', function (result) {
            if (result.errorCode == 0) {
                var log = result.data;
                var roomId = log.roomId;
                //console.log(result);
                generateExtentItem(roomId, log, 'pin', true, '');
                displayExtentSearchResultLabel(getExtentPaneCotentWrapper(roomId, 'pin'), '');
                updateLogPinLayout(roomId, log, true);
            }
        });

        socket.on('removePinItem', function (result) {
            if (result.errorCode == 0) {
                var log = result.data;
                var roomId = log.roomId;
                removePinItem(roomId, log);
                displayExtentSearchResultLabel(getExtentPaneCotentWrapper(roomId, 'pin'), '');
                updateLogPinLayout(roomId, log, false);
            }
        });

        socket.on('changeChannelInfo', function (result) {
            if (result.errorCode == 0) {
                var roomInfo = result.data;
                changeGroupChannelInfo(roomInfo);
            }
        });

        socket.on('changeGroupInfo', function (result) {
            if (result.errorCode == 0) {
                var roomInfo = result.data;
                changeGroupChannelInfo(roomInfo);
            }
        });

        socket.on('permissionChange', function (result) {
            if (result.errorCode == 0) {
                var data = result.data;
                var roomId = data.roomId;
                var isAdmin = data.isAdmin;
                var permissions = data.permissions;
                var roomInfo = data.roomInfo;
                updateRoomPermissionChange(roomId, roomInfo, isAdmin, permissions);
            }
        });
    });
}

function updateRoomPermissionChange(roomId, roomInfo, isAdmin, permissions) {
    var $chatBox = getChatBox(roomId);
    switch (roomInfo.type) {
        case 'channel': {
            if (isAdmin) {
                $chatBox.find('.memberExtent').removeClass('hide');
                if (permissions.editInfo) {
                    $chatBox.find('.configEditInfo').removeClass('hide');
                    $chatBox.find('.box-avatar').addClass('editable-img');
                    $chatBox.find('.box-name').addClass('editable');
                }
                else {
                    $chatBox.find('.configEditInfo').addClass('hide');
                    $chatBox.find('.box-avatar').removeClass('editable-img');
                    $chatBox.find('.box-name').removeClass('editable');
                }

                if (permissions.postMessage) {
                    $chatBox.find('.text-input').removeClass('hide');
                    $chatBox.find('.other-input').removeClass('hide');
                    $chatBox.find('.chat-input-disabled').addClass('hide');
                }
                else {
                    $chatBox.find('.text-input').addClass('hide');
                    $chatBox.find('.other-input').addClass('hide');
                    $chatBox.find('.chat-input-disabled').removeClass('hide');
                }

                if (permissions.editMessageOfOthers) {
                }
                else {
                }

                if (permissions.deleteMessageOfOthers) {
                }
                else {
                }

                if (permissions.addUsers) {
                    $chatBox.find('.configAddMember').removeClass('hide');
                    $chatBox.find('.configBanMember').removeClass('hide');
                }
                else {
                    $chatBox.find('.configAddMember').addClass('hide');
                    $chatBox.find('.configBanMember').addClass('hide');
                }

                if (permissions.addNewAdmins) {
                    $chatBox.find('.configAddNewAdmins').removeClass('hide');
                }
                else {
                    $chatBox.find('.configAddNewAdmins').addClass('hide');
                }
            }
            else {
                $chatBox.find('.memberExtent').addClass('hide');
                $chatBox.find('.chat-input').addClass('hide');
                $chatBox.find('.other-input').addClass('hide');
                $chatBox.find('.configEditInfo').addClass('hide');
                $chatBox.find('.configAddMember').addClass('hide');
                $chatBox.find('.configBanMember').addClass('hide');
                $chatBox.find('.configAddNewAdmins').addClass('hide');
                $chatBox.find('.box-avatar').removeClass('editable-img');
                $chatBox.find('.box-name').removeClass('editable');
            }
            break;
        }
        case 'custom': {
            break;
        }
    }
}

function changeGroupChannelInfo(roomInfo) {
    var roomId = roomInfo._id;
    var $chatBox = getChatBox(roomId);
    $chatBox.find('.box-info .box-name').html(roomInfo.roomName);
    if (!roomInfo.isPrivate && roomInfo.joinLink && roomInfo.joinLink.trim()) {
        $chatBox.find('.config-line .joinLink').closest('.config-line').removeClass('hide');
    }
    else {
        $chatBox.find('.config-line .joinLink').closest('.config-line').addClass('hide');
    }
    $chatBox.find('.config-line .joinLink').prop('href', roomInfo.joinLink);
    $chatBox.find('.config-line .joinLink').html(roomInfo.joinLink);

    var $des = $chatBox.find('.config-line .description');
    $des.html(replaceEmoji(roomInfo.description));
    if (roomInfo.description && roomInfo.description.trim()) {
        $des.closest('.config-line').removeClass('hide');
    }
    else {
        $des.closest('.config-line').addClass('hide');
    }
    var $recentItem = getRecentChatItem(roomId);
    $recentItem.find('.chat-room-name').prop('title', roomInfo.roomName);
    $recentItem.find('.chat-room-name').html(roomInfo.roomName);
    if (roomInfo.type == 'custom') {
        if (roomInfo.onlyAdminAddUser) {
            var currentMember = roomInfo.members.find(function (x) {
                return x.userId == currentUserId;
            });

            if (!currentMember.isOwner && !(currentMember.isAdmin && currentMember.permissions && currentMember.permissions.addUsers)) {
                $chatBox.find('.add-member').addClass('hide');
                $chatBox.find('.configAddMember').addClass('hide');
            }
        }
        else {
            $chatBox.find('.add-member').removeClass('hide');
            $chatBox.find('.configAddMember').removeClass('hide');
        }
    }
}

function updateLogPinLayout(roomId, log, isPin) {
    var $chatHistory = getChatHistoryWrapper(roomId);
    if ($chatHistory.length > 0) {
        var $item = $chatHistory.find('li[data-id=' + log._id + ']');
        if ($item.length > 0) {
            if (isPin) {
                $item.addClass('isPinned');
                $item.find('.btPin').remove();
                var $btUnpin = $("<a id='btUnPin" + log._id + "' class='btUnPin pointer'><span class='fa fa-thumb-tack'></span></a>");
                $btUnpin.appendTo($item.find('.message-pin-holder'));
                $btUnpin.click(function (e) {
                    if (!$(this).prop('disabled')) {
                        unpinMessage(log._id, roomId);
                    }
                });
            }
            else {
                $item.removeClass('isPinned');
                $item.find('.btUnPin').remove();
                if ($item.find('.btPin').length == 0) {
                    var $btPin = $("<a class='btn btn- xs btPin' id='btPin pointer" + log._id + "'><span class='fa fa-thumb-tack'></span></a>");
                    $btPin.appendTo($item.find('.btn-group'));
                    $btPin.click(function (e) {
                        if (!$(this).prop('disabled')) {
                            pinMessage(log._id, log.roomId);
                        }
                    });
                }
            }
        }
    }
}
function removePinItem(roomId, log) {
    var $pinPane = getExtentPaneCotentWrapper(roomId, 'pin');
    $pinPane.find('li[data-id=' + log._id + ']').remove();
}

function getUnreadRoomCount(callback) {
    socket.emit('getUnreadRoomCount', {}, function (result) {
        //console.log(result);
        if (result.errorCode == 0) {
            unreadChatRoom = result.data;
        }
        callback(result);
    });
}

function activeChatRoom($chatBox) {
    $('.chat-pane').addClass('hide');
    $chatBox.removeClass('hide');
}

function showChatRoom(info, callback, needGet) {
    channelAdministratorRoomId = info._id;
    var $chatBox = $('#divChatBoxList').find('.chat-pane[data-id=' + info._id + ']');
    if ($chatBox.length > 0) {
        activeChatRoom($chatBox);
        callback();
    }
    else {
        switch (info.type) {
            case 'item': {
                if (!info.item) {
                    needGet = true;
                }
                break;
            }
            case 'page': {
                if (!info.page) {
                    needGet = true;
                }
                break;
            }
        }
        if (info.roomName && !needGet) { //nếu có roomName 
            generateChatRoom(info, true); //tạo thông tin trước luôn, ko cần chờ socket đẩy về, cần thì update lại sao                    
        }
        socket.emit('getChatRoom', { roomId: info._id }, function (result) {
            //console.log(result);
            if (result.errorCode == 0) {
                var roomInfo = result.data;
                //console.log('getChatRoom');
                //console.log(roomInfo);
                var roomId = result.data._id;
                roomInfos[roomId] = roomInfo;

                if (!info.roomName || needGet) {
                    generateChatRoom(roomInfo);
                }
                else {
                    var $chatHistoryX = getChatHistoryWrapper(roomId);
                    bindChatRoomAction(roomId, $chatHistoryX, roomInfo.members);
                }
                currentRoomMembers[roomId] = result.data.members;
                var lastLogId = null;
                var $chatHistory = getChatHistoryWrapper(roomId);
                getRoomLog($chatHistory, roomId, lastLogId, true);
                getRoomExentList(roomInfo);

            }
            else {
                showNoPermissionBox();
            }
            callback();
        });
    }
}
function showNoPermissionBox() {
    $('.chat-pane').addClass('hide');
    $('.chat-pane.no-permission').removeClass('hide');
}
function generateExtentItem(roomId, log, type, isPrepend, keyword) {

    switch (type) {
        case messageType.file:
        case messageType.image:
        case messageType.video: {
            generateFile(getExtentPaneCotentWrapper(roomId, log.type), log, isPrepend, keyword);
            break;
        }
        case messageType.link: {
            generateLink(getExtentPaneCotentWrapper(roomId, log.type), log, isPrepend, keyword);
            break;
        }
        case 'search': {
            generateSearchResultItem(getExtentPaneCotentWrapper(roomId, 'search'), log, isPrepend, keyword);
            break;
        }
        case 'pin': {
            generateSearchResultItem(getExtentPaneCotentWrapper(roomId, 'pin'), log, isPrepend, keyword, true);
            break;
        }
    }
}

function removeExtentItem(roomId, log) {
    removeFileOrLinkItem(getExtentPaneCotentWrapper(roomId, messageType.file), log); //tìm trong file
    removeFileOrLinkItem(getExtentPaneCotentWrapper(roomId, messageType.link), log); //tìm trong link    
}

function removeFileOrLinkItem($chatScrollPane, log) {
    $chatScrollPane.find('li[data-id=' + log._id + ']').remove();
}

function generateFile($chatFileScrollPane, log, isPrepend, keyword) {
    var html = '';
    html += '<li data-id="' + log._id + '"><a href="' + log.content.link + '" target="_blank">';
    if (log.type == messageType.image) {
        html += '<img class="preview" src="' + log.content.thumbLink + '"/>';
    }
    else {
        html += '<i class="fa fa-file-text fileType"></i>';
    }
    html += '<div>';
    html += '<div class="item-name">' + log.content.name + '</div>';
    html += '<div class="author-info">' + log.authorInfo.name + ' <span>' + formatDateTime(log.createDate) + '</span></div>';
    html += '</div>';
    html += '</a></li>';

    var $html = $(html);
    if ($html.find('.item-name').length > 0 && keyword) {
        $html.find('.item-name').mark(keyword);
    }
    if (isPrepend) {
        $chatFileScrollPane.find('ul.item-list').prepend($html);
    }
    else {
        $chatFileScrollPane.find('ul.item-list').append($html);
    }
}
function generateLink($chatLinkScrollPane, log, isPrepend, keyword) {
    var html = '';
    html += '<li data-id="' + log._id + '"><a href="' + log.content.link + '" target="_blank">';
    html += '<i class="fa fa-link"></i>';
    html += '<div>';
    html += '<div class="item-name">' + log.content.link + '</div>';
    html += '<div class="author-info">' + log.authorInfo.name + ' <span>' + formatDateTime(log.createDate) + '</span></div>';
    html += '</div>';
    html += '</a></li>';
    var $html = $(html);
    if ($html.find('.item-name').length > 0 && keyword) {
        $html.find('.item-name').mark(keyword);
    }
    if (isPrepend) {
        $chatLinkScrollPane.find('ul.item-list').prepend($html);
    }
    else {
        $chatLinkScrollPane.find('ul.item-list').append($html);
    }
}

function scrollToAndHighlight($chatHistory, $chatItem) {
    $chatHistory.animate({
        scrollTop: ($chatItem.offset().top - $chatHistory.offset().top + $chatHistory.scrollTop() - ($chatHistory.innerHeight() / 2) + ($chatItem.outerHeight() / 2))
    }, 500);

    $chatItem.addClass('highlight');
    setTimeout(function () {
        $chatItem.removeClass('highlight');
    }, 3000);
}
function scrollToItem($chatHistory, $chatItem) {
    $chatHistory.scrollTop($chatItem.offset().top - $chatHistory.offset().top + $chatHistory.scrollTop() - ($chatHistory.innerHeight() / 2) + ($chatItem.outerHeight() / 2));
}
function loadMoreLogs($searchResult, roomId, isPrev, noMore) {
    if (noMore) {
        if (isPrev) {
            $searchResult.find('ul > li.chat-item:first-child').data('end-scroll', true);
        }
        else {
            $searchResult.find('ul > li.chat-item:last-child').data('end-scroll', true);
        }
    }
    if ($searchResult.hasScrollBar()) {
        //console.log('scroll: ' + $searchResult[0].scrollHeight);
        //console.log('height: ' + $searchResult.outerHeight());
        $searchResult.bind('scroll', function () {
            //if ($(this).innerHeight() < this.scrollHeight) {
            //console.log($(this).scrollTop());
            if ($(this).scrollTop() >= (this.scrollHeight - $searchResult.outerHeight() - 15) &&
                !$searchResult.find('ul li.chat-item:last-child').data('end-scroll')) {
                //console.log('bottom');
                $(this).unbind('scroll');
                //console.log($searchResult.find('ul li.chat-item:first-child').data('id'));
                loadRoomNextLogs($searchResult, roomId, $searchResult.find('ul li.chat-item:last-child').data('id'));
            }
            else if ($(this).scrollTop() <= 15 && !$searchResult.find('ul li.chat-item:first-child').data('end-scroll')) {
                //console.log('top');
                $(this).unbind('scroll');
                //console.log($searchResult.find('ul li.chat-item:first-child').data('id'));
                loadRoomPrevLogs($searchResult, roomId, $searchResult.find('ul li.chat-item:first-child').data('id'));
            }
        });
    }
}
function loadRoomPrevLogs($searchResult, roomId, logId) {
    //console.log('prev');
    //console.log(logId);
    socket.emit('getRoomPreviousLogs', { roomId: roomId, logId: logId, itemCount: searchItemCount }, function (result) {
        if (result.errorCode == 0) {
            var lst = result.data;
            if (lst.length > 0) {
                lst = lst.reverse();
                var startDate = lst[lst.length - 1].createDate;
                $searchResult.find('.search-result-title span.startDate').html(formatDateTime(startDate));
            }

            for (var i = 0; i < lst.length; i++) {
                generateLogFull($searchResult, lst[i], true);
            }

            var $chatItem = $searchResult.find('li[data-id=' + logId + ']');
            if ($chatItem.length > 0) {
                scrollToItem($searchResult, $chatItem);
            }

            if (lst.length > 0) {
                loadMoreLogs($searchResult, roomId, true, false);
            }
            else {
                loadMoreLogs($searchResult, roomId, true, true);
            }
        }
    });
}

function loadRoomNextLogs($searchResult, roomId, logId) {
    //console.log('next');
    //console.log(logId);
    socket.emit('getRoomNextLogs', { roomId: roomId, logId: logId, itemCount: searchItemCount }, function (result) {
        if (result.errorCode == 0) {
            var lst = result.data;
            if (lst.length > 0) {
                var endDate = lst[lst.length - 1].createDate;
                $searchResult.find('.search-result-title span.endDate').html(formatDateTime(endDate));
            }

            var $chatHistory = getChatHistoryWrapper(roomId);
            var isMerge = false;
            //console.log('first his: ' + $chatHistory.find('ul li.chat-item:first-child').data('id'));
            for (var i = 0; i < lst.length && !isMerge; i++) {
                if (lst[i]._id != $chatHistory.find('ul li.chat-item:first-child').data('id')) {
                    generateLogFull($searchResult, lst[i], false);
                }
                else {
                    //console.log('merge');
                    isMerge = true;
                    $searchResult.find('ul li').prependTo($chatHistory.find('ul'));
                    closeSearchResult(roomId, false);
                }
            }

            //var $chatItem = $searchResult.find('li[data-id=' + logId + ']');
            //if ($chatItem.length > 0) {
            //    scrollToAndHighlight($searchResult, $chatItem);
            //}

            if (lst.length > 0) {
                loadMoreLogs($searchResult, roomId);
            }
            else {
                loadMoreLogs($searchResult, roomId, false, true);
            }
        }
    });
}

function loadRoomNearbyLogs($searchResult, log) {
    $searchResult.unbind('scroll');
    socket.emit('getRoomNearbyLogs', { roomId: log.roomId, logId: log._id, itemCount: searchItemCount }, function (result) {
        if (result.errorCode == 0) {
            var lst = result.data;
            if (lst.length > 0) {
                var startDate = lst[0].createDate;
                var endDate = lst[lst.length - 1].createDate;
                $searchResult.find('.search-result-title span.startDate').html(formatDateTime(startDate));
                $searchResult.find('.search-result-title span.endDate').html(formatDateTime(endDate));
            }

            for (var i = 0; i < lst.length; i++) {
                generateLogFull($searchResult, lst[i], false);
            }

            var $chatItem = $searchResult.find('li[data-id=' + log._id + ']');
            if ($chatItem.length > 0) {
                scrollToAndHighlight($searchResult, $chatItem);
            }

            loadMoreLogs($searchResult, log.roomId);
        }
    });
}
function closeSearchResult(roomId, isOpen) {
    var $chatHistory = getChatHistoryWrapper(roomId);
    var $searchResult = getChatSearchResultWrapper(roomId);
    if (isOpen) {
        $chatHistory.addClass('hide');
        $searchResult.removeClass('hide');
    }
    else {
        $chatHistory.removeClass('hide');
        $searchResult.addClass('hide');
    }
}
function generateSearchResultItem($chatScrollPane, log, isPrepend, keyword, isPin) {
    var html = '';
    html += '<li id="btGo' + log._id + '" class="search" data-id="' + log._id + '">';

    //html += '<i  class="btGo fa fa-hand-o-left" data-id="' + log._id + '">&nbsp;go</i>';
    if (isPin) {
        html += '<i id="btUnPin' + log._id + '" class="btPin fa fa-remove" data-id="' + log._id + '"></i>';
    }
    html += '<img src="' + log.authorInfo.avatar + '" class="circle avatar-small"/>';
    html += '<div class="message-wrapper">';
    html += "<div class='message-data' >";
    html += "<span class='message-data-name'>" + log.authorInfo.name + "</span>";
    html += "&nbsp;&nbsp;";
    html += "<span class='message-data-time' >" + formatDateTime(log.createDate) + "</span>";
    html += "</div>";
    html += "<div class='message chat-item'>";
    html += generateMessageFromType(log);
    html += "</div>";
    html += "</div>";
    html += '</li>';
    var $html = $(html);
    $html.find('.message-link-wrapper, .message').mark(keyword);
    if (isPrepend) {
        $chatScrollPane.find('ul.item-list').prepend($html);
    }
    else {
        $chatScrollPane.find('ul.item-list').append($html);
    }

    var timeoutHandle;
    $('#btCloseSearchResult' + log.roomId).click(function () {
        closeSearchResult(log.roomId, false);
    });
    $('#btGo' + log._id).click(function () {
        if (!$(this).prop('disabled')) {
            var $chatHistory = getChatHistoryWrapper(log.roomId);
            var $searchResult = getChatSearchResultWrapper(log.roomId);
            var $chatItem = $chatHistory.find('li[data-id=' + log._id + ']');
            //console.log($chatItem.html());
            if ($chatItem.length > 0) {
                closeSearchResult(log.roomId, false);
                scrollToAndHighlight($chatHistory, $chatItem);
            }
            else {
                closeSearchResult(log.roomId, true);

                if ($searchResult.find('ul li').length == 0) {
                    loadRoomNearbyLogs($searchResult, log);
                }
                else {
                    //tìm xem có log trong đó ko
                    $chatItem = $searchResult.find('li[data-id=' + log._id + ']');

                    if ($chatItem.length > 0) {
                        //nếu có thì scroll đến
                        scrollToAndHighlight($searchResult, $chatItem);
                    }
                    else {
                        //nếu ko thì xoá hết li rồi gọi socket lấy, dùng chung hàm trên
                        $searchResult.find('ul').html('');
                        loadRoomNearbyLogs($searchResult, log);
                    }
                }
            }
        }
    });

    if (isPin) {
        $('#btUnPin' + log._id).click(function () {
            if (!$(this).prop('disabled')) {
                unpinMessage(log._id, log.roomId);
            }
        });
    }
}
function generateRoomConfig(roomInfo) {
    var $chatScrollPane = getExtentPaneCotentWrapper(roomInfo._id, 'config');
    generateRoomNotifyConfig(roomInfo, $chatScrollPane);
}

function generateRoomEditInfoButton(roomId, roomInfo, isShow) {
    var html = '';
    html += '<div class="configEditInfo config-line  ' + (isShow ? '' : 'hide') + '">';
    html += '<a onclick="' + (roomInfo.type == 'channel' ? 'showEditChannelModal' : 'showEditGroupModal') + '(\'' + roomId + '\');" class="action-link" href="javascript:;">';
    html += '<span class="fa fa-info"></span>';
    if (roomInfo.type == 'channel') {
        html += 'Cập nhật thông tin Channel';
    }
    else {
        html += 'Cập nhật thông tin Chat group';
    }
    html += '</a >';
    html += '</div>';
    return html;
}

function generateRoomAddUsersButton(roomId, roomInfo, isShow) {
    var html = '';
    html += '<div class="configAddMember config-line ' + (isShow ? '' : 'hide') + '">';
    html += '<a id="btInfoAddMember' + roomId + '" class="action-link" href="javascript:;"><span class="fa fa-user-plus"></span>Thêm user</a>';
    html += '</div>';
    html += '<div class="configBanMember config-line ' + (isShow ? '' : 'hide') + '">';
    html += '<a onclick="showChannelBanUserModal(\'' + roomId + '\')" class="action-link" href="javascript:;"><span class="fa fa-ban"></span>Ban user</a>';
    html += '</div>';
    return html;
}

function generateRoomAddNewAdminsButton(roomId, roomInfo, isShow) {
    var html = '';
    html += '<div class="configAddNewAdmins config-line ' + (isShow ? '' : 'hide') + '">';
    html += '<a class="action-link" onclick="showChannelAdministratorModal(\'' + roomId + '\', ' + (roomInfo.type == 'channel') + ')" href="javascript:;"><span class="fa fa-star"></span>Administrator</a>';
    html += '</div>';
    return html;
}
function generateRoomNotifyConfig(roomInfo, $chatScrollPane) {
    var roomId = roomInfo._id;
    var currentMember = roomInfo.members.find(function (member) { return member.userId == currentUserId; });
    var isMuted = false;
    if (currentMember) {
        isMuted = currentMember.isMuted;
    }
    var html = '<div style="width:100%">';
    if (roomInfo.type == 'channel') {
        html += '<div class="config-line ' + (!roomInfo.isPrivate && roomInfo.joinLink && roomInfo.joinLink.trim() ? "" : "hide") + '">';
        html += '<div class="sub-heading">Link mời tham gia</div>';
        html += '<p><a class="joinLink link" href="' + roomInfo.joinLink + '">' + roomInfo.joinLink + '</a></p>';
        html += '</div>';


        html += '<div class="config-line bottom-line ' + (roomInfo.description ? "" : "hide") + '">';
        html += '<div class="sub-heading">Mô tả</div>';
        html += '<p class="description">' + replaceEmoji(roomInfo.description) + '</p>';
        html += '</div>';
    }

    html += '<div class="config-line m-form__group form-group" style="margin-bottom:0">';
    //html += '<div class="checkbox checkbox-success">';
    html += '<div class="m-checkbox-list">';
    html += '<label class="m-checkbox m-checkbox--bold m-checkbox--state-success">';
    html += '<input type="checkbox" id="ckNotify' + roomInfo._id + '" ' + (!isMuted ? 'checked' : '') + '  /> ';
    html += 'Nhận notify từ group';
    html += '<span></span>';
    //html += '<label for="ckNotify' + roomInfo._id + '" id="lbNotify' + roomId + '">Nhận notify từ group</label>';
    html += '</label>';
    //html += '</div>';
    html += '</div>';
    html += '</div>';

    if (roomInfo.type == 'channel') {
        html += generateRoomEditInfoButton(roomId, roomInfo, (currentMember.isOwner || (currentMember.isAdmin && currentMember.permissions.editInfo)));

        html += generateRoomAddUsersButton(roomId, roomInfo, (currentMember.isOwner || (currentMember.isAdmin && currentMember.permissions.addUsers)));

        html += generateRoomAddNewAdminsButton(roomId, roomInfo, (currentMember.isOwner || (currentMember.isAdmin && currentMember.permissions.addNewAdmins)));
    }
    if (roomInfo.type == 'custom' || roomInfo.type == 'channel') {
        html += '<div class="config-line top-line">';
        html += '<a id="btLeaveRoom' + roomId + '" class="action-link" href="javascript:;"><span class="fa fa-sign-out"></span>Rời nhóm</a>';
        html += '</div>';
    }
    html += '</div>';

    $chatScrollPane.html(html);

    $('#btInfoAddMember' + roomId).click(function () {
        if (!$(this).prop('disabled')) {
            initInviteMemberBox(roomInfo);
            $('#inviteMemberToChatModal').showModal();
        }
    });

    $('#btLeaveRoom' + roomId).click(function () {
        removeRoomMember(roomId, currentMember.userInfo);
    });

    $('#ckNotify' + roomInfo._id).click(function (e) {
        var isMuted = !$(this).prop('checked');
        var eventMuteName = 'unmuteChatRoom';
        if (isMuted) {
            eventMuteName = 'muteChatRoom';
        }
        socket.emit(eventMuteName, {
            roomId: roomInfo._id
        }, function (result) {
            //console.log(eventMuteName);
            //console.log(result);
            if (result.errorCode != 0) {
                showNotifyError('Lỗi xảy ra khi cấu hình notify');
            }
        });
    });
}

function getRoomExentList(roomInfo) {
    generateRoomConfig(roomInfo);
    generateRoomMembers(roomInfo); //room members
    getRoomExtentItemList(messageType.file, roomInfo._id, null, ''); //room files
    getRoomExtentItemList(messageType.link, roomInfo._id, null, ''); //room links
    getRoomExtentItemList('pin', roomInfo._id, null, ''); //pin list
}
function disableChatRoom(roomId) {
    var $chatBox = getChatBox(roomId);
    if ($chatBox.length > 0) {
        $chatBox.find('textarea').prop('disabled', true);
        $chatBox.find('.big-button').prop('disabled', true);
        $chatBox.find('#btAddMember' + roomId).prop('disabled', true);
        $chatBox.find('.btPin').prop('disabled', true);
        $chatBox.find('.btUnPin').prop('disabled', true);
        $chatBox.find('#fileAttach' + roomId).prop('disabled', true);
    }
}
function enableChatRoom(roomId) {
    var $chatBox = getChatBox(roomId);
    if ($chatBox.length > 0) {
        $chatBox.find('textarea').prop('disabled', false);
        $chatBox.find('.big-button').prop('disabled', false);
        $chatBox.find('#btAddMember' + roomId).prop('disabled', false);
        $chatBox.find('.btPin').prop('disabled', false);
        $chatBox.find('.btUnPin').prop('disabled', false);
        $chatBox.find('#fileAttach' + roomId).prop('disabled', false);
    }
}
function removeRoomMemberFromLayout(roomId, memberInfo) {
    var $pane = getMemberPane(roomId);
    //console.log(memberInfo);
    $pane.find('li[data-id=' + memberInfo.userId + ']').remove();

}

function generateRoomMember(roomInfo, memberInfo, currentMember) {
    var roomId = roomInfo._id;
    //console.log(currentMember);
    var $chatMemberPane = getMemberPane(roomId);
    var userId = memberInfo.userId;
    if (!userId) {
        userId = memberInfo._id;
    }
    var html = '';
    html += '<li class="padding5 member pointer" data-id="' + userId + '">';
    html += '<div class="avatar-wrapper">';
    html += '<img width="50" height="50" class="circle avatar-chat" src= "' + memberInfo.avatar + '" />';
    html += '<div class="user-status ' + (memberInfo.online ? 'on' : 'off') + '"></div>';
    html += '</div>';
    html += '<div>';
    html += '<div class="item-name" data-email="' + memberInfo.email + '" data-mobile="' + memberInfo.phone + '">' + memberInfo.name + '</div>';
    html += '<div class="author-info">' + memberInfo.phone + '</div>';
    if (userId != currentUserId) {
        html += '<div class="member-button-wrapper">';
        html += '<a id="btMemberDropDown' + roomId + '_' + userId + '" class="small-button member-button dropdown-button">';
        html += '<span class="fa fa-ellipsis-h"></span>';
        html += '</a>';
        html += '<div id="dropdownMenu' + roomId + '_' + userId + '" class="dropdown-menu">';
        html += '<div class="menu-item">';
        html += '<a href="' + memberInfo.url + '" target="_blank">Xem profile</a>';
        html += '</div>';
        html += '<div class="menu-item">';
        html += '<a id="btCreateChat' + roomId + '_' + userId + '">Gửi tin nhắn</a>';
        html += '</div>';
        if (roomInfo.type == 'custom' || (roomInfo.type == 'channel' && currentMember && (currentMember.isOwner || (currentMember.isAdmin && currentMember.permissions && currentMember.permissions.addUsers)))) {

            html += '<div class="menu-divider">';
            html += '</div>';
            html += '<div class="menu-item">';
            html += '<a id="btRemoveRoomMember' + roomId + '_' + userId + '">Xoá khỏi Group</a>';
            html += '</div>';
        }
        html += '</div>';
    }
    else {
        html += '<div class="member-button-wrapper">';
        html += '<a id="btMemberDropDown' + roomId + '_' + userId + '" class="small-button member-button dropdown-button">';
        html += '<span class="fa fa-ellipsis-h"></span>';
        html += '</a>';
        html += '<div id="dropdownMenu' + roomId + '_' + userId + '" class="dropdown-menu">';
        html += '<div class="menu-item">';
        html += '<a href="' + memberInfo.url + '" target="_blank">Tới profile</a>';
        html += '</div>';
        if (roomInfo.type == 'custom' || (roomInfo.type == 'channel' && currentMember && (currentMember.isOwner || (currentMember.isAdmin && currentMember.permissions && currentMember.permissions.addUsers)))) {
            html += '<div class="menu-divider">';
            html += '</div>';
            html += '<div class="menu-item">';
            html += '<a id="btRemoveRoomMember' + roomId + '_' + userId + '">Rời Group</a>';
            html += '</div>';
        }
        html += '</div>';
    }
    html += '</li>';
    var $html = $(html);
    $html.appendTo($chatMemberPane.find('ul.item-list'));
    $html.on('click', function (e) {
        //console.log(e);
        if ($(e.target).hasClass('fa') || $(e.target).hasClass('member-button') ||
            $(e.target).hasClass('member-button-wrapper') ||
            $(e.target).hasClass('dropdown-menu') || $(e.target).closest('.dropdown-menu').length > 0) {
        }
        else {
            createPrivateChatRoom(userId);
        }
    });

    $('#btMemberDropDown' + roomId + '_' + userId).click(function (e) {
        $(this).toggleClass('open');
        $('#dropdownMenu' + roomId + '_' + userId).toggle();
    });

    $('#btRemoveRoomMember' + roomId + '_' + userId).click(function (e) {
        removeRoomMember(roomId, memberInfo);
    });

    $('#btCreateChat' + roomId + '_' + userId).click(function (e) {
        createPrivateChatRoom(userId);
    });
}

function setChatRoomSelected($recentItem, item, needGet) {
    var url = window.location.protocol + '//' + window.location.host + window.location.pathname + "?/" + item._id;
    var title = item.roomName + " - ChatNhanh";
    history.pushState('', title, url);
    showChatRoom(item, function () {
        setRecentChatRoomSelected($recentItem);
        setChatIsViewed(item._id);
    }, needGet);
}

function createPrivateChatRoom(userId) {
    socket.emit('getPrivateRoom', {
        userId: userId
    }, function (result) {
        if (result.errorCode == 0) {
            var item = result.data;
            var $recentItem = $('#divRecentChat li[data-id=' + item._id + ']');
            setChatRoomSelected($recentItem, item);
        }
    });
}
function generateAddMemberButton(roomInfo, $chatMemberPane) {
    var currentMember = roomInfo.members.find(function (x) {
        return x.userId == currentUserId;
    });

    if ((!roomInfo.onlyAdminAddUser && roomInfo.type == 'custom') || //ko phải chỉ admin mới đc thêm thì ai cũng đc thêm        
        roomInfo.everyoneIsAdmin ||
        currentMember.isOwner ||
        (currentMember.isAdmin && currentMember.permissions && currentMember.permissions.addUsers)
    ) {
        var html = '';
        html += '<li class="padding5 add-member">';
        html += '<a class="no-margin no-padding" id="btAddMember' + roomInfo._id + '">';
        html += '<span class="fa fa-plus circle-button"></span>';
        html += '<div class="item-name">Thêm member</div>';
        html += '</a>';
        html += '</li>';

        $chatMemberPane.find('ul.item-list').append(html);

        $('#btAddMember' + roomInfo._id).click(function (e) {
            if (!$(this).prop('disabled')) {
                initInviteMemberBox(roomInfo);
                $('#inviteMemberToChatModal').showModal();
            }
        });
    }
}
function sortRoomMembers(roomInfo) {
    var $chatMemberPane = getMemberPane(roomInfo._id);
    var $lst = $chatMemberPane.find('ul.item-list li.member')
        .map(function () {
            return { name: removeAccent($(this).find(".item-name").html().toLowerCase()), status: ($(this).find('.status').hasClass('on') ? false : true), el: this };
        })
        .sort(function (a, b) {
            return a.name > b.name ? 1 : a.name < b.name ? -1 : 0;
        })
        .map(function () {
            // reduce the list to the actual dom-element
            return this.el;
        });
    if ($chatMemberPane.find('ul.item-list li.add-member').length > 0) {
        $lst.insertAfter($chatMemberPane.find('ul.item-list li.add-member'));
    }
    else {
        $lst.appendTo($chatMemberPane.find('ul.item-list'));
    }
}
function generateRoomMembers(roomInfo) {
    //console.log(roomInfo);
    var $chatMemberPane = getMemberPane(roomInfo._id);
    var currentMember;
    //console.log('currentUserId', currentUserId);
    if (roomInfo.type == 'custom' || roomInfo.type == 'channel') {
        generateAddMemberButton(roomInfo, $chatMemberPane);
        currentMember = roomInfo.members.find(function (x) {
            return x.userId == currentUserId;
        });
        if (roomInfo.onlyAdminAddUser) {
            if (!currentMember.isOwner && !(currentMember.isAdmin && currentMember.permissions && currentMember.permissions.addUsers)) {
                $('#btAddMember' + roomInfo._id).closest('li').addClass('hide');
            }
        }
    }
    //console.log('currentMember');
    //console.log(currentMember);
    var members = _.uniqBy(roomInfo.members, 'userId');
    for (var i = 0; i < members.length; i++) {
        generateRoomMember(roomInfo, members[i].userInfo, currentMember);
    }
    sortRoomMembers(roomInfo);
}
function clearRoomSearchResult(roomId) {
    var $chatScrollPane = getExtentPaneCotentWrapper(roomId, 'search');
    $chatScrollPane.find('ul').html('');
}

function clearRoomMedias(type, roomId) {
    var $chatScrollPane = getExtentPaneCotentWrapper(roomId, type);
    $chatScrollPane.find('ul').html('');
}

function displayExtentSearchResultLabel($chatScrollPane, keyword) {
    if ($chatScrollPane.find('ul.item-list li').length > 0) {
        $chatScrollPane.find('ul.item-list').removeClass('hide');
        $chatScrollPane.find('div.no-result').addClass('hide');
    }
    else {
        $chatScrollPane.find('ul.item-list').addClass('hide');
        if (keyword) {
            $chatScrollPane.find('div.no-result .default').hide();
            $chatScrollPane.find('div.no-result .custom').html('Không tìm thấy kết quả tương ứng với "' + keyword + '"');
            $chatScrollPane.find('div.no-result .custom').show();
        }
        else {
            $chatScrollPane.find('div.no-result .default').show();
            $chatScrollPane.find('div.no-result .custom').hide();
        }
        $chatScrollPane.find('div.no-result').removeClass('hide');
    }
}

function getRoomExtentItemList(type, roomId, lastLogId, keyword, noLoadHasNoScrollbar) {
    //console.log(roomId);
    //console.log(type);
    var $chatScrollPane = getExtentPaneCotentWrapper(roomId, type);
    //console.log($chatScrollPane);
    var $current_top_element = $chatScrollPane.find('ul').children().first();
    var eventName = 'getRoomFiles';
    var isSearch = false;
    switch (type) {
        case messageType.link: {
            eventName = 'getRoomLinks';
            break;
        }
        case 'search': {
            eventName = 'searchRoomLogs';
            isSearch = true;
            break;
        }
        case 'pin': {
            eventName = 'getPinLogs';
            break;
        }
    }
    socket.emit(eventName,
        {
            roomId: roomId,
            lastLogId: lastLogId,
            keyword: keyword
        }, function (result) {

            if (result.errorCode == 0) {
                var logs = result.data;
                if (logs) {

                    for (var i = 0; i < logs.length; i++) {
                        var log = logs[i];
                        generateExtentItem(roomId, log, type, false, keyword);
                    }

                    displayExtentSearchResultLabel($chatScrollPane, keyword);
                    //console.log(lastLogId);
                    if (!lastLogId) { //lần đầu
                        //scrollToBottom($chatScrollPane);
                        //lấy số lượng tin chưa đọc
                        //getRoomUnreadCount(logs);
                    }
                    else {
                        //scroll tới message mới load
                        //var previous_height = 0;
                        //$current_top_element.prevAll().each(function () {
                        //    previous_height += $(this).outerHeight();
                        //});

                        //$chatScrollPane.scrollTop(previous_height);
                    }

                    if (logs.length > 0) {
                        //console.log('has scrollbar', $chatScrollPane.hasScrollBar());
                        if ($chatScrollPane.hasScrollBar()) {
                            $chatScrollPane.bind('scroll', function () {
                                console.log('scroll', $(this).innerHeight(), this.scrollHeight, $(this).scrollTop());
                                if ($(this).innerHeight() < this.scrollHeight) {
                                    if ($(this).scrollTop() >= (this.scrollHeight - $(this).innerHeight() - 50)) {
                                        //console.log('load more');
                                        $(this).unbind('scroll');
                                        getRoomExtentItemList(type, roomId, logs[logs.length - 1]._id, keyword);
                                    }
                                }
                            });
                        }
                        else {
                            if (!noLoadHasNoScrollbar) {
                                getRoomExtentItemList(type, roomId, logs[logs.length - 1]._id, keyword, true);
                            }
                            else {
                                $chatScrollPane.bind('scroll', function () {
                                    console.log('scroll', $(this).innerHeight(), this.scrollHeight, $(this).scrollTop());
                                    if ($(this).innerHeight() < this.scrollHeight) {
                                        if ($(this).scrollTop() >= (this.scrollHeight - $(this).innerHeight() - 50)) {
                                            //console.log('load more');
                                            $(this).unbind('scroll');
                                            getRoomExtentItemList(type, roomId, logs[logs.length - 1]._id, keyword);
                                        }
                                    }
                                });
                            }
                        }
                    }
                }
            }
            else {
                showNotifyError('Lỗi không lấy được log!');
            }
        });
}

function getRecentChatItem(roomId) {
    return $('#divRecentChat li[data-id=' + roomId + ']');
}

function getChatBox(roomId) {
    return $('#divChatBoxList').find('.chat-pane[data-id=' + roomId + ']');
}

function getChatSearchResultWrapper(roomId) {
    return $('#divChatBoxList').find('.chat-pane[data-id=' + roomId + '] .chat-search-result');
}

function getMemberPane(roomId) {
    return getExtentPaneCotentWrapper(roomId, 'member');
}

function getExtentPaneCotentWrapper(roomId, type) {
    var paneName;
    switch (type) {
        case messageType.file:
        case messageType.image:
        case messageType.video: {
            paneName = '.filePane';
            break;
        }
        case messageType.link: {
            paneName = '.linkPane';
            break;
        }
        case 'member': {
            paneName = '.memberPane';
            break;
        }
        case 'search': {
            paneName = '.searchResultPane';
            break;
        }
        case 'pin': {
            paneName = '.pinPane';
            break;
        }
        case 'config': {
            paneName = '.configPane';
            break;
        }
    }
    //console.log(paneName);
    return $('#divChatBoxList').find('.chat-pane[data-id=' + roomId + '] ' + paneName + ' .scroll-content');
}

function getExtentPaneHeader(roomId, type) {
    var paneName;
    switch (type) {
        case messageType.file:
        case messageType.image:
        case messageType.video: {
            paneName = '.filePane';
            break;
        }
        case messageType.link: {
            paneName = '.linkPane';
            break;
        }
        case 'member': {
            paneName = '.memberPane';
            break;
        }
        case 'search': {
            paneName = '.searchResultPane';
            break;
        }
    }
    //console.log(paneName);
    return $('#divChatBoxList').find('.chat-pane[data-id=' + roomId + '] ' + paneName + ' .header');
}

//create custom chat
function createCustomChat(members) {
    socket.emit('createChatRoom', {
        members: members
    }, function (result) {
        //console.log(result);

        if (result.errorCode == 0) {
            creatingChatRoomId = result.data._id;
            $('#createChatModal').closeModal();
            $select2Search.val(null).trigger('change');
            if (members.length == 1) {
                var item = {
                    _id: creatingChatRoomId
                };

                var $item = $('ul#divRecentChat > li[data-id=' + creatingChatRoomId + ']');
                showChatRoom(item, function () {
                    if ($item.length > 0) {
                        setRecentChatRoomSelected($item);
                    }
                    setChatIsViewed(item._id);
                });
                creatingChatRoomId = '';
            }
        }
        else {
            showNotifyError('Lỗi xảy ra khi tạo nhóm chat');
            //console.log(result);
        }
    });
}

//invite
function initInviteMemberBox(roomInfo) {
    currentInviteRoomInfo = roomInfo;
    $('#inviteChatRoomName').html(roomInfo.roomName);
    $select2InviteSearch.val(null).trigger('change');
}

//remove member
function removeRoomMember(roomId, memberInfo) {
    confirmNotify('Xoá thành viên ra khỏi group', function (res) {
        if (res) {

            //console.log(memberInfo);
            var userId = memberInfo.userId;
            if (!userId) {
                userId = memberInfo._id;
            }
            //console.log('remove', userId);
            memberInfo.userId = userId;

            socket.emit('removeRoomMember',
                {
                    roomId: roomId, memberInfo: memberInfo
                },
                function (result) {
                    //console.log(result);
                    if (result.errorCode != 0) {
                        if (result.errorCode == 100) {
                            showNotifyError('Không có quyền thực hiện thao tác này!');
                        }
                        else {
                            showNotifyError('Lỗi xảy ra khi xoá thành viên!');
                        }
                    }
                });
        }
    });
}
function inviteAMember(roomId, member, callback) {
    socket.emit('addRoomMember',
        {
            roomId: roomId,
            memberInfo: member
        },
        function (result) {
            callback();
        });
}
function inviteMembersToChat(roomId, members) {
    var itemProcessed = 0;
    //console.log(roomId);
    for (var i = 0; i < members.length; i++) {
        inviteAMember(roomId, members[i], function () {
            itemProcessed++;
            //console.log('process: ' + itemProcessed);
            //console.log('member length' + members.length);
            if (itemProcessed == members.length) {
                //console.log('close');
                $('#inviteMemberToChatModal').closeModal();
                currentInviteRoomInfo = null;
            }
        });
    }
}

//action phát sinh trong newMessage
function generateAdditionAction(log) {
    var $chatBox;
    var $recentItem;
    switch (log.content.actionType) {
        case actionType.addMember: {
            generateRoomMember(log.roomInfo, log.content.data.memberInfo);
            enableChatRoom(log.roomId);
            break;
        }
        case actionType.removeMember: {
            removeRoomMemberFromLayout(log.roomId, log.content.data.memberInfo);
            //xoá khỏi chat gần đây nếu bị remove
            if (currentUserId == log.content.data.memberInfo.userId) {
                $('#divRecentChat').find('li[data-id=' + log.roomId + ']').remove();
                disableChatRoom(log.roomId);
            }
            break;
        }
        case actionType.changeAvatarRoom: {
            $chatBox = getChatBox(log.roomId);
            if ($chatBox.length > 0) {
                $chatBox.find('.chat-top-bar img.box-avatar').attr('src', log.content.data.roomAvatar);
            }
            $recentItem = $('#divRecentChat li[data-id=' + log.roomId + ']');
            if ($recentItem.length > 0) {
                $recentItem.find('.room-avatar').attr('src', log.content.data.roomAvatar);
            }
            break;
        }
        case actionType.renameRoom: {
            $chatBox = getChatBox(log.roomId);
            if ($chatBox.length > 0) {
                $chatBox.find('.box-name').html(log.content.data.roomName);
            }
            $recentItem = $('#divRecentChat li[data-id=' + log.roomId + ']');
            if ($recentItem.length > 0) {
                $recentItem.find('.chat-room-name').html(log.content.data.roomName);
            }
            break;
        }
    }
}

//Recent chat room


function updateRecentChatRoom(log, isDelete) {
    var $recentItem = $('#divRecentChat').find('li[data-id=' + log.roomId + ']');
    if ($recentItem.length > 0) {
        if (isDelete == true) {
            if ($recentItem.attr('data-lastlogid') == log._id) {
                var html = generateLastLogFromType(log);
                $recentItem.find('.last-message span').html(html);
            }
        }
        else {
            var type = $recentItem.data('type');
            $recentItem.find('.last-message').html(generateLastMessage(log.authorInfo.name, log, type));
            $recentItem.find('.last-message-time').html(formatDateTimeShort(log.createDate));

            var $lastFav = $('#divRecentChat .pin-room:last');
            var recentIsFav = $recentItem.hasClass('pin-room');
            $recentItem.attr('data-lastlogid', log._id);
            if (recentIsFav) {
                $recentItem.prependTo($('#divRecentChat'));
            }
            else {
                if ($lastFav.length > 0) {
                    $recentItem.insertAfter($lastFav);
                }
                else {
                    $recentItem.prependTo($('#divRecentChat'));
                }
            }
            if (log.userIdAuthor != currentUserId) {
                $recentItem.attr('data-view', false);
            }
        }
    }
    else {
        if (!isDelete) {
            //console.log(log);
            var item = {
                _id: log.roomId,
                type: log.roomInfo.type,
                roomName: log.roomInfo.roomName,
                roomAvatar: log.roomInfo.roomAvatar,
                members: log.roomInfo.members,
                lastLog: log,
                lastLogDate: log.createDate,
                lastLogAuthor: log.authorInfo,
                userId1: log.roomInfo.userId1,
                userId2: log.roomInfo.userId2
            };
            if (item.type == 'private') {
                if (item.userId1 != item.userId2) {
                    try {
                        var roomName = item.members[0].userId == currentUserId ? item.members[1].userInfo.name : item.members[0].userInfo.name;
                        var roomAvatar = item.members[0].userId == currentUserId ? item.members[1].userInfo.avatar : item.members[0].userInfo.avatar;
                        item.roomName = roomName;
                        item.roomAvatar = roomAvatar;
                    }
                    catch (ex) {

                    }
                }
            }
            generateRecentChatRoomItem($('#divRecentChat'), item, true);
        }
    }

    setTimeout(function () {
        removeDuplicateRecentRoom();
    }, 500);
}


function generateLastMessage(authorName, lastLog, roomType) {
    var html = '';
    if (lastLog.type != messageType.action && roomType.toLowerCase() != 'channel') {
        html += authorName + ':&nbsp;&nbsp;';
    }
    else if (lastLog.type != messageType.action && roomType.toLowerCase() == 'channel') {
        html += '<i class="la la-bullhorn"></i>' + ':&nbsp;&nbsp;';
    }
    html += '<span>';
    html += generateLastLogFromType(lastLog);
    html += '</span>';
    return html;
}

function generateRecentChatRoomItem($recentWrapper, item, isPrepend, isFav, isForward) {
    if (isFav == undefined || isFav == null) {
        isFav = false;
    }
    var html = '';
    var currentView = { isViewed: false };

    if (item.lastLog) {
        currentView = item.lastLog.views.find(function (view) {
            return view.userId == currentUserId;
        });
    }

    var roomName = item.roomName;
    var roomAvatar = item.roomAvatar;

    if (item.type == 'private' && item.userId1 == item.userId2) {
        roomName = 'Tin nhắn lưu trữ';
        roomAvatar = '/images/fave.png';
    }

    html += '<li  class="' + (isFav ? "pin-room" : "") + '" data-type="' + item.type + '" data-id="' + item._id + '" data-view="' + (currentView ? currentView.isViewed : false) + '" data-lastlogid="' + (item.lastLog ? item.lastLog.chatLogId : null) + '">';
    html += '<img class="room-avatar circle" src= "' + roomAvatar + '" />';
    html += '<div class="chat-item">'; //begin chat-item
    html += '<div class="chat-info">'; //begin chat-info
    html += '<div class="chat-room-name" title="' + escapeQuote(roomName) + '">' + roomName + '</div>';
    html += '<div class="last-message-time">' + formatDateTimeShort(item.lastLogDate) + '</div>';
    html += '</div>'; //end chat-info
    html += '<div class="last-message">'; //begin last-message
    if (item.lastLog && item.lastLogAuthor) {
        html += generateLastMessage(item.lastLogAuthor.name, item.lastLog, item.type);
    }
    html += '</div>';//end last-message
    html += '</div>';//end chat-item
    html += '<div class="pinWrap"><span id="btPinRoom' + item._id + '" class="fa ' + (isFav ? 'fa-star' : 'fa-star-o') + ' btFavorite" data-favorite="' + isFav + '" data-id="' + item._id + '"></span></div>';
    html += '</li>';
    var $item = $(html);
    var $lastFav;
    if (!isForward) {

        $item.click(function () {
            //console.log(window.location);
            if (isPrepend) {
                setChatRoomSelected($item, item, true);
            }
            else {
                setChatRoomSelected($item, item);
            }
        });
    }
    else {
        //forward
        $item.click(function () {
            $(this).closest('ul').find('li').removeClass('selected');
            $(this).addClass('selected');
            selectedForwardRoomId = item._id;
        });
    }

    if (isPrepend) {
        if (isFav) {
            $recentWrapper.prepend($item);
        }
        else {
            $lastFav = $('#divRecentChat .pin-room:last');
            if ($lastFav.length > 0) {
                $item.insertAfter($lastFav);
            }
            else {
                $recentWrapper.prepend($item);
            }
        }
    }
    else {
        if (isFav) {
            $lastFav = $('#divRecentChat .pin-room:last');
            if ($lastFav.length > 0) {
                $item.insertAfter($lastFav);
            }
            else {
                $recentWrapper.prepend($item);
            }
        }
        else {
            $recentWrapper.append($item);
        }
    }

    if (!isForward) {
        $('#btPinRoom' + item._id).click(function () {

            var $this = $(this);
            var $recentItem = $this.closest('li');
            var isFavorite = $this.data('favorite');
            var roomId = $this.data('id');
            //console.log(roomId);
            //console.log(isFavorite);
            if (isFavorite) {
                socket.emit('unpinRoom', { roomId: roomId }, function (result) {
                    //console.log(result);
                    if (result.errorCode == 0) {
                        $this.closest('li').removeClass('pin-room');
                        $this.data('favorite', !isFavorite);
                        $this.removeClass('fa-star');
                        $this.addClass('fa-star-o');
                        var $lastFav = $('#divRecentChat .pin-room:last');
                        if ($lastFav.length > 0) {
                            $recentItem.detach().insertAfter($lastFav);
                        }
                    }
                });
            }
            else {
                socket.emit('pinRoom', { roomId: roomId }, function (result) {
                    //console.log(result);
                    if (result.errorCode == 0) {
                        $this.closest('li').addClass('pin-room');
                        $this.data('favorite', !isFavorite);
                        $this.removeClass('fa-star-o');
                        $this.addClass('fa-star');
                        $recentItem.detach().prependTo('#divRecentChat');
                    }
                });
            }
        });
    }
}

function getRecentChatRoom(lastLogDate, keyword, callback) {
    socket.emit('getRecentChatRoom', {
        lastLogDate: lastLogDate,
        keyword: keyword
    }, function (result) {
        callback(result);
    });
}
function getPinRecentChatRoom(callback) {
    socket.emit('getRecentPinChatRoom', {
    }, function (result) {
        callback(result);
    });
}
function loadRoomFromUrl() {
    var search = window.location.search;
    if (search != '' && search != undefined) {
        var roomId = search.substr(2);
        var item = {
            _id: roomId
        };
        var $item = $('ul#divRecentChat > li[data-id=' + roomId + ']');
        showChatRoom(item, function () {
            if ($item.length > 0) {
                setRecentChatRoomSelected($item);
            }
            setChatIsViewed(item._id);
        });
    }
    else
        $('ul#divRecentChat > li').first().click();
}
function removeDuplicateRecentRoom() {
    $('#divRecentChat li[data-id]').each(function (e) {
        var roomId = $(this).data('id');
        var $lis = getRecentChatItem(roomId);
        if ($lis.length > 1) {
            $('#divRecentChat li[data-id=' + roomId + ']:gt(0)').remove();
        }
    });
}
function loadRecentPinChatRoom($recentWrapper, isForward) {
    getPinRecentChatRoom(function (result) {
        if (result.errorCode == 0) {
            var lst = result.data;
            for (var i = 0; i < lst.length; i++) {
                generateRecentChatRoomItem($recentWrapper, lst[i], false, true, isForward);
            }
        }
    });
}

function loadRecentChatRoom($recentWrapper, lastLogDate, keyword, isFirst, isForward) {
    getRecentChatRoom(lastLogDate, keyword, function (result) {
        if (result.errorCode == 0) {

            var lst = result.data;
            //console.log(lst);
            for (var i = 0; i < lst.length; i++) {
                generateRecentChatRoomItem($recentWrapper, lst[i], null, null, isForward);
            }

            if (lst.length > 0) {
                //console.log(createRoomInfo.type);
                if (isFirst && /*createRoomInfo.type == '' &&*/ !isForward) {
                    loadRoomFromUrl();
                }

                if ($recentWrapper.closest('.chat-pane-list').hasScrollBar()) {
                    //có thì scroll load tiếp
                    $recentWrapper.closest('.chat-pane-list').bind('scroll', function () {
                        //console.log('hello');                        
                        if ($(this).innerHeight() < this.scrollHeight) {
                            if ($(this).scrollTop() >= (this.scrollHeight - $(this).innerHeight() - 50)) {
                                $(this).unbind('scroll');
                                loadRecentChatRoom($recentWrapper, lst[lst.length - 1].lastLogDate, keyword, null, isForward);
                            }
                        }
                    });
                }
                else {
                    loadRecentChatRoom($recentWrapper, lst[lst.length - 1].lastLogDate, keyword, null, isForward);
                }
            }
            removeDuplicateRecentRoom();
        }
    });
}

$(document).ready(function () {
    bindRecentRoomAction();
});

function showSearchRecent(isShow) {
    if (isShow) {
        $('#divRecentSearch').removeClass('hide');
        $('#divRecentChat').addClass('hide');
    }
    else {
        $('#divRecentSearch').addClass('hide');
        $('#divRecentChat').removeClass('hide');
    }
}

//main chat content
function generateChatRoom(info, notBindAction) {
    var $chatBox = $('<div class="chat-pane"></div>');
    $chatBox.attr('data-id', info._id);
    $chatBox.attr('data-type', info.type);
    var roomId = info._id;
    console.log(currentUserId);
    console.log(info.members);
    var currentMember = info.members.find(function (member) { return member.userId == currentUserId; });
    //console.log(info);
    console.log(currentMember);
    var isGroupChannel = info.type == 'custom' || info.type == 'channel';
    var html = '';
    html += '<div class="chat-top-bar">';   //begin chat-top-bar
    var roomAvatar = info.roomAvatar;
    if (info.type == 'private' && info.userId1 == info.userId2) {
        roomAvatar = '/images/fave.png';
    }
    html += '<img class="box-avatar circle ' + (isGroupChannel ? "editable-img" : "") + '" src="' + decodeURITryCatch(roomAvatar) + '" />';
    if (isGroupChannel) {
        html += '<input type="file" data-id="' + roomId + '" accept=".jpg, .jpeg, .png .gif" id="fileAvatar' + roomId + '" name="file" style="display:none" />';
    }
    html += '<div class="box-info">'; //begin box-info    
    switch (info.type) {
        case 'private': {
            var otherMember = info.members.find(function (x) {
                return x.userId != currentUserId;
            });
            if (otherMember) {
                //console.log('member');
                //console.log(member);
                html += '<div class="box-name"><a href="' + otherMember.userInfo.url + '" target="_blank">' + decodeURITryCatch(info.roomName) + '</a></div>';
                html += '<div class="boxMoreInfo">'; //begin boxMoreInfo
                html += 'Mobile: ' + otherMember.userInfo.phone;
                html += '</div>'; //end boxMoreInfo
            }
            else {
                html += '<div class="box-name">Tin nhắn lưu trữ</div>';
            }
            break;
        }
        case 'item': {
            html += '<div class="box-name"><a href="' + decodeURITryCatch(info.item.itemLink) + '" target="_blank">' + decodeURITryCatch(info.roomName) + '</a></div>';
            html += '<div class="boxMoreInfo">'; //begin boxMoreInfo
            html += 'Giá bán: ' + formatCurrencyVND(info.item.itemPrice);
            html += '</div>'; //end boxMoreInfo
            break;
        }
        case 'page': {
            html += '<div class="box-name"><a href="' + decodeURITryCatch(info.page.pageLink) + '" target="_blank">' + decodeURITryCatch(info.roomName) + '</a></div>';
            html += '<div class="boxMoreInfo">'; //begin boxMoreInfo
            html += info.memberCount + ' thành viên';
            html += '</div>'; //end boxMoreInfo
            break;
        }
        default: {
            html += '<div class="box-name editable">' + decodeURITryCatch(info.roomName) + '</div>';
            html += '<div class="boxMoreInfo">'; //begin boxMoreInfo
            html += info.memberCount + ' thành viên';
            html += '</div>'; //end boxMoreInfo
            break;
        }
    }
    html += '</div>';//end box-info
    html += '<div class="tool-section">';//begin tool-section
    html += '<a class="tool-button mr10 btExtentTab" title="Tìm kiếm trong chat" href= ".searchResultPane"><span class="fa fa-search"></span></a>';
    if (info.type != 'channel' || (currentMember.isOwner || currentMember.isAdmin)) {
        html += '<a class="tool-button mr10 btExtentTab memberExtent selected" title="Thành viên" href= ".memberPane"><span class="fa fa-users"></span></a>';
    }
    else {
        html += '<a class="tool-button mr10 btExtentTab hide memberExtent selected" title="Thành viên" href= ".memberPane"><span class="fa fa-users"></span></a>';
    }
    html += '<a class="tool-button mr10 btExtentTab" title="Danh sách file" href= ".filePane"><span class="fa fa-paperclip"></span></a>';
    html += '<a class="tool-button mr10 btExtentTab" title="Danh sách link" href= ".linkPane"><span class="fa fa-link"></span></a>';
    html += '<a class="tool-button mr10 btExtentTab" title="Danh sách tin nhắn đã pin" href= ".pinPane"><span class="fa fa-thumb-tack"></span></a>';
    html += '<a class="tool-button mr10 btExtentTab" title="Cấu hình chat room" href= ".configPane"><span class="fa fa-info-circle"></span></a>';
    html += '</div>';//end tool-section 
    html += '</div>';//end chat-top-bar
    html += '<div class="chat-main">'; //begin chat-main
    html += '<div class="chat-window chat">'; //begin chat-window chat
    html += '<div data-id="' + roomId + '" class="dragandrophandler" ondragenter="onDragenter(event, this)" ondragover="onDragover(event)" ondragleave="onDragleave(event, this)" ondrop="onDrop(event, this)">Kéo & thả tập tin vào đây để upload</div>';
    html += '<div class="chat-search-result chat-logs hide">'; //begin chat-search-result
    html += '<div class="search-result-title">'; //begin search-result-title
    html += 'Xem tin nhắn cũ từ <span class="startDate"></span> đến <span class="endDate"></span>';
    html += '<i class="fa fa-remove btCloseSearchResult" id="btCloseSearchResult' + roomId + '"></i>';
    html += '</div>'; //end search-result-title
    html += '<ul></ul>';
    html += '</div>'; //end chat-search-result
    html += '<div class="chat-history chat-logs" data-id="' + roomId + '">';
    html += '<ul></ul>';
    html += '<div class="scroll-down-wrapper" style="display:none"><a id="btScrollToBottom' + roomId + '" class="btn-scroll-down btn btn-circle btn-default"><i class="fa fa-chevron-down"></i></a></div>';
    html += '</div>';
    html += '<div class="reply-quote reply-quote-main">';
    html += '<a class="btn btCloseQuote" id="btCloseQuote' + roomId + '"><span class="fa fa-remove"><span></a>';
    html += '<div class="quote-content clearfix">';
    html += '<div class="quote-image"><img class="" src=""/></div>';
    html += '<div class="quote-panel">';
    html += '<div class="quote-user">';
    html += '';
    html += '</div>'; //end quote-user
    html += '<div class="quote-text">';
    html += '';
    html += '</div>'; //end quote-text
    html += '</div>'; //end quote-panel
    html += '</div>'; //end quote-content
    html += '</div>'; //end reply-quote
    html += '<div class="chat-input">'; //begin chat-input        
    //console.log('member');
    //console.log(currentMember);
    if (isGroupChannel) {
        if (currentMember) {
            if (info.type == 'channel') {
                if (currentMember.isOwner || (currentMember.isAdmin && currentMember.permissions.postMessage)) {
                    html += generateChatInput(roomId, true);
                }
                else {
                    //mute
                    //join
                    //html += '<div class="chat-input-disabled">';
                    //html += '<a id="btMuteNotify' + roomId + '" class="big-button">Mute</a>';
                    //html += '</div>';
                    html += generateChatInput(roomId, false);
                }
            }
            else if (info.type == 'custom') {
                html += generateChatInput(roomId, true);
            }
        }
        else {
            //join
            //html += '<div class="chat-input-disabled">';
            //html += '<a class="big-button">Join</a>';
            //html += '</div>';
            html += generateChatInput(roomId, false);
        }
    }
    else {
        html += generateChatInput(roomId, true);
    }
    html += '</div>';//end chat-input
    html += '<div class="emoji-wrapper" id="divEmojiWrapper' + roomId + '"></div>';
    html += '</div>';//end chat-window chat    
    html += '<div class="chat-added-info">'; //begin chat-added-info
    html += generateExtentPane(roomId, 'memberPane', 'Thành viên', 'Tìm thành viên ...', 'Chưa có thành viên', true, true, true);
    html += generateExtentPane(roomId, 'pinPane', 'Tin nhắn được pin', '', 'Chưa có pin trong room', false, false, true);
    html += generateExtentPane(roomId, 'filePane', 'Tất cả files', 'Tìm file ...', 'Chưa có file trong room', false, true, true);
    html += generateExtentPane(roomId, 'linkPane', 'Tất cả links', 'Tìm link ...', 'Chưa có link trong room', false, true, true);
    html += generateExtentPane(roomId, 'searchResultPane', 'Tìm kiếm tin nhắn', 'Tìm nội dung tin ...', 'Không có kết quả', false, true, true);
    html += generateExtentPane(roomId, 'configPane', 'Cấu hình', '', '', false, false, false);
    html += '</div>';//end chat-added-info
    html += '</div>';//end chat-main

    $chatBox.html(html);

    $('#divChatBoxList').append($chatBox);
    activeChatRoom($chatBox);

    if (!notBindAction) {
        var $chatHistory = getChatHistoryWrapper(roomId);
        bindChatRoomAction(roomId, $chatHistory, info.members);
        if (isGroupChannel) {
            bindCustomRoomAction(info);
        }
    }
}

function generateChatInput(roomId, isShow) {
    var html = '';
    html += '<div class="text-input ' + (isShow ? "" : "hide") + '">';  //begin text-input
    html += '<textarea id="tbChatInput' + roomId + '" class="" placeholder= "Nhập để chat ..." />';
    html += '</div>';//end text-input
    html += '<div class="other-input ' + (isShow ? "" : "hide") + '">'; //begin other-input
    html += '<input type="file" data-id="' + roomId + '" multiple id="fileAttach' + roomId + '" name="file" style="display:none" />';
    html += '<a class="big-button mr5 btEmoji" id="btEmoji' + roomId + '"><i class="fa fa-smile-o"></i></a>';
    html += '<a class="big-button mr5" id="btUploadFile' + roomId + '"><i class="fa fa-paperclip"></i></a>';
    html += '<a class="big-button mr5" id="btAddPlan' + roomId + '"><i class="fa fa-calendar"></i></a>';
    //html += '<a class="big-button" id="btPickLocation' + roomId + '"><i class="fa fa-map-marker"></i></a>';
    html += '<a class="big-button" id="btSendLike' + roomId + '"><img class="emojione" alt="👍" title=":thumbsup:" src="/emojione/32/1f44d.png"></a>';
    //html += '<a class="big-button" id="btSendLike' + roomId + '"><i class="fa fa-thumbs-o-up"></i></a>';
    html += '<a class="big-button hide large-font" id="btSendText' + roomId + '">Send</a>';
    html += '</div>';//end other-input
    return html;
}

function bindCustomRoomAction(roomInfo) {
    var roomId = roomInfo._id;
    var $chatBox = getChatBox(roomId);

    $chatBox.find('.box-name').editable(function (value, settings) {
        var origvalue = this.revert;
        //console.log(origvalue);
        if (origvalue != value) { //có thay đổi mới gọi socket, ko thì thôi
            var data = { roomId: roomId, roomName: value };
            socket.emit('renameChatRoom', data, function (result) {
                //console.log(result);
            });
        }
        return (value);
    }, {
            type: 'text',
            submit: 'Done',
            onblur: 'cancel'
        });
    $chatBox.find('.box-avatar').click(function (e) {
        $('#fileAvatar' + roomId).click();
    });

    $('#fileAvatar' + roomId).bind('change', function () {
        var files = this.files;
        if (files.length > 0) {
            handleFileAvatarUpload(files[0], roomId);
        }
    });

    //$('#btMuteNotify' + roomId).click(function () {

    //});
}

function handleFileAvatarUpload(file, roomId) {
    var fileName = file.name.toString();
    fileName = replaceSpecialCharacter(compound2Unicode(fileName));
    var itemGUID = generateGUID();
    socket.emit('generateUploadSAS', { roomId: roomId, itemGUID: itemGUID, fileName: fileName },
        function (result) {
            if (result.errorCode == 0) {
                var az = result.data;
                executeAvatarToCloud(roomId, file, az, itemGUID);
            }
            else {
                //console.log(result);
                showNotifyError('Lỗi xảy ra khi upload');
            }
        });
}
function executeAvatarToCloud(roomId, file, az, itemGUID) {
    resizeImage(file, 300).then(function (res) {
        var thumbfile = res.file;
        //console.log(az);
        uploadFileToCloud(thumbfile, az.sasUrl, itemGUID, false).then(function (resX) {
            if (resX.errorCode == 0) {
                socket.emit('changeAvatarChatRoom',
                    {
                        roomId: roomId,
                        roomAvatar: az.link
                    },
                    function (result) {

                    });
            }
            else {
                showNotifyError('Lỗi xảy ra khi upload');
            }
        });
    });
}
function updateExtentPaneTitle(roomId, type, keyword) {
    var $paneHeader = getExtentPaneHeader(roomId, type);

    var header = '';
    switch (type) {
        case messageType.file:
        case messageType.image:
        case messageType.video: {
            if (keyword) {
                header = 'Kết quả tìm "' + keyword + '"';
            }
            else {
                header = 'Tất cả files';
            }
            break;
        }
        case messageType.link: {
            if (keyword) {
                header = 'Kết quả tìm "' + keyword + '"';
            }
            else {
                header = 'Tất cả links';
            }
            break;
        }
        case 'member': {
            if (keyword) {
                header = 'Kết quả tìm "' + keyword + '"';
            }
            else {
                header = 'Thành viên';
            }
            break;
        }
        case 'search': {
            if (keyword) {
                header = 'Kết quả tìm "' + keyword + '"';
            }
            else {
                header = 'Tìm kiếm tin nhắn';
            }
            break;
        }
    }
    $paneHeader.html(header);
}
function bindChatRoomAction(roomId, $chatHistory, members) {
    var $chatTextarea = $('#tbChatInput' + roomId);

    var mentionList = members.map(function (x) {
        return {
            uid: "userid:" + x.userId,
            value: x.userInfo.name,
            email: x.userInfo.phone,
            image: x.userInfo.avatar,
            type: 'userid'
        };
    });

    function myFuckingAutoSearch(request, response) {
        function hasMatch(s) {
            return s.toLowerCase().indexOf(request.term.toLowerCase()) !== -1;
        }
        var i, l, obj, matches = [];

        if (request.term === "") {
            response([]);
            return;
        }

        for (i = 0, l = mentionList.length; i < l; i++) {
            obj = mentionList[i];
            if (hasMatch(obj.value) || hasMatch(obj.email)) {
                matches.push(obj);
            }
        }
        response(matches);
    }

    $chatTextarea.mentionsInput({
        source: myFuckingAutoSearch
    });

    $chatTextarea.bind('keydown', function (event) {

        var key = event.key ? event.key : event.keyIdentifier;
        //console.log(event);
        console.log('autocomplete', $('.ui-autocomplete').is(':visible'));

        if (key == 'Enter' && event.shiftKey == false && !$('.ui-autocomplete').is(':visible')) {
            //console.log('enter');
            event.preventDefault();
            closeSearchResult(roomId);
            getTextToSend($chatTextarea, $chatHistory, roomId, event.timeStamp, true);
        }
    });

    $chatTextarea.bind('keyup', function (event) {
        var value = $.trim($chatTextarea.val());
        //console.log(value);
        toggleSendButton(roomId, value);
    });

    $('#btSendLike' + roomId).click(function (event) {
        if (!$(this).prop('disabled')) {
            closeSearchResult(roomId);
            var rawMessage = '👍';
            parseRoomLogBeforeSend($chatHistory, currentUserId, roomId, rawMessage, true);
        }
    });

    $('#btSendText' + roomId).click(function (event) {
        if (!$(this).prop('disabled')) {
            closeSearchResult(roomId);
            getTextToSend($chatTextarea, $chatHistory, roomId, event.timeStamp, true);
        }
    });
    $chatTextarea.bind('click', function () {
        if (!$(this).prop('disabled')) {
            //console.log('click');
            setChatIsViewed(roomId);
        }
    });

    $('#fileAttach' + roomId).bind('change', function () {
        if (!$(this).prop('disabled')) {
            var files = this.files;
            closeSearchResult(roomId);
            handleFileUpload(files, roomId, $chatHistory, true);
        }
    });

    $('#fileAttach' + roomId).bind('click', function (e) {
        $('#fileAttach' + roomId).val("");
        setChatIsViewed(roomId);
    });

    $('#btUploadFile' + roomId).click(function (data) {
        if (!$(this).prop('disabled')) {
            $('#fileAttach' + roomId).click();
            setChatIsViewed(roomId);
        }
    });

    $('#btAddPlan' + roomId).click(function (data) {
        if (!$(this).prop('disabled')) {
            $('#btSendPlan').data('room-id', roomId);
            clearAddPlanModal();
            $('#addPlanModal').showModal();
            $("#tbAPDatetime").datetimepicker({
                format: "yyyy-mm-dd hh:ii",
                autoclose: true,
                todayBtn: true,
                pickerPosition: "bottom-left",
                startDate: moment().toDate()
            });
            setChatIsViewed(roomId);
        }
    });

    $('#divEmojiWrapper' + roomId).load('/emoji/', function () {
        //$('#divEmojiWrapper' + roomId).html(replaceEmoji($('#divEmojiWrapper' + roomId).html()));
        //$('#divEmojiWrapper' + roomId).find('.emoji-list').each(function (e) {
        //    //$(this).html(replaceEmoji($(this).html()));
        //});
        $('#divEmojiWrapper' + roomId).find('.emoji-tabs > li').click(function () {
            var tabName = $(this).data('href');
            $('#divEmojiWrapper' + roomId).find('.emoji-tabs > li').removeClass('selected');
            $(this).addClass('selected');
            $('#divEmojiWrapper' + roomId).find('.emoji-tab-content > li').hide();
            $('#divEmojiWrapper' + roomId).find('.emoji-tab-content > li[data-target=' + tabName + ']').show();
        });
        $('#divEmojiWrapper' + roomId).find('ul.emoji-list li > a').click(function (e) {
            e.preventDefault();
            //var emoji = $(this).data('alt');
            var emoji = $(this).find('.emojis__list__native').html();
            //console.log('before: ' + $chatTextarea.prop('selectionStart'));
            insertAtCursor($chatTextarea[0], emoji);
            //console.log($chatTextarea.prop('selectionStart'));

            toggleSendButton(roomId, $chatTextarea.val());
        });
    });
    $('#btEmoji' + roomId).click(function (data) {
        if (!$(this).prop('disabled')) {
            $('#divEmojiWrapper' + roomId).toggle();
        }
    });

    var timeoutSearchMember;
    //extent search
    $('#tbmemberPane' + roomId).bind('keyup', function (event) {
        var key = event.key ? event.key : event.keyIdentifier;
        //console.log(event);
        //if (key == 'Enter' && event.shiftKey == false) {
        var keyword = $('#tbmemberPane' + roomId).val();
        updateExtentPaneTitle(roomId, 'member', keyword);
        clearTimeout(timeoutSearchMember);
        timeoutSearchMember = setTimeout(function () { searchRoomMember(roomId, keyword); }, 200);
        //}

        if ($('#tbmemberPane' + roomId).val() == '') {
            $('#btClearmemberPane' + roomId).hide();
        }
        else {
            $('#btClearmemberPane' + roomId).show();
        }
    });

    $('#btClearmemberPane' + roomId).bind('click', function (event) {
        $(this).hide();
        var keyword = '';
        $('#tbmemberPane' + roomId).val(keyword);
        updateExtentPaneTitle(roomId, 'member', keyword);
        searchRoomMember(roomId, keyword);
    });

    var timeoutSearchFile;
    $('#tbfilePane' + roomId).bind('keyup', function (event) {
        var key = event.key ? event.key : event.keyIdentifier;
        //console.log(event);
        //if (key == 'Enter' && event.shiftKey == false) {
        var keyword = $('#tbfilePane' + roomId).val();
        clearRoomMedias(messageType.file, roomId);
        updateExtentPaneTitle(roomId, messageType.file, keyword);
        clearTimeout(timeoutSearchFile);
        timeoutSearchFile = setTimeout(function () { getRoomExtentItemList(messageType.file, roomId, null, keyword); }, 400);
        //}

        if ($('#tbfilePane' + roomId).val() == '') {
            $('#btClearfilePane' + roomId).hide();
        }
        else {
            $('#btClearfilePane' + roomId).show();
        }
    });

    $('#btClearfilePane' + roomId).bind('click', function (event) {
        $('#btClearfilePane' + roomId).hide();
        var keyword = '';
        $('#tbfilePane' + roomId).val(keyword);
        clearRoomMedias(messageType.file, roomId);
        updateExtentPaneTitle(roomId, messageType.file, keyword);
        getRoomExtentItemList(messageType.file, roomId, null, keyword);
    });

    var timeoutSearchLink;
    $('#tblinkPane' + roomId).bind('keyup', function (event) {
        var key = event.key ? event.key : event.keyIdentifier;
        //console.log(event);
        //if (key == 'Enter' && event.shiftKey == false) {
        var keyword = $('#tblinkPane' + roomId).val();
        clearRoomMedias(messageType.link, roomId);
        updateExtentPaneTitle(roomId, messageType.link, keyword);
        clearTimeout(timeoutSearchLink);
        timeoutSearchLink = setTimeout(function () { getRoomExtentItemList(messageType.link, roomId, null, keyword); }, 400);
        //}
        if ($('#tblinkPane' + roomId).val() == '') {
            $('#btClearlinkPane' + roomId).hide();
        }
        else {
            $('#btClearlinkPane' + roomId).show();
        }
    });

    $('#btClearlinkPane' + roomId).bind('click', function (event) {
        $('#btClearlinkPane' + roomId).hide();
        var keyword = '';
        $('#tblinkPane' + roomId).val(keyword);
        clearRoomMedias(messageType.link, roomId);
        updateExtentPaneTitle(roomId, messageType.link, keyword);
        getRoomExtentItemList(messageType.link, roomId, null, keyword);
    });

    var timeoutSearchChat;
    $('#tbsearchResultPane' + roomId).bind('keyup', function (event) {
        var key = event.key ? event.key : event.keyIdentifier;
        //console.log(event);
        //if (key == 'Enter' && event.shiftKey == false) {
        //console.log('enter');
        var keyword = $('#tbsearchResultPane' + roomId).val();
        clearRoomSearchResult(roomId);
        updateExtentPaneTitle(roomId, 'search', keyword);
        clearTimeout(timeoutSearchChat);
        timeoutSearchChat = setTimeout(function () {
            if (keyword != '') {
                getRoomExtentItemList('search', roomId, null, keyword);
            }
        }, 400);
        //}

        if ($('#tbsearchResultPane' + roomId).val() == '') {
            $('#btClearsearchResultPane' + roomId).hide();
        }
        else {
            $('#btClearsearchResultPane' + roomId).show();
        }
    });

    $('#btClearsearchResultPane' + roomId).bind('click', function (event) {
        $('#btClearsearchResultPane' + roomId).hide();
        var keyword = '';
        $('#tbsearchResultPane' + roomId).val(keyword);
        updateExtentPaneTitle(roomId, 'search', keyword);
        clearRoomSearchResult(roomId);
    });

    //scroll down button
    $('#btScrollToBottom' + roomId).click(function () {
        var $chatHistory = $('.chat-history.chat-logs[data-id=' + roomId + ']');
        $chatHistory.scrollTop($chatHistory.prop('scrollHeight') - $chatHistory.prop('clientHeight'));
    });

    generateMagnificPopup($chatHistory);

    //reply message
    $('#btCloseQuote' + roomId).click(function () {
        closeReplyPopup(roomId);
    });
}
function searchRoomMember(roomId, keyword) {
    var $chatMemberPane = getMemberPane(roomId);
    if (keyword) {
        $chatMemberPane.find('ul li').hide();
        $chatMemberPane.find('ul li.member').filter(function (index) {
            var itemName = $(this).find('.item-name');
            return searchInString(itemName.html(), keyword) || searchInString(itemName.data('mobile'), keyword);
        }).show();
    }
    else {
        $chatMemberPane.find('ul li').show();
    }
}
//extent-pane
function generateExtentPane(roomId, className, headerName, searchPlaceHoder, noResultMessage, isShow, showSearchPane, useUL) {
    var html = '';
    html += '<div data-id="' + roomId + '" class="extend-pane ' + className + (isShow ? '' : ' hide') + '">'; //begin memberPane
    //html += '<div class="header"> ' + headerName; //begin memberPane header
    ////html += '<a class="tool-button btCloseExPane"><span class="fa fa-remove"></span></a>';
    //html += '</div>';//end memberPane header 
    html += '<div class="pane-content">'; //begin memberPane pane-content
    if (showSearchPane) {
        html += '<div class="search-pane">'; //begin memberPane search-content
        html += '<input id="tb' + className + roomId + '" type="text" placeholder= "' + searchPlaceHoder + '" />';
        html += '<i class="fa fa-search search-label"></i>';
        html += '<i class="fa fa-remove btClear" id="btClear' + className + roomId + '"></i>';
        html += '</div>';//end memberPane search-content
    }
    html += '<div class="scroll-content">';  //begin memberPane scroll-content
    if (useUL) {
        html += '<ul class="item-list"></ul>';
    }
    html += '<div class="no-result hide"><div class="default">' + noResultMessage + '</div><div class="custom"></div></div>';
    html += '</div>';//end memberPane scroll-content
    html += '</div>';//end memberPane pane-content
    html += '</div>';//end memberPane
    return html;
}

//function test() {
//    socket.emit('getChannelAdminRoom', {
//        userIdGuest: 165199,
//        channelId: '5adf005bdf57c21a00087383'
//    }, function(res) {
//        console.log(res);
//    });
//}
//$(document).ready(function () {
//    setTimeout(function () {
//        test();
//    }, 2000);
//});

