﻿moment.locale('vi');
function formatTime(dateTime) {
    return moment(dateTime).format('HH:mm');
}
function formatDateTime(dateTime) {
    var html = '';    
    var day = moment().utc().startOf('day').diff(moment(dateTime).startOf('day'), 'days');
    if (day == 0) {
        html = 'Hôm nay, ' + moment(dateTime).format('HH:mm') ;
    }
    else if (day == 1) {
        html = 'Hôm qua, ' + moment(dateTime).format('HH:mm');
    }
    else if (day < 7) {
        html = moment(dateTime).format('dddd, HH:mm');
    }
    else {
        html = moment(dateTime).format('DD/MM, HH:mm');
    }
    return html;
}

function formatDateTimeShort(dateTime) {
    var html = '';
    var day = moment().utc().startOf('day').diff(moment(dateTime).startOf('day'), 'days');
    if (day == 0) {
        html = moment(dateTime).format('HH:mm');
    }
    else if (day == 1) {
        html = 'Hôm qua';
    }
    else if (day < 7) {
        html = moment(dateTime).format('dddd');
    }
    else {
        html = moment(dateTime).format('DD-MM');
    }
    return html;
}

function formatFullDateTime(dateTime) {
    var html = '';
    var day = moment().utc().startOf('day').diff(moment(dateTime).startOf('day'), 'days');
    if (day == 0) {
        html = 'Hôm nay, ' + moment(dateTime).format('DD MMMM, YYYY');
    }
    else if (day == 1) {
        html = 'Hôm qua, ' + moment(dateTime).format('DD MMMM, YYYY');
    }    
    else {
        html = moment(dateTime).format('dddd, DD MMMM, YYYY');
    }
    return html;    
}