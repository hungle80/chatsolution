﻿var express = require('express');
var router = express.Router();
var PHPUnserialize = require('php-unserialize');
var debug = true;

var scriptDebug = [
    '/javascripts/jquery-3.2.1.min.js',
    '/javascripts/socket.io.js',
    '/javascripts/moment-with-locales.min.js',
    '/javascripts/pnotify.custom.min.js',
    '/javascripts/textParseHelper.js',
    '/javascripts/dateTimeHelper.js',
    '/javascripts/constant.js',
    '/javascripts/notifyHelper.js',
    '/javascripts/join.js'
];
var scriptRelease = ['/js/join.js'];
var appDeepLinkJoin = 'mbnappchat://chat/room?type=join&link=';
function getMobileOperatingSystem(userAgent) {
    //var userAgent = navigator.userAgent || navigator.vendor || window.opera;

    // Windows Phone must come first because its UA also contains "Android"
    if (/windows phone/i.test(userAgent)) {
        return "Windows Phone";
    }

    if (/android/i.test(userAgent)) {
        return "Android";
    }

    // iOS detection from: http://stackoverflow.com/a/9039885/177710
    if (/iPad|iPhone|iPod/.test(userAgent)) {
        return "iOS";
    }

    return "unknown";
}
/* GET users listing. */
router.get('/:link', function (req, res) {
    var joinLink = '';
    if (req.params.link) {
        joinLink = req.params.link;
    }

    //var OS = getMobileOperatingSystem(req.headers['user-agent']);
    //if (OS == 'Android' || OS == 'iOS') {
    //    console.log('is mobile');
    //    console.log(appDeepLinkJoin + joinLink);
    //    res.redirect(301, appDeepLinkJoin + joinLink);
    //}
    //else {

        var userInfo;
        if (req.cookies['USER']) {
            var mbnUserInfo = PHPUnserialize.unserialize(req.cookies['USER']);
            if (mbnUserInfo) {
                userInfo = {
                    userId: mbnUserInfo.id,
                    mobile: mbnUserInfo.phone_number
                };
                //token = PHPUnserialize.unserialize(req.cookies['USER_MBN_TOKEN']);
            }
        }
        else if (req.hostname == 'localhost') {
            userInfo = {
                userId: 112567,
                //userId: 165199,
                mobile: "0943282606"
            };
        }
        var loginUrl = "https://muabannhanh.com/auth/login?redirectUrl=" + encodeURIComponent(req.protocol + '://' + req.get('host') + req.originalUrl);
        var registerUrl = "https://thanhvien.muabannhanh.com/user/register?token=b554afdb94eced9878eccf0be562e0f9&redirectUrl=" + encodeURIComponent(req.protocol + '://' + req.get('host') + req.originalUrl);
        res.render('join', { scripts: (debug ? scriptDebug : scriptRelease), title: 'ChatNhanh', info: userInfo, loginUrl: loginUrl, registerUrl: registerUrl, joinLink: joinLink });
    //}
});

module.exports = router;