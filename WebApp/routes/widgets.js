﻿var express = require('express');
var router = express.Router();
var http = require('http');
var PHPUnserialize = require('php-unserialize');

var debug = true;
var scriptDebug = [
    '/javascripts/jquery-3.2.1.min.js',
    '/javascripts/socket.io.js',
    '/javascripts/moment-with-locales.min.js',
    '/javascripts/pnotify.custom.min.js',
    '/javascripts/textParseHelper.js',
    '/javascripts/dateTimeHelper.js',
    '/javascripts/url.js',
    '/javascripts/myLinkPreview.js',
    '/javascripts/jquery.percentageloader-0.1.js',
    '/javascripts/emojione.js',
    '/javascripts/jquery.magnific-popup.js',
    '/javascripts/modal.js',
    '/javascripts/cropper.js',
    '/javascripts/imagePaste.js',
    '/javascripts/imageHelper.js',
    '/javascripts/notifyHelper.js',
    '/javascripts/messageHelper.js',
    '/javascripts/constant.js',
    '/javascripts/widgets.js'
];
var scriptRelease = ['/js/widgets.js'];

/* GET users listing. */
router.get('/', function (req, res) {
    console.log(req.get('Referrer'));
    //console.log(req.params);
    //console.log(req.cookies);
    var referrer = req.get('Referrer');
    console.log(referrer);
    var redirectUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
    var userInfo;
    var token;
    if (req.cookies['USER']) {
        var mbnUserInfo = PHPUnserialize.unserialize(req.cookies['USER']);
        if (mbnUserInfo) {
            userInfo = {
                userId: mbnUserInfo.id,
                mobile: mbnUserInfo.phone_number
            };
            //token = PHPUnserialize.unserialize(req.cookies['USER_MBN_TOKEN']);
        }
    }
    else if (req.hostname == 'localhost') {
        userInfo = {
            //userId: 112567,
            userId: 165199,
            mobile: "0943282606"
        };
    }

    res.render('widgets', { title: 'ChatNhanh', info: userInfo, referrer: referrer, redirectUrl: redirectUrl });
});

module.exports = router;