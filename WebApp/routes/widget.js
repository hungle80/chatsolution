﻿var express = require('express');
var router = express.Router();
var http = require('http');
var PHPUnserialize = require('php-unserialize');

var debug = false;

/* GET users listing. */
router.get('/', function (req, res) {    
    var userInfo;
    var token;
    if (req.cookies['USER']) {
        var mbnUserInfo = PHPUnserialize.unserialize(req.cookies['USER']);
        if (mbnUserInfo) {
            userInfo = {
                userId: mbnUserInfo.id,
                mobile: mbnUserInfo.phone_number
            };
            //token = PHPUnserialize.unserialize(req.cookies['USER_MBN_TOKEN']);
        }
    }
    else if (debug) {
        userInfo = {
            userId: 165199,
            mobile: "0943282606"
        };
    }
        
    res.render('widget', { title: 'ChatNhanh', info: userInfo});
});

module.exports = router;