﻿var express = require('express');
var router = express.Router();
var http = require('http');
var PHPUnserialize = require('php-unserialize');

var debug = false;

function decodeURITryCatch(str) {
    var decode;
    try {
        decode = decodeURIComponent(str);
    }
    catch (ex) {
        decode = str;
    }
    return decode;
}

var scriptDebug = [
    '/javascripts/jquery-3.2.1.min.js',
    '/javascripts/popper.min.js',
    '/javascripts/socket.io.js',
    '/javascripts/moment-with-locales.min.js',
    '/javascripts/pnotify.custom.min.js',
    '/javascripts/textParseHelper.js',
    '/javascripts/dateTimeHelper.js',
    '/javascripts/url.js',
    '/javascripts/myLinkPreview.js',
    '/javascripts/jquery.percentageloader-0.1.js',
    '/javascripts/imageHelper.js',
    '/javascripts/modal.js',
    '/javascripts/emojione.js',
    '/javascripts/jquery.mark.min.js',
    '/javascripts/jquery.magnific-popup.js',
    '/javascripts/cropper.js',
    '/javascripts/imagePaste.js',
    '/javascripts/jquery.jeditable.js',
    '/javascripts/notifyHelper.js',
    '/javascripts/messageHelper.js',
    '/javascripts/js.cookie.js',
    '/javascripts/constant.js',
    '/javascripts/favico.js',
    '/javascripts/select2.full.js',
    '/javascripts/index.js',
    '/javascripts/channel/createChannel.js',
    '/javascripts/channel/updateChannel.js',
    '/javascripts/channel/channelAdministrator.js',
    '/javascripts/channel/channelBanUser.js',
    '/javascripts/group/updateGroup.js',
    '/javascripts/contact/contact.js'
];
var scriptRelease = ['/js/index.js'];


/* GET home page. */
router.get('/', function (req, res) {    
    var userId;
    var token;
    
    if (req.cookies['_tn']) {
        token = req.cookies['_tn'];
    }

    if (req.cookies['_u']) {
        userId = req.cookies['_u'];
    }

    //if (req.cookies['USER']) {
    //    var mbnUserInfo = PHPUnserialize.unserialize(req.cookies['USER']);
    //    if (mbnUserInfo) {
    //        userInfo = {
    //            userId: mbnUserInfo.id,
    //            mobile: mbnUserInfo.phone_number
    //        };
    //        //token = PHPUnserialize.unserialize(req.cookies['USER_MBN_TOKEN']);
    //    }
    //}
    //else if (req.hostname == 'localhost') {
    //    userInfo = {
    //        //userId: 112567,
    //        userId: 165199,
    //        mobile: "0943282606"
    //        //userId: 133609,
    //        //mobile: "01649770150"
    //        //userId: 139367,
    //        //mobile: "0903019070"
    //        //userId: 217345,
    //        //mobile: "01662342558",
    //        //userId: 158852,
    //        //mobile: '0938936128'
    //    };
    //}

    //var createRoomInfo = { type: '' };
    //if (req.query && req.query.type) {
    //    createRoomInfo.type = req.query.type;
    //    switch (createRoomInfo.type) {
    //        case 'private': {
    //            createRoomInfo.userId = req.query.userId;
    //            if (req.query.createTextMessage) {
    //                createRoomInfo.createTextMessage = req.query.createTextMessage;
    //                createRoomInfo.text = decodeURITryCatch(req.query.text);
    //            }
    //            break;
    //        }
    //        case 'item': {
    //            createRoomInfo.ownerId = req.query.ownerId;
    //            createRoomInfo.itemId = req.query.itemId;
    //            createRoomInfo.itemName = decodeURITryCatch(req.query.itemName);
    //            createRoomInfo.itemImage = decodeURITryCatch(req.query.itemImage);
    //            createRoomInfo.itemLink = decodeURITryCatch(req.query.itemLink);
    //            createRoomInfo.itemPrice = req.query.itemPrice;
    //            break;
    //        }
    //        case 'page': {
    //            createRoomInfo.pageId = req.query.pageId;
    //            createRoomInfo.pageName = decodeURITryCatch(req.query.pageName);
    //            createRoomInfo.pageLink = decodeURITryCatch(req.query.pageLink);
    //            createRoomInfo.pageImage = decodeURITryCatch(req.query.pageImage);
    //            if (req.query.createItemMessage) {
    //                createRoomInfo.createItemMessage = req.query.createItemMessage;
    //                createRoomInfo.itemId = req.query.itemId;
    //                createRoomInfo.itemName = decodeURITryCatch(req.query.itemName);
    //                createRoomInfo.itemImage = decodeURITryCatch(req.query.itemImage);
    //                createRoomInfo.itemLink = decodeURITryCatch(req.query.itemLink);
    //                createRoomInfo.itemPrice = req.query.itemPrice;
    //            }
    //            break;
    //        }
    //    }        
    //}

    //var loginUrl = "https://muabannhanh.com/auth/login?redirectUrl=" + encodeURIComponent(req.protocol + '://' + req.get('host') + req.originalUrl);
    //var registerUrl = "https://thanhvien.muabannhanh.com/user/register?token=b554afdb94eced9878eccf0be562e0f9&redirectUrl=" + encodeURIComponent(req.protocol + '://' + req.get('host') + req.originalUrl);
    //res.render('index', { scripts: (debug ? scriptDebug : scriptRelease), title: 'ChatNhanh', info: userInfo, loginUrl: loginUrl, registerUrl: registerUrl, createRoomInfo: createRoomInfo });
    res.render('index', { scripts: (debug ? scriptDebug : scriptRelease), title: 'ChatNhanh', token: token, userId: userId });
});

module.exports = router;