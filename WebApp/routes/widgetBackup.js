﻿var express = require('express');
var router = express.Router();
var http = require('http');
var PHPUnserialize = require('php-unserialize');

/* GET users listing. */
router.get('/', function (req, res) {    
    console.log(req.get('Referrer'));
    //console.log(req.params);
    //console.log(req.cookies);
    var referrer = req.get('Referrer');
    console.log(referrer);
    var model;
    if (referrer.startsWith('https://localhost') || referrer.startsWith('http://localhost')) {
        model = {
            userId: 165199,
            mobile: '0943282606',
            type: 'private',
            chatWithId: 133609,
            chatWithName: 'Huy Triệu',
            chatWithAvatar: 'https://cdn.muabannhanh.com/asset/frontend/img/avatar/2017/01/26/5888db48813dd_1485364040.jpg',
            chatWithMobile: '01649770150'
        };
    }
    else {       

        if (req.cookies['USER']) {
            var userInfo = PHPUnserialize.unserialize(req.cookies['USER']);
            if (userInfo) {
                var token = PHPUnserialize.unserialize(req.cookies['USER_MBN_TOKEN']);
                console.log(userInfo);                

                var type = req.query.type;
                model = { type: type, userId: userInfo.id, token: token, mobile: userInfo.phone_number };
                switch (type) {
                    case 'private': {
                        model.chatWithId = req.query.chatWithId;
                        model.chatWithName = req.query.chatWithName;
                        model.chatWithAvatar = req.query.chatWithAvatar;
                        model.chatWithMobile = req.query.chatWithMobile;
                        break;
                    }
                    case 'item': {
                        model.ownerId = req.query.ownerId;
                        model.ownerName = req.query.ownerName;
                        model.ownerAvatar = req.query.ownerAvatar;
                        model.itemId = req.query.itemId;
                        model.itemName = req.query.itemName;
                        model.itemImage = req.query.itemImage;
                        model.itemLink = req.query.itemLink;
                        model.itemPrice = req.query.itemPrice;
                        break;
                    }
                    case 'page': {
                        model.pageMembers = req.query.pageMembers;
                        model.pageId = req.query.pageId;
                        model.pageMobile = req.query.pageMobile;
                        model.pageName = req.query.pageName;
                        model.pageImage = req.query.pageImage;
                        model.pageLink = req.query.pageLink;
                        break;
                    }
                }
                console.log(model);
            }
            else {
                model = null;
            }
        }
        else {
            model = null;
        }
    }
    res.render('widget', { title: 'ChatNhanh', info: model, referrer: referrer });
});

module.exports = router;