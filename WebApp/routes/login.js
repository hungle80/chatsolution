﻿var express = require('express');
var router = express.Router();
var cryptoHelper = require('../utility/cryptoHelper');
var accountKitHelper = require('../utility/accountKitHelper');
var unirest = require('unirest');
var restAPI = 'http://localhost:3002/';
router.get('/', function (req, res) {

    res.render('login', { title: 'ChatNhanh' });
});

router.get('/complete', function (req, res) {

    res.render('login/registerSuccess', { title: 'ChatNhanh' });
});
router.get('/pass', function (req, res) {
    var phone = req.cookies['_m'];
    var _id = req.cookies['_u'];
    var token = cryptoHelper.encrypt(phone + '|' + _id);
    res.render('login/passwordConfirm', { title: 'ChatNhanh', token: token });
});
router.get('/fba', function (req, res) {
    var code = req.query.code;
    var csrf = req.query.csrf;
    console.log(code);
    console.log(csrf);
    var dec = cryptoHelper.decrypt(csrf).split('|');
    var phone = req.cookies['_m'];
    var _id = req.cookies['_u'];
    if (dec[0] == phone || dec[1] == _id) {
        console.log(_id);
        //xử lý token đăng nhập, ghi cookie

        accountKitHelper.queryAccessTokenData(code, (result) => {

            if (result.errorCode == 0) {
                if (phone.includes(result.data)) {
                    unirest.post(restAPI + 'fbaLogin').form({ mobile: phone, _id: _id }).end((response) => {
                        if (response.status == 200) {
                            var resX = response.body;
                            console.log(resX);
                            if (resX.errorCode == 0) {
                                var data = resX.data;
                                //Cookies.set('_tn', data.token);
                                //Cookies.set('_u', data._id);
                                //res.cookie('_tn', data.token, { maxAge: 900000, httpOnly: true });
                                //res.cookie('_u', data._id, { maxAge: 900000, httpOnly: true });
                                res.render('login/fbs', { user: data });
                            }
                            else {
                                res.render('login/fbf', { result: resX });
                            }
                        }
                        else {
                            res.render('login/fbf', { result: { errorCode: 109, error: 'Kết nối server bị lỗi!' } });
                        }
                    });
                }
                else {
                    res.render('login/wrongUser', {});
                }
            }
            else {
                res.render('login/fbf', { result: result });
            }
        });
    }
    else {
        res.render('login/wrongUser', {});
    }
});

module.exports = router;