﻿//== Class Definition
var SnippetLogin = function () {

    var restUrl = 'http://localhost:3000/';

    var login = $('#m_login');

    var showErrorMsg = function (form, type, msg) {
        var alert = $('<div class="m-alert m-alert--outline alert alert-' + type + ' alert-dismissible" role="alert">\
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>\
			<span>'+ msg + '</span>\
		</div>');

        form.find('.alert').remove();
        alert.prependTo(form);
        alert.animateClass('fadeIn animated');

    }

    //== Private Functions

    var displaySignUpForm = function () {
        login.removeClass('m-login--forget-password');
        login.removeClass('m-login--signin');

        login.addClass('m-login--signup');
        login.find('.m-login__signup').animateClass('flipInX animated');
    }

    var displaySignInForm = function () {
        login.removeClass('m-login--forget-password');
        login.removeClass('m-login--signup');

        login.addClass('m-login--signin');
        login.find('.m-login__signin').animateClass('flipInX animated');
    }

    var displayForgetPasswordForm = function () {
        login.removeClass('m-login--signin');
        login.removeClass('m-login--signup');

        login.addClass('m-login--forget-password');
        login.find('.m-login__forget-password').animateClass('flipInX animated');
    }

    var handleFormSwitch = function () {
        $('#m_login_forget_password').click(function (e) {
            e.preventDefault();
            displayForgetPasswordForm();
        });

        $('#m_login_forget_password_cancel').click(function (e) {
            e.preventDefault();
            displaySignInForm();
        });

        $('#m_login_signup').click(function (e) {
            e.preventDefault();
            displaySignUpForm();
        });

        $('#m_login_signup_cancel').click(function (e) {
            e.preventDefault();
            displaySignInForm();
        });
    }

    var handleSignInFormSubmit = function () {
        $('#m_login_signin_submit').click(function (e) {
            console.log('signin');
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');

            form.validate({
                rules: {
                    email: {
                        required: true,
                        email: true
                    },
                    password: {
                        required: true
                    }
                },
                messages: {
                    email: "Cần nhập email",
                    password: "Cần nhập mật khẩu"
                }
            });

            if (!form.valid()) {
                return;
            }

            btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);
            var email = form.find('[name=email]').val();
            var pass = form.find('[name=password]').val();
            var requestData = { username: email, password: pass };
            console.log(requestData);
            $.ajax({
                url: restUrl + 'login',
                data: JSON.stringify(requestData),
                type: 'POST',
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                error: function (xhr) {
                    btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                    showErrorMsg(form, 'danger', 'Đã có lỗi xảy ra khi đăng nhập');
                },
                success: function (res) {
                    btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                    if (res.errorCode == 0) {
                        var data = res.data;
                        Cookies.set('_tn', data.token);
                        Cookies.set('_u', data._id);
                        window.location.href = '/';
                    }
                    else {
                        showErrorMsg(form, 'danger', res.error);
                    }
                    
                }
            });

            //form.ajaxSubmit({
            //     url: restUrl + 'login',
            //    success: function (response, status, xhr, $form) {
            //        console.log(response);
            //        // similate 2s delay
            //        setTimeout(function () {
            //            btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
            //            showErrorMsg(form, 'danger', 'Incorrect username or password. Please try again.');
            //        }, 2000);
            //    }
            //});
        });
    }

    var handleSignUpFormSubmit = function () {
        $('#m_login_signup_submit').click(function (e) {
            e.preventDefault();

            var btn = $(this);
            var form = $(this).closest('form');

            form.validate({
                rules: {
                    fullname: {
                        required: true
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    password: {
                        required: true
                    },
                    rpassword: {
                        required: true
                    },
                    agree: {
                        required: true
                    }
                },
                messages: {
                    fullname: "Cần nhập họ tên",
                    email: "Cần nhập email",
                    password: "Cần nhập mật khẩu",
                    rpassword: "Cần nhập xác nhận mật khẩu",
                    agree: "Cần check đồng ý"
                }
            });


            if (!form.valid()) {
                return;
            }

            var email = form.find('[name=email]').val();
            var pass = form.find('[name=password]').val();
            var rpass = form.find('[name=rpassword]').val();
            var fullname = form.find('[name=fullname]').val();

            if (pass != rpass) {
                showErrorMsg(form, 'danger', 'Xác nhận lại mật khẩu không trùng khớp.');
                return;
            }
            
            btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);
            var requestData = { username: email, password: pass, fullname: fullname };
            console.log(requestData);

            $.ajax({
                url: restUrl + 'signup',
                data: JSON.stringify(requestData),
                type: 'POST',
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                error: function (xhr) {
                    btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                    showErrorMsg(form, 'danger', 'Đã có lỗi xảy ra khi đăng kí');
                },
                success: function (res) {
                    btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                    if (res.errorCode == 0) {
                        var data = res.data;
                        Cookies.set('_tn', data.token);
                        Cookies.set('_u', data._id);
                        window.location.href = '/';
                    }
                    else {
                        showErrorMsg(form, 'danger', res.error);
                    }

                }
            });
            //form.ajaxSubmit({
            //    url: '',
            //    success: function (response, status, xhr, $form) {
            //        // similate 2s delay
            //        setTimeout(function () {
            //            btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
            //            form.clearForm();
            //            form.validate().resetForm();

            //            // display signup form
            //            displaySignInForm();
            //            var signInForm = login.find('.m-login__signin form');
            //            signInForm.clearForm();
            //            signInForm.validate().resetForm();

            //            showErrorMsg(signInForm, 'success', 'Thank you. To complete your registration please check your email.');
            //        }, 2000);
            //    }
            //});
        });
    }

    var handleForgetPasswordFormSubmit = function () {
        $('#m_login_forget_password_submit').click(function (e) {
            e.preventDefault();

            var btn = $(this);
            var form = $(this).closest('form');

            form.validate({
                rules: {
                    email: {
                        required: true,
                        email: true
                    }
                }
            });

            if (!form.valid()) {
                return;
            }

            btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);

            form.ajaxSubmit({
                url: '',
                success: function (response, status, xhr, $form) {
                    // similate 2s delay
                    setTimeout(function () {
                        btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false); // remove 
                        form.clearForm(); // clear form
                        form.validate().resetForm(); // reset validation states

                        // display signup form
                        displaySignInForm();
                        var signInForm = login.find('.m-login__signin form');
                        signInForm.clearForm();
                        signInForm.validate().resetForm();

                        showErrorMsg(signInForm, 'success', 'Cool! Password recovery instruction has been sent to your email.');
                    }, 2000);
                }
            });
        });
    }

    var bgAnimate = function () {
        // Some random colors
        const colors = ["#3CC157", "#2AA7FF", "#1B1B1B", "#FCBC0F", "#F85F36"];

        const numBalls = 13;
        const balls = [];
        var ballWrapper = document.getElementById('balls');
        for (let i = 0; i < numBalls; i++) {
            let ball = document.createElement("div");
            ball.classList.add("ball");
            ball.style.background = colors[Math.floor(Math.random() * colors.length)];
            ball.style.left = `${Math.floor(Math.random() * 100)}vw`;
            ball.style.top = `${Math.floor(Math.random() * 100)}vh`;
            ball.style.transform = `scale(${Math.random()})`;
            ball.style.width = `${Math.random()}em`;
            ball.style.height = ball.style.width;

            balls.push(ball);
            ballWrapper.append(ball);
        }

        // Keyframes
        balls.forEach((el, i, ra) => {
            let to = {
                x: Math.random() * (i % 2 === 0 ? -11 : 11),
                y: Math.random() * 12
            };

            let anim = el.animate(
                [
                    { transform: "translate(0, 0)" },
                    { transform: `translate(${to.x}rem, ${to.y}rem)` }
                ],
                {
                    duration: (Math.random() + 1) * 2000, // random duration
                    direction: "alternate",
                    fill: "both",
                    iterations: Infinity,
                    easing: "ease-in-out"
                }
            );
        });

    }

    //== Public Functions
    return {
        // public functions
        init: function () {
            bgAnimate();
            handleFormSwitch();
            handleSignInFormSubmit();
            handleSignUpFormSubmit();
            handleForgetPasswordFormSubmit();
        }
    };
}();

//== Class Initialization
jQuery(document).ready(function () {
    SnippetLogin.init();
    bgAnimate();
});