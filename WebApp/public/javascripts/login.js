﻿//== Class Definition
var SnippetLogin = function () {

    var restUrl = 'http://localhost:3002/';

    jQuery.validator.addMethod('mobileVN', function (phone_number, element) {
        phone_number = phone_number.replace(/\s+|-/g, '');
        return this.optional(element) || phone_number.length > 9 &&
            phone_number.match(/^((09|03|07|08|05)+([0-9]{8})\b)$/);
    }, 'Số mobile không đúng định dạng');

    var login = $('#m_login');

    var showErrorMsg = function (form, type, msg) {
        var alert = $('<div class="m-alert m-alert--outline alert alert-' + type + ' alert-dismissible" role="alert">\
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>\
			<span>'+ msg + '</span>\
		</div>');

        form.find('.alert').remove();
        alert.prependTo(form);
        alert.animateClass('fadeIn animated');

    }

    //== Private Functions
    var handleSignInFormSubmit = function () {
        $('#m_login_signin_submit').click(function (e) {
            console.log('signin');
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');

            form.validate({
                rules: {
                    mobile: {
                        required: true,
                        mobileVN: true
                    }
                },
                messages: {
                    mobile: {
                        required: "Cần nhập mobile"
                    }
                }
            });

            if (!form.valid()) {
                return;
            }

            btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);
            var mobile = form.find('[name=mobile]').val();
            mobile = mobile.replace(/\s+|-/g, '');

            var requestData = { mobile: mobile };
            console.log(requestData);
            $.ajax({
                url: restUrl + 'mobileLogin',
                data: JSON.stringify(requestData),
                type: 'POST',
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                error: function (xhr) {
                    btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                    showErrorMsg(form, 'danger', 'Đã có lỗi xảy ra khi đăng nhập');
                },
                success: function (res) {
                    btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                    console.log(res);
                    if (res.errorCode == 0) {
                        localforage.setItem('user', res.data);
                        //var data = res.data;
                        Cookies.set('_m', res.data.phone);
                        Cookies.set('_u', res.data._id);
                        if (res.data.password) {
                            //window.location.href = '/';
                            window.location.href = '/login/pass';
                        }
                        else {
                            window.location.href = '/login/complete';
                        }
                    }
                    else if (res.errorCode == 1) {
                        localforage.setItem('user', res.data);
                        Cookies.set('_m', res.data.phone);
                        Cookies.set('_u', res.data._id);
                        window.location.href = '/login/complete';
                    }
                    else {
                        showErrorMsg(form, 'danger', res.error);
                    }

                }
            });
           
        });
    }

    var handleSignInConfirmPass = function () {
        $('#m_login_confirm_pass').click(function (e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');

            form.validate({
                rules: {
                    password: {
                        required: true
                    }
                },
                messages: {
                    password: {
                        required: "Cần nhập mật khẩu"
                    }
                }
            });

            if (!form.valid()) {
                return;
            }

            var pass = form.find('[name=password]').val();
            localforage.getItem('user', function (err, user) {
                if (!err) {
                    var username = user.phone;
                    var requestData = { username: username, password: pass };
                    $.ajax({
                        url: restUrl + 'login',
                        data: JSON.stringify(requestData),
                        type: 'POST',
                        dataType: 'json',
                        contentType: 'application/json; charset=utf-8',
                        error: function (xhr) {
                            btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                            showErrorMsg(form, 'danger', 'Đã có lỗi xảy ra khi đăng nhập');
                        },
                        success: function (res) {
                            btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                            if (res.errorCode == 0) {
                                var data = res.data;
                                Cookies.set('_tn', data.token);
                                Cookies.set('_u', data._id);
                                window.location.href = '/';
                            }
                            else {
                                showErrorMsg(form, 'danger', res.error);
                            }

                        }
                    });

                }
            });
        });
    }

    var handleSignUpFormSubmit = function () {
        $('#m_login_signup_submit').click(function (e) {
            e.preventDefault();

            var btn = $(this);
            var form = $(this).closest('form');

            form.validate({
                rules: {
                    fullname: {
                        required: true
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    password: {
                        required: true
                    },
                    rpassword: {
                        required: true
                    },
                    agree: {
                        required: true
                    }
                },
                messages: {
                    fullname: "Cần nhập họ tên",
                    email: "Cần nhập email",
                    password: "Cần nhập mật khẩu",
                    rpassword: "Cần nhập xác nhận mật khẩu",
                    agree: "Cần check đồng ý"
                }
            });


            if (!form.valid()) {
                return;
            }

            var email = form.find('[name=email]').val();
            var pass = form.find('[name=password]').val();
            var rpass = form.find('[name=rpassword]').val();
            var fullname = form.find('[name=fullname]').val();

            if (pass != rpass) {
                showErrorMsg(form, 'danger', 'Xác nhận lại mật khẩu không trùng khớp.');
                return;
            }

            btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);
            localforage.getItem('user', function (err, user) {
                var _id = user._id
                var mobile = user.phone;
                var requestData = { _id: _id, mobile: mobile, email: email, password: pass, fullname: fullname };
                console.log(requestData);

                $.ajax({
                    url: restUrl + 'completeSignup',
                    data: JSON.stringify(requestData),
                    type: 'POST',
                    dataType: 'json',
                    contentType: 'application/json; charset=utf-8',
                    error: function (xhr) {
                        btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                        showErrorMsg(form, 'danger', 'Đã có lỗi xảy ra khi đăng kí');
                    },
                    success: function (res) {
                        btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                        if (res.errorCode == 0) {
                            var data = res.data;
                            console.log(data);
                            Cookies.set('_tn', data.token);
                            Cookies.set('_u', data._id);
                            window.location.href = '/';
                        }
                        else {
                            showErrorMsg(form, 'danger', res.error);
                        }

                    }
                });
            });

        });
    }

    var bgAnimate = function () {
        // Some random colors
        const colors = ["#3CC157", "#2AA7FF", "#1B1B1B", "#FCBC0F", "#F85F36"];

        const numBalls = 13;
        const balls = [];
        var ballWrapper = document.getElementById('balls');
        for (let i = 0; i < numBalls; i++) {
            let ball = document.createElement("div");
            ball.classList.add("ball");
            ball.style.background = colors[Math.floor(Math.random() * colors.length)];
            ball.style.left = `${Math.floor(Math.random() * 100)}vw`;
            ball.style.top = `${Math.floor(Math.random() * 100)}vh`;
            ball.style.transform = `scale(${Math.random()})`;
            ball.style.width = `${Math.random()}em`;
            ball.style.height = ball.style.width;

            balls.push(ball);
            ballWrapper.append(ball);
        }

        // Keyframes
        balls.forEach((el, i, ra) => {
            let to = {
                x: Math.random() * (i % 2 === 0 ? -11 : 11),
                y: Math.random() * 12
            };

            let anim = el.animate(
                [
                    { transform: "translate(0, 0)" },
                    { transform: `translate(${to.x}rem, ${to.y}rem)` }
                ],
                {
                    duration: (Math.random() + 1) * 2000, // random duration
                    direction: "alternate",
                    fill: "both",
                    iterations: Infinity,
                    easing: "ease-in-out"
                }
            );
        });

    }

    //== Public Functions
    return {
        // public functions
        init: function () {
            bgAnimate();
            handleSignInConfirmPass();
            handleSignInFormSubmit();
            handleSignUpFormSubmit();
        },
        showErrorMsg: showErrorMsg
    };
}();

//== Class Initialization
jQuery(document).ready(function () {
    SnippetLogin.init();
});