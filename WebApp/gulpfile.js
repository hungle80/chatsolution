﻿var gulp = require('gulp'),
    jshint = require('gulp-jshint');
var uglifyes = require('uglify-es'),
    composer = require('gulp-uglify/composer'),
    minify = composer(uglifyes, console);;
var concat = require('gulp-concat'),
    pump = require('pump');
var cleanCSS = require('gulp-clean-css');
var child = require('child_process');
var fs = require('fs');
var pm2 = require('pm2');

var indexJs = [
    
    'public/javascripts/vendor/popper.min.js',
    'public/javascripts/vendor/socket.io.js',
    'public/javascripts/vendor/moment-with-locales.min.js',
    'public/javascripts/vendor/pnotify.custom.min.js',
    'public/javascripts/vendor/jquery.percentageloader-0.1.js',
    'public/javascripts/vendor/select2.full.js',
    'public/javascripts/vendor/emojione.js',
    'public/javascripts/vendor/jquery.mark.min.js',
    'public/javascripts/vendor/jquery.magnific-popup.js',
    'public/javascripts/vendor/bootstrap-datetimepicker.min.js',
    'public/javascripts/vendor/cropper.js',
    'public/javascripts/vendor/jquery.jeditable.js',
    'public/javascripts/vendor/js.cookie.js',
    'public/javascripts/vendor/favico.js',
    'public/javascripts/vendor/lodash.min.js',
    'public/javascripts/vendor/jquery-ui-autocomplete.js',
    'public/javascripts/vendor/jquery.mentions.js',
    'src/javascript/notifyHelper.js',
    'src/javascript/imagePaste.js',
    'src/javascript/textParseHelper.js',
    'src/javascript/dateTimeHelper.js',
    'src/javascript/url.js',
    //'src/javascript/myLinkPreview.js',
    'src/javascript/link_preview.js',
    'src/javascript/imageHelper.js',
    'src/javascript/modal.js',
    'src/javascript/mobileRedirect.js',
    'src/javascript/messageHelper.js',
    'src/javascript/index.js',
    'src/javascript/channel/createChannel.js',
    'src/javascript/channel/updateChannel.js',
    'src/javascript/channel/channelAdministrator.js',
    'src/javascript/channel/channelBanUser.js',
    'src/javascript/group/updateGroup.js',
    'src/javascript/contact/contact.js',
    'src/javascript/googleCalendar.js',
    'src/javascript/plan/map.js',
    'src/javascript/plan/plan.js',
    'src/javascript/account.js',
];
var widgetJs = [
    'public/javascripts/vendor/jquery-3.2.1.min.js',
    'public/javascripts/vendor/socket.io.js',
    'public/javascripts/vendor/moment-with-locales.min.js',
    'public/javascripts/vendor/pnotify.custom.min.js',
    'public/javascripts/vendor/jquery.percentageloader-0.1.js',
    'public/javascripts/vendor/emojione.js',
    'public/javascripts/vendor/jquery.magnific-popup.js',
    'public/javascripts/vendor/js.cookie.js',
    'public/javascripts/vendor/cropper.js',
    'public/javascripts/vendor/lodash.min.js',
    'src/javascript/modal.js',
    'src/javascript/imagePaste.js',
    'src/javascript/imageHelper.js',
    'src/javascript/notifyHelper.js',
    'src/javascript/messageHelper.js',
    'src/javascript/textParseHelper.js',
    'src/javascript/url.js',
    'src/javascript/dateTimeHelper.js',
    //'src/javascript/myLinkPreview.js',
    'src/javascript/link_preview.js',
    'src/javascript/constant.js',
    'src/javascript/widgets.js'
];
var joinJs = [
    'public/javascripts/vendor/jquery-3.2.1.min.js',
    'public/javascripts/vendor/socket.io.js',
    'public/javascripts/vendor/moment-with-locales.min.js',
    'public/javascripts/vendor/pnotify.custom.min.js',
    'src/javascript/textParseHelper.js',
    'src/javascript/dateTimeHelper.js',
    'src/javascript/constant.js',
    'src/javascript/notifyHelper.js',
    'src/javascript/join.js'
];

var indexCss = [
    'public/stylesheets/vendor/pnotify.custom.css',
    'public/stylesheets/vendor/magnific-popup.css',
    'public/stylesheets/vendor/cropper.css',
    'public/stylesheets/vendor/select2.css',    
    'public/stylesheets/vendor/bootstrap-datetimepicker.css',
    'public/stylesheets/vendor/jquery.mentions.css',
    'src/css/modal.css',
    'src/css/chatHistory.css',
    'src/css/chatHistoryFull.css',
    'src/css/emoji.css',
    'src/css/style.css',
    
];

var widgetCss = [
    'public/stylesheets/vendor/pnotify.custom.css',
    'public/stylesheets/vendor/magnific-popup.css',
    'public/stylesheets/vendor/cropper.css',
    'public/stylesheets/vendor/select2.css',
    'src/css/modal.css',
    'src/css/chatHistory.css',    
    'src/css/emoji.css',
    'src/css/forms.css',
    'src/css/widgets.css',    
];

var joinCss = [
    'public/stylesheets/vendor/pnotify.custom.css',
    'public/stylesheets/vendor/magnific-popup.css',
    'public/stylesheets/vendor/cropper.css',
    'public/stylesheets/vendor/select2.css',    
    'src/css/join.css',
];

gulp.task('default', ['server', 'watch', 'compressJS', 'cleanCSS']);
gulp.task('server', function () {
    //var server = child.spawn('node', ['./bin/www']);
    //var log = fs.createWriteStream('server.log', { flags: 'a' });
    //server.stdout.pipe(log);
    //server.stderr.pipe(log);
    pm2.connect(true, function () {
        pm2.start({
            name: 'server',
            script: './app.js'
        }, function () {
            console.log('pm2 started');
            pm2.streamLogs('all', 0);
        });
    });
});

gulp.task('jshint', function () {
    return gulp.src('src/javascript/**/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('jshint-stylish'));
});

gulp.task('compressJS', function (cb) {
    pump([
        gulp.src(indexJs),
        concat("index.js"),
        minify(),
        gulp.dest('public/javascripts')
    ], cb);
    pump([
        gulp.src(widgetJs),
        concat("widgets.js"),
        minify(),
        gulp.dest('public/javascripts')
    ]);
    pump([
        gulp.src(joinJs),
        concat("join.js"),
        minify(),
        gulp.dest('public/javascripts')
    ]);
});

gulp.task('cleanCSS', function (cb) {
    pump([
        gulp.src(indexCss),
        concat("index.css"),
        cleanCSS({
            inline: ['remote'] // default; enables local inlining only
        }),
        gulp.dest('public/stylesheets')
    ], cb);

    pump([
        gulp.src(widgetCss),
        concat("widgets.css"),
        cleanCSS({
            inline: ['remote'] // default; enables local inlining only
        }),
        gulp.dest('public/stylesheets')
    ]);
    pump([
        gulp.src(joinCss),
        concat("join.css"),
        cleanCSS({
            inline: ['remote'] // default; enables local inlining only
        }),
        gulp.dest('public/stylesheets')
    ]);
});

gulp.task('watch', function () {
    gulp.watch('src/javascript/**/*.js', ['jshint', 'compressJS']);
    gulp.watch('src/css/**/*.css', ['cleanCSS']);
});