﻿const Request = require('request');
const Querystring = require('querystring');

const account_kit_api_version = 'v1.0';
const app_id = '359113728215314';
const app_secret = '0b43d847fba2a2e860ec3c426731b8b9';
const me_endpoint_base_url = 'https://graph.accountkit.com/' + account_kit_api_version + '/me';
const token_exchange_base_url = 'https://graph.accountkit.com/' + account_kit_api_version + '/access_token';

function queryAccessTokenData(code, callback) {
    var app_access_token = ['AA', app_id, app_secret].join('|');
    var params = {
        grant_type: 'authorization_code',
        code: code,
        access_token: app_access_token
    };
    // exchange tokens
    var token_exchange_url = token_exchange_base_url + '?' + Querystring.stringify(params);
    Request.get({ url: token_exchange_url, json: true }, function (err, resp, respBody) {
        console.log(respBody);
        var view = {
            user_access_token: respBody.access_token,
            expires_at: respBody.expires_at,
            user_id: respBody.id,
        };

        // get account details at /me endpoint
        var me_endpoint_url = me_endpoint_base_url + '?access_token=' + respBody.access_token;
        Request.get({ url: me_endpoint_url, json: true }, function (err, resp, respBody) {
            console.log(respBody);
            // send login_success.html
            if (respBody.phone) {
                callback({ errorCode: 0, data: respBody.phone.national_number });
            }
            else if (respBody.email) {
                callback({ errorCode: 0, data: respBody.email.address });
            }
            else {
                callback({ errorCode: 109, error: 'Xác thực đã quá hạn. Vui lòng thử lại' });
            }
        });
    });
}
exports.queryAccessTokenData = queryAccessTokenData;

